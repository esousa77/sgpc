/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "alinea")
@NamedQueries({
    @NamedQuery(name = "AlineaBean.findAll", query = "SELECT a FROM AlineaBean a"),
    @NamedQuery(name = "AlineaBean.findByAuxCapital", query = "SELECT a FROM AlineaBean a WHERE a.fkAuxilio = :auxilio AND a.fkCategoriaAlinea.capital = :capital"),
    @NamedQuery(name = "AlineaBean.findByAuxCusteio", query = "SELECT a FROM AlineaBean a WHERE a.fkAuxilio = :auxilio AND a.fkCategoriaAlinea.custeio = :custeio"),
    @NamedQuery(name = "AlineaBean.findByAuxReservaTecnica", query = "SELECT a FROM AlineaBean a WHERE a.fkAuxilio = :auxilio AND a.fkCategoriaAlinea.reservaTecnica = :reservaTecnica"),
    @NamedQuery(name = "AlineaBean.findByAuxBeneficioComplementar", query = "SELECT a FROM AlineaBean a WHERE a.fkAuxilio = :auxilio AND a.fkCategoriaAlinea.beneficioComplementar = :beneficioComplementar"),
    @NamedQuery(name = "AlineaBean.findByVerbaAprovada", query = "SELECT a FROM AlineaBean a WHERE a.verbaAprovada = :verbaAprovada"),
    @NamedQuery(name = "AlineaBean.findById", query = "SELECT a FROM AlineaBean a WHERE a.id = :id")})
public class AlineaBean implements Serializable, SgpcBeanInterface {
    public static final Comparator<AlineaBean> POR_NOME_CATEGORIA = new Comparator<AlineaBean>(){
        public int compare(AlineaBean al1, AlineaBean al2){
            if(al1!=null && al2!=null && al1.getFkCategoriaAlinea()!=null && al2.getFkCategoriaAlinea()!=null && al1.getFkCategoriaAlinea().getNome()!=null && al2.getFkCategoriaAlinea().getNome()!=null){
                return al1.getFkCategoriaAlinea().getNome().toUpperCase().compareTo(al2.getFkCategoriaAlinea().getNome().toUpperCase());
            }
            return -1;
        }
    };
    
    public static final Integer MOEDA_REAL = 1;
    public static final Integer MOEDA_DOLAR = 2;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "alinea_id_alinea_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_alinea", nullable = false)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name="fk_categoria_alinea", referencedColumnName = "id_categoria_alinea", nullable = false)
    private CategoriaAlineaBean fkCategoriaAlinea;

    @Basic(optional = false)
    @Column(name = "verba_aprovada", nullable = false)
    private BigDecimal verbaAprovada;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkAlinea", fetch = FetchType.LAZY, orphanRemoval = true)
    @OrderBy("dataRealizada ASC")
    private List<DespesaBean> despesas;

    @JoinColumn(name = "fk_auxilio", referencedColumnName = "id_auxilio", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected AuxilioBean fkAuxilio;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "fkAlinea", orphanRemoval = true)
    private List<BolsaAlocadaBean> bolsasAlocadas;
    
    @Column(name = "moeda", nullable = false)
    private Integer moeda;
    
    @Column(name = "observacoes", nullable = false)
    private String observacoes;
    
    @OneToMany(mappedBy = "fkAlinea", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
    private List<SubcentroDespesasBean> subcentrosDespesas;
    
    public AlineaBean() {
        despesas = new ArrayList<DespesaBean>();
    }

    public List<BolsaAlocadaBean> getBolsasAlocadas() {
        return bolsasAlocadas;
    }

    public void setBolsasAlocadas(List<BolsaAlocadaBean> bolsasAlocadas) {
        this.bolsasAlocadas = bolsasAlocadas;
    }

    public CategoriaAlineaBean getFkCategoriaAlinea() {
        return fkCategoriaAlinea;
    }

    public void setFkCategoriaAlinea(CategoriaAlineaBean fkCategoriaAlinea) {
        this.fkCategoriaAlinea = fkCategoriaAlinea;
    }

    public AuxilioBean getFkAuxilio() {
        return fkAuxilio;
    }

    public void setFkAuxilio(AuxilioBean fkAuxilio) {
        this.fkAuxilio = fkAuxilio;
    }

    public BigDecimal getVerbaAprovada() {
        return verbaAprovada;
    }

    public void setVerbaAprovada(BigDecimal verbaAprovada) {
        this.verbaAprovada = verbaAprovada;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<DespesaBean> getDespesas() {
        return despesas;
    }

    public void setDespesas(List<DespesaBean> despesas) {
        this.despesas = despesas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlineaBean)) {
            return false;
        }
        AlineaBean other = (AlineaBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.fkCategoriaAlinea.getNome();
    }

    public Integer getMoeda() {
        return moeda;
    }

    public void setMoeda(Integer moeda) {
        this.moeda = moeda;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public List<SubcentroDespesasBean> getSubcentrosDespesas() {
        return subcentrosDespesas;
    }

    public void setSubcentrosDespesas(List<SubcentroDespesasBean> subcentrosDespesas) {
        this.subcentrosDespesas = subcentrosDespesas;
    }
    
}
