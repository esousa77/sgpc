/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "cotacao")
@NamedQueries({
    @NamedQuery(name = "CotacaoBean.findAll", query = "SELECT c FROM CotacaoBean c"),
    @NamedQuery(name = "CotacaoBean.findById", query = "SELECT c FROM CotacaoBean c WHERE c.id = :id"),
    @NamedQuery(name = "CotacaoBean.findByDataSolicitada", query = "SELECT c FROM CotacaoBean c WHERE c.dataSolicitada = :dataSolicitada"),
    @NamedQuery(name = "CotacaoBean.findByObservacoes", query = "SELECT c FROM CotacaoBean c WHERE c.observacoes = :observacoes"),
    @NamedQuery(name = "CotacaoBean.findByStatus", query = "SELECT c FROM CotacaoBean c WHERE c.status = :status")})
public class CotacaoBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "cotacao_id_cotacao_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_cotacao", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "data_solicitada", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataSolicitada;

    @Column(name = "observacoes", length = 1000)
    private String observacoes;

    @Column(name = "status", length = 20)
    private String status;
    
    @JoinColumn(name = "fk_projeto", referencedColumnName = "id_projeto", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ProjetoBean fkProjeto;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkCotacao", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<ItemBean> itens;

    public CotacaoBean() {
        dataSolicitada = new Date();
    }

    public CotacaoBean(Integer id) {
        this.id = id;
    }

    public CotacaoBean(Integer id, Date dataSolicitada) {
        this.id = id;
        this.dataSolicitada = dataSolicitada;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataSolicitada() {
        return dataSolicitada;
    }

    public void setDataSolicitada(Date dataSolicitada) {
        this.dataSolicitada = dataSolicitada;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProjetoBean getFkProjeto() {
        return fkProjeto;
    }

    public void setFkProjeto(ProjetoBean fkProjeto) {
        this.fkProjeto = fkProjeto;
    }

    public List<ItemBean> getItens() {
        return itens;
    }

    public void setItens(List<ItemBean> itens) {
        this.itens = itens;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CotacaoBean)) {
            return false;
        }
        CotacaoBean other = (CotacaoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Cotacao[id=" + id + "]";
    }

}
