/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "instituicao_bancaria")
@NamedQueries({
    @NamedQuery(name = "InstituicaoBancariaBean.findAll", query = "SELECT i FROM InstituicaoBancariaBean i"),
    @NamedQuery(name = "InstituicaoBancariaBean.findByNome", query = "SELECT i FROM InstituicaoBancariaBean i WHERE i.nome = :nome"),
    @NamedQuery(name = "InstituicaoBancariaBean.findByWebsite", query = "SELECT i FROM InstituicaoBancariaBean i WHERE i.website = :website"),
    @NamedQuery(name = "InstituicaoBancariaBean.findByCodigo", query = "SELECT i FROM InstituicaoBancariaBean i WHERE i.codigo = :codigo"),
    @NamedQuery(name = "InstituicaoBancariaBean.findById", query = "SELECT i FROM InstituicaoBancariaBean i WHERE i.id = :id"),
    @NamedQuery(name = "InstituicaoBancariaBean.findByNomeFantasia", query = "SELECT i FROM InstituicaoBancariaBean i WHERE i.nomeFantasia = :nomeFantasia"),
    @NamedQuery(name = "InstituicaoBancariaBean.findByKeyword", query = "SELECT i FROM InstituicaoBancariaBean i WHERE upper(i.nome) LIKE upper(:keyword) OR upper(i.nomeFantasia) LIKE upper(:keyword) OR upper(i.website) LIKE upper(:keyword) OR i.codigo LIKE :keyword")})
public class InstituicaoBancariaBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;

    @Column(name = "website", nullable = false, length = 100)
    private String website;

    @Column(name = "codigo", nullable = false, length = 10)
    private String codigo;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="instituicao_bancaria_id_instituicao_bancaria_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_instituicao_bancaria", nullable = false)
    private Integer id;

    @Column(name = "nome_fantasia", length = 100)
    private String nomeFantasia;

    public InstituicaoBancariaBean() {
    }

    public InstituicaoBancariaBean(Integer id) {
        this.id = id;
    }

    public InstituicaoBancariaBean(Integer id, String nome, String website, String codigo) {
        this.id = id;
        this.nome = nome;
        this.website = website;
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstituicaoBancariaBean)) {
            return false;
        }
        InstituicaoBancariaBean other = (InstituicaoBancariaBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getNome();
    }

}
