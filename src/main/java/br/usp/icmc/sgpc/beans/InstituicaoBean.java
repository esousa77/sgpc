/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "instituicao")
@NamedQueries({
    @NamedQuery(name = "InstituicaoBean.findAll", query = "SELECT i FROM InstituicaoBean i"),
    @NamedQuery(name = "InstituicaoBean.findById", query = "SELECT i FROM InstituicaoBean i WHERE i.id = :id"),
    @NamedQuery(name = "InstituicaoBean.findByNome", query = "SELECT i FROM InstituicaoBean i WHERE i.nome = :nome"),
    @NamedQuery(name = "InstituicaoBean.findByKeyword", query = "SELECT i FROM InstituicaoBean i WHERE upper(i.nome) LIKE upper(:keyword) OR upper(i.sigla) LIKE upper(:keyword)")})
public class InstituicaoBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="instituicao_id_instituicao_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_instituicao", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 200)
    private String nome;

    @Basic(optional = false)
    @Column(name = "sigla", nullable = false, length = 30)
    private String sigla;

    @OneToOne
    @JoinColumn(name = "fk_endereco",referencedColumnName="id_endereco", nullable = true)
    private EnderecoBean fkEndereco;

    @OneToMany(mappedBy = "fkInstituicao", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @OrderBy("sigla ASC")
    private List<UnidadeBean> unidades;

    public InstituicaoBean() {
        unidades = new ArrayList<UnidadeBean>();
    }

    public InstituicaoBean(Integer id) {
        this.id = id;
    }

    public InstituicaoBean(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public EnderecoBean getFkEndereco() {
        return fkEndereco;
    }

    public void setFkEndereco(EnderecoBean fkEndereco) {
        this.fkEndereco = fkEndereco;
    }

    public List<UnidadeBean> getUnidades() {
        return unidades;
    }

    public void setUnidades(List<UnidadeBean> unidades) {
        this.unidades = unidades;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstituicaoBean)) {
            return false;
        }
        InstituicaoBean other = (InstituicaoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getNome();
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
