/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "pesquisa")
@Inheritance(strategy=InheritanceType.JOINED)
@PrimaryKeyJoinColumn (name = "id_projeto")
@NamedQueries({
    @NamedQuery(name = "PesquisaBean.findAll", query = "SELECT p FROM PesquisaBean p"),
    @NamedQuery(name = "PesquisaBean.findByKeyword", query = "SELECT p FROM PesquisaBean p WHERE upper(p.titulo) LIKE upper(:keyword) OR upper(p.descricao) LIKE upper(:keyword) ORDER BY p.dataCriacao desc, p.titulo "),
    @NamedQuery(name = "PesquisaBean.findByOwner", query = "SELECT p FROM PesquisaBean p WHERE p.fkResponsavel = :responsavel"),
    @NamedQuery(name = "PesquisaBean.findByOwnerTerm", query = "SELECT p FROM PesquisaBean p WHERE p.fkResponsavel = :responsavel AND (upper(p.titulo) LIKE upper(:keyword) OR upper(p.descricao) LIKE upper(:keyword))"),
    @NamedQuery(name = "PesquisaBean.findById", query = "SELECT p FROM PesquisaBean p WHERE p.id = :id")})
public class PesquisaBean extends ProjetoBean implements Serializable{
    protected static final long serialVersionUID = 1L;

    public PesquisaBean(){
    }

    @Override
    public int getTipo() {
        return PESQUISA;
    }

}
