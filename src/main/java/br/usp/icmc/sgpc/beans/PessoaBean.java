/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "pessoa")
@NamedQueries({
    @NamedQuery(name = "PessoaBean.findAll", query = "SELECT p FROM PessoaBean p ORDER BY p.nome, p.username"),
    @NamedQuery(name = "PessoaBean.findByNome", query = "SELECT p FROM PessoaBean p WHERE p.nome = :nome"),
    @NamedQuery(name = "PessoaBean.findByUsername", query = "SELECT p FROM PessoaBean p WHERE p.username = :username"),
    @NamedQuery(name = "PessoaBean.findByPassword", query = "SELECT p FROM PessoaBean p WHERE p.password = :password"),
    @NamedQuery(name = "PessoaBean.findByEmail", query = "SELECT p FROM PessoaBean p WHERE p.email = :email"),
    @NamedQuery(name = "PessoaBean.findByRg", query = "SELECT p FROM PessoaBean p WHERE p.rg = :rg"),
    @NamedQuery(name = "PessoaBean.findByCpf", query = "SELECT p FROM PessoaBean p WHERE p.cpf = :cpf"),
    @NamedQuery(name = "PessoaBean.findByPassaporte", query = "SELECT p FROM PessoaBean p WHERE p.passaporte = :passaporte"),
    @NamedQuery(name = "PessoaBean.findByDataNascimento", query = "SELECT p FROM PessoaBean p WHERE p.dataNascimento= :dataNascimento"),
    @NamedQuery(name = "PessoaBean.findByNomeMae", query = "SELECT p FROM PessoaBean p WHERE p.nomeMae= :nomeMae"),
    @NamedQuery(name = "PessoaBean.findByGroup", query = "SELECT p FROM PessoaBean p WHERE :papel in elements(p.papeis)ORDER BY p.nome"),
    @NamedQuery(name = "PessoaBean.findById", query = "SELECT p FROM PessoaBean p WHERE p.id = :id"),
    @NamedQuery(name = "PessoaBean.findByKeyword", query = "SELECT p FROM PessoaBean p WHERE upper(p.username) LIKE upper(:keyword) OR upper(p.nome) LIKE upper(:keyword) OR p.rg LIKE :keyword OR p.cpf LIKE :keyword OR p.email LIKE :keyword ORDER BY p.username")})
public class PessoaBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    public static final int ESCOPO_INTERNO = 0;
    public static final int ESCOPO_EXTERNO = 1;
    
    public static final Comparator<PessoaBean> POR_NOME = new Comparator<PessoaBean>(){
        public int compare(PessoaBean p1, PessoaBean p2){
            if(p1!=null && p2!=null && p1.nome!=null && p2.nome!=null){
                //System.out.println("+++++ " + p1.nome + " ===== " + p2.nome + "  -  " + p1.nome.compareTo(p2.nome));
                return p1.nome.compareTo(p2.nome);
            }
            return -1;
        }
    };

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="pessoa_id_pessoa_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_pessoa", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;

    @Basic(optional = true)
    @Column(name = "sobrenome", nullable = true, length = 100)
    private String sobrenome;

    @Basic(optional = false)
    @Column(name = "username", nullable = false, length = 20)
    private String username;

    @Basic(optional = false)
    @Column(name = "password", nullable = false, length = 100)
    private String password;

    @Basic(optional = true)
    @Column(name = "email", length = 100)
    private String email;

    @Basic(optional = true)
    @Column(name = "cpf", length = 100)
    private String cpf;

    @Basic(optional = true)
    @Column(name = "rg", length = 100)
    private String rg;

    @Basic(optional = true)
    @Column(name = "passaporte", length = 100)
    private String passaporte;

    @Column(name = "data_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @Column(name = "nome_mae", length = 100)
    private String nomeMae;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "inscricao_atividade",
        joinColumns =
            @JoinColumn(name = "fk_pessoa", referencedColumnName = "id_pessoa"),
        inverseJoinColumns =
            @JoinColumn(name = "fk_atividade", referencedColumnName = "id_atividade")
    )
    private List<AtividadeBean> inscricaoAtividades;

    @OneToMany(mappedBy = "fkResponsavel")
    private List<ProjetoBean> projetos;

    @OneToMany(mappedBy = "fkResponsavel")
    private List<PrestacaoContasBean> prestacoesContas;

    @JoinColumn(name = "fk_departamento", referencedColumnName = "id_departamento")
    @ManyToOne(fetch = FetchType.LAZY)
    private DepartamentoBean fkDepartamento;

    @JoinColumn(name = "fk_endereco", referencedColumnName = "id_endereco")
    @ManyToOne(fetch = FetchType.LAZY)
    private EnderecoBean fkEndereco;

    @OneToMany(mappedBy = "fkPessoa", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @OrderBy("fkBanco, agencia")
    private List<ContaCorrenteBean> contasCorrentes;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "pessoa_papel",
        joinColumns =
            @JoinColumn(name = "fk_pessoa", referencedColumnName = "id_pessoa"),
        inverseJoinColumns =
            @JoinColumn(name = "fk_papel", referencedColumnName = "id_papel")
    )
    @OrderBy("nome")
    private List<PapelBean> papeis;

    @Basic(optional = true)
    @Column(name = "telefone", length = 25)
    private String telefone;

    @Basic(optional = true)
    @Column(name = "telefone_celular", length = 25)
    private String telefoneCelular;
    
    @Column(name="escopo", columnDefinition="INTEGER default 0")
    private int escopo;

    public PessoaBean() {
        papeis = new ArrayList<PapelBean>();
    }

    public PessoaBean(Integer id) {
        this.id = id;
    }

    public PessoaBean(Integer id, String nome, String sobrenome, String username, String password, String email, String cpf, String rg, String passaporte, Date dataNascimento, String nomeMae) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.username = username;
        this.password = password;
        this.email = email;
        this.cpf = cpf;
        this.rg = rg;
        this.passaporte = passaporte;
        this.dataNascimento = dataNascimento;
        this.nomeMae = nomeMae;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ContaCorrenteBean> getContasCorrentes() {
        return contasCorrentes;
    }

    public void setContasCorrentes(List<ContaCorrenteBean> contasCorrentes) {
        this.contasCorrentes = contasCorrentes;
    }

    public List<AtividadeBean> getInscricaoAtividades() {
        return inscricaoAtividades;
    }

    public void setInscricaoAtividades(List<AtividadeBean> inscricaoAtividades) {
        this.inscricaoAtividades = inscricaoAtividades;
    }

    public List<PrestacaoContasBean> getPrestacoesContas() {
        return prestacoesContas;
    }

    public void setPrestacoesContas(List<PrestacaoContasBean> prestacoesContas) {
        this.prestacoesContas = prestacoesContas;
    }
    
    public List<ProjetoBean> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<ProjetoBean> projetos) {
        this.projetos = projetos;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPassaporte() {
        return passaporte;
    }

    public void setPassaporte(String passaporte) {
        this.passaporte = passaporte;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public DepartamentoBean getFkDepartamento() {
        return fkDepartamento;
    }

    public void setFkDepartamento(DepartamentoBean fkDepartamento) {
        this.fkDepartamento = fkDepartamento;
    }

    public EnderecoBean getFkEndereco() {
        return fkEndereco;
    }

    public void setFkEndereco(EnderecoBean fkEndereco) {
        this.fkEndereco = fkEndereco;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
         // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PessoaBean)) {
            return false;
        }
        PessoaBean other = (PessoaBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getNome();
    }

    public List<PapelBean> getPapeis() {
        return papeis;
    }

    public void setPapeis(List<PapelBean> papeis) {
        this.papeis = papeis;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

    public int getEscopo() {
        return escopo;
    }

    public void setEscopo(int escopo) {
        this.escopo = escopo;
    }

}
