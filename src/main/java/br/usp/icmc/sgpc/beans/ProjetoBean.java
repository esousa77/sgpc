/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "projeto")
@Inheritance(strategy=InheritanceType.JOINED)
@NamedQueries({
    @NamedQuery(name = "ProjetoBean.findAll", query = "SELECT p FROM ProjetoBean p ORDER BY fkResponsavel.nome, p.dataCriacao desc, p.titulo "),
    @NamedQuery(name = "ProjetoBean.findById", query = "SELECT p FROM ProjetoBean p WHERE p.id = :id"),
    @NamedQuery(name = "ProjetoBean.findByTitulo", query = "SELECT p FROM ProjetoBean p WHERE p.titulo = :titulo"),
    @NamedQuery(name = "ProjetoBean.findByDescricao", query = "SELECT p FROM ProjetoBean p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "ProjetoBean.findByStatus", query = "SELECT p FROM ProjetoBean p WHERE p.status = :status"),
    @NamedQuery(name = "ProjetoBean.findByDataInicial", query = "SELECT p FROM ProjetoBean p WHERE p.dataInicial = :dataInicial"),
    @NamedQuery(name = "ProjetoBean.findByDataFinal", query = "SELECT p FROM ProjetoBean p WHERE p.dataFinal = :dataFinal"),
    @NamedQuery(name = "ProjetoBean.findByDataCriacao", query = "SELECT p FROM ProjetoBean p WHERE p.dataCriacao = :dataCriacao"),
    @NamedQuery(name = "ProjetoBean.findByOwner", query = "SELECT p FROM ProjetoBean p WHERE p.fkResponsavel = :responsavel"),
    @NamedQuery(name = "ProjetoBean.findByOwnerTerm", query = "SELECT p FROM ProjetoBean p WHERE p.fkResponsavel = :responsavel AND (upper(p.titulo) LIKE upper(:keyword) OR upper(p.descricao) LIKE upper(:keyword))"),
    @NamedQuery(name = "ProjetoBean.findByKeyword", query = "SELECT p FROM ProjetoBean p WHERE upper(p.titulo) LIKE upper(:keyword) OR upper(p.descricao) LIKE upper(:keyword) ORDER BY p.dataCriacao desc, p.titulo ")})
public abstract class ProjetoBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_EM_PLANEJAMENTO = "Em Planejamento";
    public static final String STATUS_EM_ANDAMENTO = "Em Andamento";
    public static final String STATUS_FINALIZADO = "Finalizado";
    public static final String STATUS_CANCELADO = "Cancelado";
    public static final String STATUS_ABORTADO = "Abortado";
    public static final String STATUS_DESISTENCIA = "Desistência";
    public static final String STATUS_ATRASADO = "Atrasado";
    
    protected static final int EVENTO = 1;
    protected static final int PESQUISA = 0;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="projeto_id_projeto_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_projeto", nullable = false)    
    protected Integer id;

    @Basic(optional = false)
    @Column(name = "titulo", nullable = false, length = 500)
    protected String titulo;

    @Basic(optional = false)
    @Column(name = "descricao", nullable = false, length = 1000)
    protected String descricao;

    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 50)
    protected String status;

    @Column(name = "data_inicial")
    @Temporal(TemporalType.DATE)
    protected Date dataInicial;

    @Column(name = "data_final")
    @Temporal(TemporalType.DATE)
    protected Date dataFinal;

    @Column(name = "data_criacao")
    @Temporal(TemporalType.DATE)
    protected Date dataCriacao;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkProjeto", fetch = FetchType.LAZY)
    private List<CotacaoBean> cotacoes;

    @JoinColumn(name = "fk_responsavel", referencedColumnName = "id_pessoa", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private PessoaBean fkResponsavel;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "fkProjeto", orphanRemoval = true)
    @OrderBy("numeroMercurio")
    private List<AuxilioBean> auxilios;

    @JoinColumn(name = "fk_projeto_pai", referencedColumnName = "id_projeto")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProjetoBean fkProjetoPai;

    @JoinColumn(name = "fk_departamento", referencedColumnName = "id_departamento", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DepartamentoBean fkDepartamento;

    public ProjetoBean() {
        this.dataCriacao = new Date();
        auxilios = new ArrayList<AuxilioBean>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public List<AuxilioBean> getAuxilios() {
        return auxilios;
    }

    public void setAuxilios(List<AuxilioBean> auxilios) {
        this.auxilios = auxilios;
    }

    public List<CotacaoBean> getCotacoes() {
        return cotacoes;
    }

    public void setCotacoes(List<CotacaoBean> cotacoes) {
        this.cotacoes = cotacoes;
    }

    public DepartamentoBean getFkDepartamento() {
        return fkDepartamento;
    }

    public void setFkDepartamento(DepartamentoBean fkDepartamento) {
        this.fkDepartamento = fkDepartamento;
    }

    public ProjetoBean getFkProjetoPai() {
        return fkProjetoPai;
    }

    public void setFkProjetoPai(ProjetoBean fkProjetoPai) {
        this.fkProjetoPai = fkProjetoPai;
    }

    public PessoaBean getFkResponsavel() {
        return fkResponsavel;
    }

    public void setFkResponsavel(PessoaBean fkResponsavel) {
        this.fkResponsavel = fkResponsavel;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProjetoBean)) {
            return false;
        }
        ProjetoBean other = (ProjetoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getTitulo();
    }

    public abstract int getTipo();

}
