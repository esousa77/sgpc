/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "subcentrodespesas_alinea")
public class SubcentroDespesasBean implements Serializable, SgpcBeanInterface {
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "subcentrodespesas_alinea_id_subcentrodespesas_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_subcentrodespesas", nullable = false)
    private Integer id;
    
    @JoinColumn(name = "fk_centro_despesa", referencedColumnName = "id_centro_despesa", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private CentroDespesaBean fkCentroDespesa;
    
    @Basic(optional = false)
    @Column(name = "valor", nullable = false)
    private BigDecimal valor;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkSubcentro", fetch = FetchType.LAZY, orphanRemoval = true)
    @OrderBy("dataRealizada ASC")
    private List<DespesaBean> despesas;
    
    @OneToOne
    @JoinColumn(name = "fk_alinea",referencedColumnName="id_alinea", nullable = false)
    private AlineaBean fkAlinea;
    
    @Column(name = "centro_distribuidor", nullable = false, columnDefinition = "boolean default false")
    private boolean centroDistribuidor;
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubcentroDespesasBean)) {
            return false;
        }
        SubcentroDespesasBean other = (SubcentroDespesasBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        if(this.id != null && other.id != null && this.id.intValue() == other.id.intValue()) return true;
        
        if((this.fkAlinea.getId() != other.fkAlinea.getId()) ||(this.fkCentroDespesa.getId() != other.fkCentroDespesa.getId())) return false;
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.SubcentroDespesasBean[id=" + id + "]";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CentroDespesaBean getFkCentroDespesa() {
        return fkCentroDespesa;
    }

    public void setFkCentroDespesa(CentroDespesaBean fkCentroDespesa) {
        this.fkCentroDespesa = fkCentroDespesa;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public AlineaBean getFkAlinea() {
        return fkAlinea;
    }

    public void setFkAlinea(AlineaBean fkAlinea) {
        this.fkAlinea = fkAlinea;
    }

    public List<DespesaBean> getDespesas() {
        return despesas;
    }

    public void setDespesas(List<DespesaBean> despesas) {
        this.despesas = despesas;
    }

    public boolean isCentroDistribuidor() {
        return centroDistribuidor;
    }

    public void setCentroDistribuidor(boolean centroDistribuidor) {
        this.centroDistribuidor = centroDistribuidor;
    }
    
}
