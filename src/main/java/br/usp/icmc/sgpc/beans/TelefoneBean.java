/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "telefone")
@NamedQueries({
    @NamedQuery(name = "TelefoneBean.findAll", query = "SELECT t FROM TelefoneBean t"),
    @NamedQuery(name = "TelefoneBean.findByDdd", query = "SELECT t FROM TelefoneBean t WHERE t.ddd = :ddd"),
    @NamedQuery(name = "TelefoneBean.findByNumero", query = "SELECT t FROM TelefoneBean t WHERE t.numero = :numero"),
    @NamedQuery(name = "TelefoneBean.findByRamal", query = "SELECT t FROM TelefoneBean t WHERE t.ramal = :ramal"),
    @NamedQuery(name = "TelefoneBean.findByTipo", query = "SELECT t FROM TelefoneBean t WHERE t.tipo = :tipo")})
public class TelefoneBean implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "id_telefone", nullable = false)
    private Integer id;

    @Column(name = "ddd", length = 10)
    private String ddd;

    @Basic(optional = false)
    @Column(name = "numero",nullable = false, length = 20)
    private String numero;

    @Column(name = "ramal", length = 10)
    private String ramal;

    @Column(name = "tipo", length = 20)
    private String tipo;

    public TelefoneBean(){
    }

    public TelefoneBean(Integer id, String ddd, String numero, String ramal, String tipo) {
        this.id = id;
        this.ddd = ddd;
        this.numero = numero;
        this.ramal = ramal;
        this.tipo = tipo;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getRamal() {
        return ramal;
    }

    public void setRamal(String ramal) {
        this.ramal = ramal;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TelefoneBean)) {
            return false;
        }
        TelefoneBean other = (TelefoneBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Telefone[id=" + id + "]";
    }

}
