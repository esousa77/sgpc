/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.FornecedorBean_;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author David
 */
public class FornecedorJpaDao extends GenericJpaDao<FornecedorBean> {

    public FornecedorJpaDao() {
        super(FornecedorBean.class);
    }
    
    public List<FornecedorBean> listar(){
        List<FornecedorBean> listaFornecedores = null;
        EntityManager em = getEntityManager();
        try {
            Query queryFornecedorByKeyword = em.createQuery("SELECT f FROM FornecedorBean f ORDER BY f.nomeFantasia");
            listaFornecedores = queryFornecedorByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Fornecedor não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }

        return listaFornecedores;
    }

    public List<FornecedorBean> pesquisar(String termoPesquisa) {
        List<FornecedorBean> fornecedor = null;
        EntityManager em = getEntityManager();
        try {
            Query queryFornecedorByKeyword = em.createNamedQuery("FornecedorBean.findByKeyword");
            queryFornecedorByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            fornecedor = queryFornecedorByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Fornecedor não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }

        return fornecedor;
    }

    public List<FornecedorBean> pesquisarCriteria(String termoPesquisa, String status) {
        List<FornecedorBean> fornecedores = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<FornecedorBean> criteria = builder.createQuery(FornecedorBean.class);
        Root<FornecedorBean> root = criteria.from(FornecedorBean.class);

        List<Predicate> predicados = new ArrayList<Predicate>();

        if (!"".equals(termoPesquisa) && termoPesquisa != null) {
            Predicate razao = builder.like(builder.upper(root.get(FornecedorBean_.razaoSocial)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeFantasia = builder.like(builder.upper(root.get(FornecedorBean_.nomeFantasia)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate cnpj = builder.like(builder.upper(root.get(FornecedorBean_.cnpj)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate ie = builder.like(builder.upper(root.get(FornecedorBean_.ie)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate website = builder.like(builder.upper(root.get(FornecedorBean_.website)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate fone = builder.like(builder.upper(root.get(FornecedorBean_.fone)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate fax = builder.like(builder.upper(root.get(FornecedorBean_.fax)), ("%" + termoPesquisa + "%").toUpperCase());

            Predicate soma = builder.or(razao, nomeFantasia, cnpj, ie, website, fone, fax);
            predicados.add(soma);
        }
        if (!"".equals(status) && status != null) {
            Predicate statusP = builder.equal(root.get(FornecedorBean_.status), status);
            predicados.add(statusP);
        }
        
        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        
        Path path = root.get(FornecedorBean_.nomeFantasia);
        Order ordenacao = builder.asc(path);
        criteria.orderBy(ordenacao);

        Query queryProjetoByOwner = em.createQuery(criteria);
        fornecedores = queryProjetoByOwner.getResultList();


        return fornecedores;
    }
}
