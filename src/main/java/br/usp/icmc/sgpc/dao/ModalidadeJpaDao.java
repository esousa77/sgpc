/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.*;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.*;

/**
 *
 * @author david
 */
public class ModalidadeJpaDao extends GenericJpaDao<ModalidadeBean>{

    public ModalidadeJpaDao() {
        super(ModalidadeBean.class);
    }

    public List<ModalidadeBean> pesquisar(String termoPesquisa) {
        List<ModalidadeBean> modalidade = null;
        EntityManager em = getEntityManager();
        try {
            Query queryModalidadeByNome = em.createNamedQuery("ModalidadeBean.findByNome"); //faço um by keyword?
            queryModalidadeByNome.setParameter("nome", "%" + termoPesquisa + "%");
            modalidade = queryModalidadeByNome.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Modalidade não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }

        return modalidade;
    }

    public List<ModalidadeBean> pesquisar(FinanciadorBean financiadorPesquisa, String siglaPesquisa){
        List<ModalidadeBean> modalidades = new ArrayList<ModalidadeBean>();

        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<ModalidadeBean> criteria = builder.createQuery(ModalidadeBean.class);
        Root<ModalidadeBean> root = criteria.from(ModalidadeBean.class);
        Join<ModalidadeBean, TipoAuxilioBean> tipoAuxilio = root.join(ModalidadeBean_.fkTipoAuxilio);
        Join<TipoAuxilioBean, FinanciadorBean> financiador = tipoAuxilio.join(TipoAuxilioBean_.fkFinanciador);

        List<Predicate> predicados = new ArrayList<Predicate>();

        if (!"".equals(siglaPesquisa) && siglaPesquisa != null) {
            Predicate siglaP = builder.equal(builder.upper(root.get(ModalidadeBean_.sigla)), (siglaPesquisa).toUpperCase());
            predicados.add(siglaP);
        }
        if(financiadorPesquisa!=null){
            Predicate financiadorP = builder.equal(tipoAuxilio.get(TipoAuxilioBean_.fkFinanciador), financiador);
            predicados.add(financiadorP);
        }

        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));

        Query queryModalidade = em.createQuery(criteria);
        modalidades = queryModalidade.getResultList();

        return modalidades;
    }

}
