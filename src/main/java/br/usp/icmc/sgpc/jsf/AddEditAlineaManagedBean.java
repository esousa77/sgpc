/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.CategoriaAlineaBean;
import br.usp.icmc.sgpc.beans.CentroDespesaBean;
import br.usp.icmc.sgpc.beans.HistoricoAuxilioBean;
import br.usp.icmc.sgpc.beans.SubcentroDespesasBean;
import br.usp.icmc.sgpc.service.ContabilidadeService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.component.celleditor.CellEditor;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditAlineaMB")
@ViewScoped
public class AddEditAlineaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(AddEditAlineaManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private List<CategoriaAlineaBean> listaCategorias = new ArrayList<CategoriaAlineaBean>();
    private List<CentroDespesaBean> listaSubCentros = new ArrayList<CentroDespesaBean>();
    private AlineaBean alineaBean;
    private SubcentroDespesasBean subcentroExcluir;

    @PostConstruct
    public void postConstruct() {
        listaCategorias = Service.getInstance().listarCategoriaAlinea();
        listaSubCentros = Service.getInstance().listarCentrosDespesa();
        alineaBean = new AlineaBean();
    }
    
    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }
    
    public void prepareAdicionar() {
        this.setCurrentState(ADICIONAR_STATE);
        AuxilioBean auxTemp = userSessionMB.getAuxilio();
        alineaBean = new AlineaBean();
        alineaBean.setFkAuxilio(auxTemp);
        SubcentroDespesasBean subcentro = new SubcentroDespesasBean();
        subcentro.setCentroDistribuidor(true);
        subcentro.setFkCentroDespesa(auxTemp.getFkCentroDespesa());
        subcentro.setFkAlinea(alineaBean);
        alineaBean.setSubcentrosDespesas(new ArrayList<SubcentroDespesasBean>());
        alineaBean.getSubcentrosDespesas().add(subcentro);
        RequestContext.getCurrentInstance().execute("PF('editAlineaPanel').show();");
    }
    
    public void prepareEditar(AlineaBean alinea) {
        this.setCurrentState(EDITAR_STATE);
        this.alineaBean = alinea;
        RequestContext.getCurrentInstance().execute("PF('editAlineaPanel').show();");
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public List<CategoriaAlineaBean> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<CategoriaAlineaBean> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public AlineaBean getAlineaBean() {
        return alineaBean;
    }

    public void setAlineaBean(AlineaBean alineaBean) {
        this.alineaBean = alineaBean;
    }

    public List<CentroDespesaBean> getListaSubCentros() {
        return listaSubCentros;
    }

    public void setListaSubCentros(List<CentroDespesaBean> listaSubCentros) {
        this.listaSubCentros = listaSubCentros;
    }

    public SubcentroDespesasBean getSubcentroExcluir() {
        return subcentroExcluir;
    }

    public void setSubcentroExcluir(SubcentroDespesasBean subcentroExcluir) {
        this.subcentroExcluir = subcentroExcluir;
    }

    public void atualizaValorSubcentroDistribuidor() {
        AuxilioBean auxTemp = userSessionMB.getAuxilio();
        BigDecimal valorAprovado = alineaBean.getVerbaAprovada();
        BigDecimal valorDistribuido = new BigDecimal(0);
        BigDecimal oldValue = alineaBean.getSubcentrosDespesas().get(0).getValor();
        SubcentroDespesasBean sc = null;
        for (int i = 1; i < alineaBean.getSubcentrosDespesas().size(); i++) {
            sc = alineaBean.getSubcentrosDespesas().get(i);
            valorDistribuido = valorDistribuido.add(sc.getValor());
        }

        sc = alineaBean.getSubcentrosDespesas().get(0);
        if (oldValue != valorAprovado.subtract(valorDistribuido)) {
            sc.setValor(valorAprovado.subtract(valorDistribuido));

            if (sc.getId() != null) {
                HistoricoAuxilioBean hist = new HistoricoAuxilioBean();
                hist.setAcao("Alteração de valor de centro distribuidor");
                hist.setDataAlteracao(new Date());
                hist.setNomeAlinea(alineaBean.getFkCategoriaAlinea().getNome());
                hist.setFkAuxilio(auxTemp);
                hist.setFkResponsavel(userSessionMB.getLoggedUser());
                hist.setNomeCentroDespesas(sc.getFkCentroDespesa().getNome());
                if (valorAprovado.subtract(valorDistribuido).compareTo(oldValue) == 1) {
                    hist.setDescricao("Aditivo");
                    hist.setValor(valorAprovado.subtract(valorDistribuido).subtract(oldValue));
                    hist.setOperacao(HistoricoAuxilioBean.OPERACAO_CREDITO);
                } else {
                    hist.setDescricao("Estorno de concessão");
                    hist.setValor(oldValue.add(valorDistribuido).subtract(valorAprovado));
                    hist.setOperacao(HistoricoAuxilioBean.OPERACAO_DEBITO);
                }

                auxTemp.getHistoricosAuxilio().add(hist);
                Service.getInstance().registrarHistoricoAuxilio(hist);
            }
        }

    }

    public void validateValorSubcentro(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        BigDecimal value = BigDecimal.valueOf(Double.valueOf(newValue.toString()));
        BigDecimal oldValue = BigDecimal.valueOf((Double) ((UIInput) component).getValue());
        BigDecimal somaSubcentros = value.subtract(oldValue);
        CellEditor cellEditor = (CellEditor) component.getParent();
        Column column = (Column) cellEditor.getParent();
        DataTable dataTable = (DataTable) column.getParent();

        dataTable.getRowIndex();

        SubcentroDespesasBean sub = (SubcentroDespesasBean) dataTable.getRowData();
        BigDecimal valorConsumidoSubcentro = ContabilidadeService.getInstance().totalDespesaSubcentroPorAlinea(sub);

        if (value.compareTo(valorConsumidoSubcentro)==-1) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor inválido", "O valor do subcentro não pode ser menor do que as despesas já realizadas.");
            facesContext.addMessage(component.getClientId(facesContext), message);
            return;
        }

        if (value.compareTo(BigDecimal.ZERO) == -1) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor negativo", "O valor do subcentro não pode ser negativo.");
            facesContext.addMessage(component.getClientId(facesContext), message);
            return;
        }

        SubcentroDespesasBean sc = null;
        for (int i = 1; i < alineaBean.getSubcentrosDespesas().size(); i++) {
            sc = alineaBean.getSubcentrosDespesas().get(i);
            somaSubcentros = somaSubcentros.add(sc.getValor());
        }
        //double centroDistribuidor = alineaBean.getSubcentrosDespesas().get(0).getValor();
        BigDecimal centroDistribuidor = alineaBean.getVerbaAprovada().subtract(somaSubcentros);
        if (somaSubcentros.add(centroDistribuidor).compareTo(alineaBean.getVerbaAprovada()) ==1) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor acima do limite", "O valor do subcentro supera o valor disponível da alínea.");
            facesContext.addMessage(component.getClientId(facesContext), message);
        } else {
            //component.processUpdates(facesContext);

            DecimalFormat df = new DecimalFormat("###,##0.00");
            String nomeAlinea = "";
            if (alineaBean.getFkCategoriaAlinea() != null) {
                nomeAlinea = alineaBean.getFkCategoriaAlinea().getNome();
            }

            AuxilioBean auxTemp = userSessionMB.getAuxilio();
            if (sub != null) {
                if (sub.getId() != null && value != oldValue) {
                    HistoricoAuxilioBean hist = new HistoricoAuxilioBean();
                    hist.setAcao("Alteração de valor de subcentro");
                    hist.setDataAlteracao(new Date());
                    hist.setNomeAlinea(nomeAlinea);
                    hist.setFkAuxilio(auxTemp);
                    hist.setFkResponsavel(userSessionMB.getLoggedUser());
                    hist.setNomeCentroDespesas(sub.getFkCentroDespesa().getNome());
                    if (value.compareTo(oldValue) == 1) {
                        hist.setDescricao("Aditivo");
                        hist.setValor(value.subtract(oldValue));
                        hist.setOperacao(HistoricoAuxilioBean.OPERACAO_CREDITO);
                    } else {
                        hist.setDescricao("Estorno de concessão");
                        hist.setValor(oldValue.subtract(value));
                        hist.setOperacao(HistoricoAuxilioBean.OPERACAO_DEBITO);
                    }

                    auxTemp.getHistoricosAuxilio().add(hist);
                    Service.getInstance().registrarHistoricoAuxilio(hist);
                }
            }
            if (value != oldValue) {
                sc = alineaBean.getSubcentrosDespesas().get(0);
                sc.setValor(alineaBean.getVerbaAprovada().subtract(somaSubcentros));

                if (sc.getId() != null) {
                    HistoricoAuxilioBean hist = new HistoricoAuxilioBean();
                    hist.setAcao("Alteração de valor de centro distribuidor");
                    hist.setDataAlteracao(new Date());
                    hist.setNomeAlinea(nomeAlinea);
                    hist.setFkAuxilio(auxTemp);
                    hist.setFkResponsavel(userSessionMB.getLoggedUser());
                    hist.setNomeCentroDespesas(sc.getFkCentroDespesa().getNome());
                    if (value.compareTo(oldValue) == -1) {
                        hist.setDescricao("Aditivo");
                        hist.setValor(oldValue.subtract(value));
                        hist.setOperacao(HistoricoAuxilioBean.OPERACAO_CREDITO);
                    } else {
                        hist.setDescricao("Estorno de concessão");
                        hist.setValor(value.subtract(oldValue));
                        hist.setOperacao(HistoricoAuxilioBean.OPERACAO_DEBITO);
                    }

                    auxTemp.getHistoricosAuxilio().add(hist);
                    Service.getInstance().registrarHistoricoAuxilio(hist);
                }
            }
        }
    }

    public void excluirSubcentro() {
        if (subcentroExcluir.getId() != null && !subcentroExcluir.getDespesas().isEmpty()) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Existem despesas associadas a este subcentro", "Existem despesas associadas a este subcentro");
            FacesContext.getCurrentInstance().addMessage("deletesubcentrolink", message);
            return;
        }
        SubcentroDespesasBean subDistr = this.alineaBean.getSubcentrosDespesas().get(0);
        subDistr.setValor(subDistr.getValor().add(subcentroExcluir.getValor()));

        this.alineaBean.getSubcentrosDespesas().remove(subcentroExcluir);

        if (subcentroExcluir.getId() == null) {
            return;
        }
        AuxilioBean auxTemp = userSessionMB.getAuxilio();

        HistoricoAuxilioBean hist = new HistoricoAuxilioBean();
        hist.setAcao("Alteração de valor de centro distribuidor");
        hist.setDataAlteracao(new Date());
        hist.setNomeAlinea(this.alineaBean.getFkCategoriaAlinea().getNome());
        hist.setFkAuxilio(auxTemp);
        hist.setFkResponsavel(userSessionMB.getLoggedUser());
        hist.setNomeCentroDespesas(subDistr.getFkCentroDespesa().getNome());
        hist.setDescricao("Estorno de subcentro excluído");
        hist.setOperacao(HistoricoAuxilioBean.OPERACAO_CREDITO);
        hist.setValor(subcentroExcluir.getValor());
        auxTemp.getHistoricosAuxilio().add(hist);
        Service.getInstance().registrarHistoricoAuxilio(hist);

        hist = new HistoricoAuxilioBean();
        hist.setAcao("Exclusão de subcentro");
        hist.setDataAlteracao(new Date());
        hist.setNomeAlinea(this.alineaBean.getFkCategoriaAlinea().getNome());
        hist.setFkAuxilio(auxTemp);
        hist.setFkResponsavel(userSessionMB.getLoggedUser());
        hist.setNomeCentroDespesas(subcentroExcluir.getFkCentroDespesa().getNome());
        hist.setDescricao("Exclusão subcentro");
        hist.setOperacao(HistoricoAuxilioBean.OPERACAO_DEBITO);
        hist.setValor(subcentroExcluir.getValor());
        auxTemp.getHistoricosAuxilio().add(hist);
        Service.getInstance().registrarHistoricoAuxilio(hist);
        RequestContext.getCurrentInstance().execute("PF('deletePopupSubcentro').hide();");

    }

    public void addSubcentro() {
        SubcentroDespesasBean subcentro = new SubcentroDespesasBean();
        subcentro.setFkAlinea(alineaBean);
        subcentro.setCentroDistribuidor(false);
        alineaBean.getSubcentrosDespesas().add(subcentro);
    }

    public void gravarAlinea() {
        logger.debug("-------- Info AUX 2 ----add alinea--");
        DecimalFormat df = new DecimalFormat("###,##0.00");
        AuxilioBean auxTemp = userSessionMB.getAuxilio();
        if (ADICIONAR_STATE.equals(this.currentState)) {

            auxTemp.getAlineas().add(this.alineaBean);
            Service.getInstance().atualizarStatusAuxilio(auxTemp);
            auxTemp = Service.getInstance().atualizarAuxilio(auxTemp);

            HistoricoAuxilioBean hist = new HistoricoAuxilioBean();
            hist.setAcao("Criação de alínea");
            hist.setDataAlteracao(new Date());
            hist.setNomeAlinea(alineaBean.getFkCategoriaAlinea().getNome());
            hist.setDescricao("Criação de nova alinea.");
            hist.setValor(alineaBean.getVerbaAprovada());
            hist.setFkAuxilio(auxTemp);
            hist.setFkResponsavel(userSessionMB.getLoggedUser());
            Service.getInstance().registrarHistoricoAuxilio(hist);

        } else if (EDITAR_STATE.equals(this.currentState)) {
            Service.getInstance().atualizarAlinea(this.alineaBean);
        }
        RequestContext.getCurrentInstance().execute("PF('editAlineaPanel').hide();");

        for (SubcentroDespesasBean sub : this.alineaBean.getSubcentrosDespesas()) {
            if (sub.getId() == null) {
                HistoricoAuxilioBean hist = new HistoricoAuxilioBean();
                hist.setDataAlteracao(new Date());
                if (sub.isCentroDistribuidor()) {
                    hist.setAcao("Criação de novo centro distribuidor");
                } else {
                    hist.setAcao("Criação de novo subcentro");
                }
                hist.setDescricao("Concessão inicial");
                hist.setValor(sub.getValor());
                hist.setNomeCentroDespesas(sub.getFkCentroDespesa().getNome());
                hist.setOperacao(HistoricoAuxilioBean.OPERACAO_CREDITO);
                hist.setNomeAlinea(this.alineaBean.getFkCategoriaAlinea().getNome());
                hist.setFkAuxilio(auxTemp);
                hist.setFkResponsavel(userSessionMB.getLoggedUser());
                auxTemp.getHistoricosAuxilio().add(hist);
                Service.getInstance().registrarHistoricoAuxilio(hist);
            }
        }
        this.alineaBean = new AlineaBean();
//        atualizaAuxilio();
//        listaAlineasDisponiveis = auxilio.getAlineas();
//        pesquisarHistorico();
    }
    
    public void prepareExcluirSubcentro(SubcentroDespesasBean subc){
        this.subcentroExcluir = subc;
        RequestContext.getCurrentInstance().execute("PF('deletePopupSubcentro').show();");
    }
}
