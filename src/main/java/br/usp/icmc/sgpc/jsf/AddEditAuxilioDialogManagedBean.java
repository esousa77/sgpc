/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.*;
import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.AuditoriaService;
import br.usp.icmc.sgpc.service.ConfiguracaoService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditAuxilioMB")
@ViewScoped
public class AddEditAuxilioDialogManagedBean implements Serializable{
    private static final Logger logger = Logger.getLogger(AddEditAuxilioDialogManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;

    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    
    private ProjetoBean projeto;
    private AuxilioBean auxilio;
    private PessoaBean responsavel;
    private PessoaBean responsavelCC;
    private Integer idContaCorrente;
    private CentroDespesaBean centroDespesasSelecionado;
    private Integer idFinanciador;
    private Integer idTipoAuxilio;
    private Integer idModalidade;
    private DepartamentoBean departamento;
    
    private List<SelectItem> listaFinanciadores = new ArrayList<SelectItem>();
    private List<SelectItem> listaTiposAuxilio = new ArrayList<SelectItem>();
    private List<SelectItem> listaModalidades = new ArrayList<SelectItem>();
    private List<DepartamentoBean> listaDepartamentos = new ArrayList<DepartamentoBean>();
    private List<PessoaBean> listaPessoas = new ArrayList<PessoaBean>();
    private List<PessoaBean> listaPessoasBolsistas = new ArrayList<PessoaBean>();
    private List<SelectItem> listaContaCorrente = new ArrayList<SelectItem>();
    private List<SelectItem> listaCentroDespesa = new ArrayList<SelectItem>();
    private List<ProgramaEspecialFinanciamentoBean> listaProgramasEspeciaisFinanciamento = new ArrayList<ProgramaEspecialFinanciamentoBean>();
    private List<InstituicaoBancariaBean> listaInstituicaoBancaria = new ArrayList<InstituicaoBancariaBean>();
    private List<SelectItem> listaInstituicoes = new ArrayList<SelectItem>();
    private List<SelectItem> listaUnidades = new ArrayList<SelectItem>();
    
    private List<String> listaStatus = new ArrayList<String>();
    private boolean existeProjetoAtivo = false;
    private boolean insereProjetoAtivo = false;
    private boolean indicarBolsista = false;
    private boolean podeAlterarResponsavel = false;
    private boolean podeAlterarStatusAuxilioFinanceiro = false;
    private boolean podeEditarRegistro = false;
    private boolean criarRegistrosTerceiros;
    private boolean criarRegistroProprio;
    
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    
    private Integer idResponsavel;
    private Integer idInstituicaoSelecionada;
    private Integer idUnidadeSelecionada;
    
    private TreeNode rootNode;
    private TreeNode selectedNode;

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        
        //exibirColunaOwner = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.AUXILIO_CONSULTAR_TERCEIROS);

        criarRegistroProprio = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_CRIAR_PROPRIO);
        criarRegistrosTerceiros = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_CRIAR_TERCEIROS);
        podeAlterarStatusAuxilioFinanceiro = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.AUXILIO_ALTERAR_STATUS_FINANCEIRO);
        podeEditarRegistro = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.AUXILIO_EDITAR);
        listaFinanciadores = GenericConverter.createSelectItems(Service.getInstance().listarFinanciadores());
        this.listaProgramasEspeciaisFinanciamento = Service.getInstance().listarProgramaEspecialFinanciamento();
        this.listaCentroDespesa = GenericConverter.createSelectItems(Service.getInstance().listarCentrosDespesa());
        //this.listaContaCorrente = Service.getInstance().pesquisarContasCorrentes(userSessionMB.getLoggedUser());
        this.listaDepartamentos = Service.getInstance().pesquisarDepartamentos();
        listaInstituicaoBancaria = Service.getInstance().listarInstituicoesBancarias();
        listaInstituicaoBancaria.add(0, Service.getInstance().buscarInstituicaoBancaria(43)); // adiciona banco brasil em primeiro da lista

        this.listaPessoasBolsistas = Service.getInstance().pesquisarPessoasAlunos();
        if (criarRegistrosTerceiros && criarRegistroProprio) {
            listaPessoas.clear();
            this.listaPessoas = Service.getInstance().pesquisarPessoasReponsaveis();
        } else if (criarRegistroProprio && !criarRegistrosTerceiros) {
            listaPessoas.clear();
            listaPessoas.add(userSessionMB.getLoggedUser());
        } else if (!criarRegistroProprio && criarRegistrosTerceiros) {
            listaPessoas.clear();
            this.listaPessoas = Service.getInstance().pesquisarPessoasReponsaveis();
            listaPessoas.remove(userSessionMB.getLoggedUser());
        }
        
        listaInstituicoes = GenericConverter.createSelectItems(Service.getInstance().listarInstituicoes());
        
        String siglaUnidadeDefault = ConfiguracaoService.getInstance().getConfiguracao("CONFIG_DEFAULT_UNIDADE").getValor();
        List<UnidadeBean> unidadeDefault = Service.getInstance().buscarUnidade(siglaUnidadeDefault);
        if(!unidadeDefault.isEmpty()){
            idUnidadeSelecionada = unidadeDefault.get(0).getId();
            idInstituicaoSelecionada = unidadeDefault.get(0).getFkInstituicao().getId();
            listaUnidades = GenericConverter.createSelectItems(unidadeDefault.get(0).getFkInstituicao().getUnidades());
        }else if(listaInstituicoes!=null && !listaInstituicoes.isEmpty()){
            idInstituicaoSelecionada = (Integer) listaInstituicoes.get(0).getValue();
            listaUnidades = GenericConverter.createSelectItems(Service.getInstance().buscarInstituicao(idInstituicaoSelecionada).getUnidades());
            if(listaUnidades!=null && !listaUnidades.isEmpty()){
                idUnidadeSelecionada = (Integer) listaUnidades.get(0).getValue();
            }
        }
        criarNodesArvore();

    }

    public AuxilioBean getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(AuxilioBean auxilio) {
        this.auxilio = auxilio;
    }

    public CentroDespesaBean getCentroDespesasSelecionado() {
        return centroDespesasSelecionado;
    }

    public void setCentroDespesasSelecionado(CentroDespesaBean centroDespesasSelecionado) {
        this.centroDespesasSelecionado = centroDespesasSelecionado;
    }

    public Integer getIdContaCorrente() {
        return idContaCorrente;
    }

    public void setIdContaCorrente(Integer idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public DepartamentoBean getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoBean departamento) {
        this.departamento = departamento;
    }

    public boolean isExisteProjetoAtivo() {
        return existeProjetoAtivo;
    }

    public void setExisteProjetoAtivo(boolean existeProjetoAtivo) {
        this.existeProjetoAtivo = existeProjetoAtivo;
    }

    public Integer getIdFinanciador() {
        return idFinanciador;
    }

    public void setIdFinanciador(Integer idFinanciador) {
        this.idFinanciador = idFinanciador;
    }

    public boolean isIndicarBolsista() {
        return indicarBolsista;
    }

    public void setIndicarBolsista(boolean indicarBolsista) {
        this.indicarBolsista = indicarBolsista;
    }

    public boolean isInsereProjetoAtivo() {
        return insereProjetoAtivo;
    }

    public void setInsereProjetoAtivo(boolean insereProjetoAtivo) {
        this.insereProjetoAtivo = insereProjetoAtivo;
    }

    public List<SelectItem> getListaCentroDespesa() {
        return listaCentroDespesa;
    }

    public void setListaCentroDespesa(List<SelectItem> listaCentroDespesa) {
        this.listaCentroDespesa = listaCentroDespesa;
    }

    public List<SelectItem> getListaContaCorrente() {
        atualizaContasCorrentes();
        return listaContaCorrente;
    }

    public void setListaContaCorrente(List<SelectItem> listaContaCorrente) {
        this.listaContaCorrente = listaContaCorrente;
    }

    public List<DepartamentoBean> getListaDepartamentos() {
        return listaDepartamentos;
    }

    public void setListaDepartamentos(List<DepartamentoBean> listaDepartamentos) {
        this.listaDepartamentos = listaDepartamentos;
    }

    public List<SelectItem> getListaFinanciadores() {
        return listaFinanciadores;
    }

    public void setListaFinanciadores(List<SelectItem> listaFinanciadores) {
        this.listaFinanciadores = listaFinanciadores;
    }

    public List<SelectItem> getListaModalidades() {
        return listaModalidades;
    }

    public void setListaModalidades(List<SelectItem> listaModalidades) {
        this.listaModalidades = listaModalidades;
    }

    public List<PessoaBean> getListaPessoasBolsistas() {
        return listaPessoasBolsistas;
    }

    public void setListaPessoasBolsistas(List<PessoaBean> listaPessoasBolsistas) {
        this.listaPessoasBolsistas = listaPessoasBolsistas;
    }

    public List<ProgramaEspecialFinanciamentoBean> getListaProgramasEspeciaisFinanciamento() {
        return listaProgramasEspeciaisFinanciamento;
    }

    public void setListaProgramasEspeciaisFinanciamento(List<ProgramaEspecialFinanciamentoBean> listaProgramasEspeciaisFinanciamento) {
        this.listaProgramasEspeciaisFinanciamento = listaProgramasEspeciaisFinanciamento;
    }

    public List<String> getListaStatus() {
        return listaStatus;
    }

    public void setListaStatus(List<String> listaStatus) {
        this.listaStatus = listaStatus;
    }

    public List<SelectItem> getListaTiposAuxilio() {
        return listaTiposAuxilio;
    }

    public void setListaTiposAuxilio(List<SelectItem> listaTiposAuxilio) {
        this.listaTiposAuxilio = listaTiposAuxilio;
    }

    public Integer getIdModalidade() {
        return idModalidade;
    }

    public void setIdModalidade(Integer idModalidade) {
        this.idModalidade = idModalidade;
    }

    public PessoaBean getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(PessoaBean responsavel) {
        this.responsavel = responsavel;
    }

    public Integer getIdTipoAuxilio() {
        return idTipoAuxilio;
    }

    public void setIdTipoAuxilio(Integer idTipoAuxilio) {
        this.idTipoAuxilio = idTipoAuxilio;
    }

    public boolean isPodeAlterarResponsavel() {
        return podeAlterarResponsavel;
    }

    public void setPodeAlterarResponsavel(boolean podeAlterarResponsavel) {
        this.podeAlterarResponsavel = podeAlterarResponsavel;
    }

    public List<PessoaBean> getListaPessoas() {
        return listaPessoas;
    }

    public void setListaPessoas(List<PessoaBean> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }

    public boolean isPodeAlterarStatusAuxilioFinanceiro() {
        return podeAlterarStatusAuxilioFinanceiro;
    }

    public void setPodeAlterarStatusAuxilioFinanceiro(boolean podeAlterarStatusAuxilioFinanceiro) {
        this.podeAlterarStatusAuxilioFinanceiro = podeAlterarStatusAuxilioFinanceiro;
    }

    public ProjetoBean getProjeto() {
        return projeto;
    }

    public void setProjeto(ProjetoBean projeto) {
        this.projeto = projeto;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public boolean isCriarRegistroProprio() {
        return criarRegistroProprio;
    }

    public void setCriarRegistroProprio(boolean criarRegistroProprio) {
        this.criarRegistroProprio = criarRegistroProprio;
    }

    public boolean isCriarRegistrosTerceiros() {
        return criarRegistrosTerceiros;
    }

    public void setCriarRegistrosTerceiros(boolean criarRegistrosTerceiros) {
        this.criarRegistrosTerceiros = criarRegistrosTerceiros;
    }

    public List<InstituicaoBancariaBean> getListaInstituicaoBancaria() {
        return listaInstituicaoBancaria;
    }

    public void setListaInstituicaoBancaria(List<InstituicaoBancariaBean> listaInstituicaoBancaria) {
        this.listaInstituicaoBancaria = listaInstituicaoBancaria;
    }

    public boolean isPodeEditarRegistro() {
        return podeEditarRegistro;
    }

    public void setPodeEditarRegistro(boolean podeEditarRegistro) {
        this.podeEditarRegistro = podeEditarRegistro;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }
    
    
    public void validateNumeroUsp(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        if(newValue!=null && !"".equalsIgnoreCase((String)newValue) && ADICIONAR_STATE.equalsIgnoreCase(this.currentState)){
            List<AuxilioBean> listaAux = Service.getInstance().pesquisarProtocolo((String)newValue);
            if(!listaAux.isEmpty()){
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"O número de processo USP digitado já existe.","O número de processo USP digitado já existe.");
                facesContext.addMessage(component.getClientId(facesContext), message);
            }
        }
    }

    public void validateNumeroMercurio(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        if(newValue!=null && !"".equalsIgnoreCase((String)newValue) && ADICIONAR_STATE.equalsIgnoreCase(this.currentState)){
            List<AuxilioBean> listaAux = Service.getInstance().pesquisarNumeroMercurio((String)newValue);
            if(!listaAux.isEmpty()){
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"O número Mercúrio digitado já existe.","O número Mercúrio digitado já existe.");
                facesContext.addMessage(component.getClientId(facesContext), message);
            }
        }
    }

    public void validateProtocoloFinanciador(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        if(newValue!=null && !"".equalsIgnoreCase((String)newValue) && ADICIONAR_STATE.equalsIgnoreCase(this.currentState)){
            List<AuxilioBean> listaAux = Service.getInstance().pesquisarProtocoloFinanciador((String)newValue);
            if(!listaAux.isEmpty()){
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"O número de Processo Entidade digitado já existe.","O número de Processo Entidade digitado já existe.");
                facesContext.addMessage(component.getClientId(facesContext), message);
            }
        }
    }
    
    public void atualizaContasCorrentes(){
        listaContaCorrente.clear();
        if(responsavel!=null){
            this.responsavel = Service.getInstance().buscarPessoa(idResponsavel);
            this.responsavelCC = Service.getInstance().buscarPessoa(idResponsavel);
            List<ContaCorrenteBean> contasAtivas = new ArrayList<ContaCorrenteBean>();
            for(ContaCorrenteBean cc : responsavelCC.getContasCorrentes()){
                if(cc.isAtiva()) contasAtivas.add(cc);
            }
            listaContaCorrente = GenericConverter.createSelectItems(contasAtivas);
        }
    }

    public PessoaBean getResponsavelCC() {
        return responsavelCC;
    }

    public void setResponsavelCC(PessoaBean responsavelCC) {
        this.responsavelCC = responsavelCC;
    }
    
    public void clickCheckBox(ValueChangeEvent event){
        //this.idContaCorrente=null;
        this.indicarBolsista = (Boolean) event.getNewValue();
        //atualizaContasCorrentes();
    }

    public void atualizaTipoAuxilio() {
        this.listaTiposAuxilio.clear();
        if(idFinanciador!=null){
            FinanciadorBean tmp = Service.getInstance().buscarFinanciador(idFinanciador);
            listaTiposAuxilio = GenericConverter.createSelectItems(tmp.getTiposAuxilio());
        }
        idTipoAuxilio = null;
        atualizaModalidade();
    }

    public void atualizaModalidade() {
        this.listaModalidades.clear();
        if(idTipoAuxilio!=null){
            TipoAuxilioBean tmp = Service.getInstance().buscarTiposAuxilio(idTipoAuxilio);
            for(ModalidadeBean tmp2:tmp.getModalidades()){
                String labelExibicao = tmp2.getNome();
                if(labelExibicao.length()>50) labelExibicao = labelExibicao.substring(0,40);
                this.listaModalidades.add(new SelectItem(tmp2.getId(),labelExibicao));
            }
        }
        idModalidade = null;
    }

    public void atualizaDepartamento() {
        //this.responsavel = Service.getInstance().buscarPessoa(idResponsavel);
        this.departamento = this.responsavel.getFkDepartamento();
        atualizaContasCorrentes();
    }
    
    public void prepareAdicionar() {
        logger.debug("=================== PREPARE ADICIONAR ===================");

        listaContaCorrente.clear();
        this.setCurrentState(ADICIONAR_STATE);
        podeAlterarResponsavel = true;
        responsavel = null;
        departamento = null;
        idContaCorrente = null;
        idModalidade = null;
        idTipoAuxilio = null;
        idFinanciador = null;
        idResponsavel = null;
        auxilio=new AuxilioBean();
        auxilio.setTipo("auxilios");
        this.listaTiposAuxilio.clear();
        this.listaModalidades.clear();
        centroDespesasSelecionado = null;
        
        RequestContext.getCurrentInstance().execute("PF('editAdicionaAuxilio').show();");
    }
    
    public void prepareAdicionarProjeto(){
        logger.debug("=================== PREPARE ADICIONAR PROJETO===================");
        this.setCurrentState(ADICIONAR_STATE);
        auxilio=new AuxilioBean();
        auxilio.setTipo("auxilios");
        this.listaTiposAuxilio.clear();
        this.listaModalidades.clear();
        
        podeAlterarResponsavel = true;
        insereProjetoAtivo = true;
        responsavelCC = projeto.getFkResponsavel();
        responsavel = projeto.getFkResponsavel();
        idResponsavel = projeto.getFkResponsavel().getId();
        departamento = responsavel.getFkDepartamento();
        atualizaContasCorrentes();
    }
    
    public void prepareEditar(AuxilioBean aux) {
        this.setCurrentState(EDITAR_STATE);
        this.auxilio = aux;
        this.responsavel = auxilio.getFkResponsavel();
        this.responsavelCC = auxilio.getFkResponsavel();
        idResponsavel = responsavel.getId();
        this.departamento = auxilio.getFkProjeto().getFkDepartamento();
        idFinanciador = auxilio.getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getId();
        atualizaTipoAuxilio();
        idTipoAuxilio = auxilio.getFkModalidade().getFkTipoAuxilio().getId();
        
////        this.listaTiposAuxilio.clear();
////        for(TipoAuxilioBean tmp:auxilio.getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getTiposAuxilio()){
////            this.listaTiposAuxilio.add(new SelectItem(tmp.getId(),tmp.getNome()));
////        }
        atualizaModalidade();
        
////        this.listaModalidades.clear();
////        for(ModalidadeBean tmp:auxilio.getFkModalidade().getFkTipoAuxilio().getModalidades()){
////            this.listaModalidades.add(new SelectItem(tmp.getId(),tmp.getNome()));
////        }
        idModalidade = auxilio.getFkModalidade().getId();
        
        atualizaContasCorrentes();
        if(auxilio.getFkContaCorrente()!=null){
            idContaCorrente = auxilio.getFkContaCorrente().getId();
        }else{
            idContaCorrente = null;
        }
        this.centroDespesasSelecionado = auxilio.getFkCentroDespesa();
        this.idUnidadeSelecionada = centroDespesasSelecionado.getFkUnidade().getId();
        this.idInstituicaoSelecionada = centroDespesasSelecionado.getFkUnidade().getFkInstituicao().getId();
        this.podeAlterarResponsavel = true;
        this.indicarBolsista = auxilio.getFkBolsista()!=null;
        criarNodesArvore();
        
        RequestContext.getCurrentInstance().execute("PF('editAdicionaAuxilio').show();");
    }
    
    public String gravarAuxilio(boolean mudarDataProjeto, boolean alterarCentroAuxilio, boolean alterarCentroDespesasVinculadas) {
        logger.debug("--Auxilio ----gravarAuxilio--");

        if (this.auxilio.getDataInicial()!= null && this.auxilio.getDataFinal() != null && this.auxilio.getDataInicial().after(this.auxilio.getDataFinal())) {
            String message = "Data Inicial do Auxílio não pode ser depois da Data Final do Auxílio";
            FacesContext.getCurrentInstance().addMessage("erroInfoProjeto", new FacesMessage(FacesMessage.SEVERITY_ERROR,message,message));
        }
        if(this.auxilio.getFkProjeto() != null){
            if (this.auxilio.getDataInicial() != null && this.auxilio.getDataInicial().after(this.auxilio.getFkProjeto().getDataFinal())){
                String message = "Data Inicial do Auxílio não pode ser posterior à Data Final do Projeto";
                FacesContext.getCurrentInstance().addMessage("erroInfoProjeto", new FacesMessage(FacesMessage.SEVERITY_ERROR,message,message));
            }
        }
        if(centroDespesasSelecionado == null){
            String message = "É necessário indicar Centro de Despesas";
            FacesContext.getCurrentInstance().addMessage("treeCentrosDesp", new FacesMessage(FacesMessage.SEVERITY_ERROR,message,message));
        }
        if(FacesContext.getCurrentInstance().getMessageList().isEmpty()){
            auxilio.setFkModalidade(Service.getInstance().buscarModalidade(idModalidade));
            auxilio.setFkResponsavel(responsavel);
            if(idContaCorrente!=null){
                auxilio.setFkContaCorrente(Service.getInstance().buscarContaCorrente(idContaCorrente));
            }else{
                auxilio.setFkContaCorrente(null);
            }

            if(!indicarBolsista){
                auxilio.setFkBolsista(null);
            }

            if (ADICIONAR_STATE.equals(this.currentState)) {
                Service.getInstance().atualizarStatusAuxilio(auxilio);
                auxilio.setFkCentroDespesa(centroDespesasSelecionado);

                if (insereProjetoAtivo) {
                    projeto = userSessionMB.getProjeto();
                    auxilio.setFkProjeto(projeto);
                    projeto.getAuxilios().add(auxilio);
                    Service.getInstance().atualizarStatusProjeto(projeto);
                    Service.getInstance().atualizarProjeto(projeto);
                    AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Adicionar Registro", "Auxilio", "Adicionou auxílio no projeto de id: " + this.projeto.getId() + " - Título: " + this.projeto.getTitulo() + " - Auxílio de nome: " + auxilio.getNomeIdentificacao(), ConfigConstantes.CONFIG_AUDITORIA_ADICIONAR_AUXILIO);
                    atualizarProjetoSessao();
                } else {

                    projeto = new PesquisaBean();
                    projeto.setDataCriacao(new Date());
                    projeto.setDataInicial(auxilio.getDataInicial());
                    projeto.setDataFinal(auxilio.getDataFinal());
                    projeto.setDescricao(auxilio.getNomeIdentificacao());
                    projeto.setTitulo(auxilio.getNomeIdentificacao());
                    Service.getInstance().atualizarStatusProjeto(projeto);
                    projeto.setFkResponsavel(responsavel);
                    projeto.setFkDepartamento(departamento);

                    logger.debug("--Auxilio ----gravando Auxilio........");
                    projeto = Service.getInstance().cadastraProjeto(projeto);
                    auxilio.setFkProjeto(projeto);
//                    auxilio = Service.getInstance().cadastraAuxilio(auxilio);
                    this.projeto.getAuxilios().add(auxilio);
                    projeto = Service.getInstance().atualizarProjeto(projeto);

                    AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Adicionar Registro", "Auxilio", "Adicionou auxílio no projeto de id: " + this.projeto.getId() + " - Título: " + this.projeto.getTitulo() + " - Auxílio de nome: " + auxilio.getNomeIdentificacao(), ConfigConstantes.CONFIG_AUDITORIA_ADICIONAR_AUXILIO);
                }
                userSessionMB.setProjeto(projeto);
                userSessionMB.setAuxilio(projeto.getAuxilios().get(projeto.getAuxilios().size()-1));

                //return "infoAuxilio?faces-redirect=true&idAuxilioSelecionado="+projeto.getAuxilios().get(0).getId();
                return "infoAuxilio?faces-redirect=true";
            } else if (EDITAR_STATE.equals(this.currentState)) {
                if (this.auxilio.getFkProjeto() != null) {
                    boolean temErro = false;
                    if (this.auxilio.getDataFinal() != null && this.auxilio.getDataFinal().after(this.auxilio.getFkProjeto().getDataFinal())) {
                        if (mudarDataProjeto) {
                            this.auxilio.getFkProjeto().setDataFinal(this.auxilio.getDataFinal());
                        } else {
                            String message = "Data Final do Auxílio não pode ser posterior à Data Final do Projeto";
                            FacesContext.getCurrentInstance().addMessage("erroDatasProjeto", new FacesMessage(FacesMessage.SEVERITY_ERROR,message,message));
                            temErro = true;
                        }
                    }
                    if (this.auxilio.getDataInicial() != null && this.auxilio.getDataInicial().before(this.auxilio.getFkProjeto().getDataInicial())) {
                        if (mudarDataProjeto) {
                            this.auxilio.getFkProjeto().setDataInicial(this.auxilio.getDataInicial());
                        } else {
                            String message = "Data Inicial do Auxílio não pode ser anterior à Data Final do Projeto";
                            FacesContext.getCurrentInstance().addMessage("erroDatasProjeto", new FacesMessage(FacesMessage.SEVERITY_ERROR,message,message));
                            temErro = true;
                        }
                    }
                    if(temErro){
                        RequestContext.getCurrentInstance().execute("PF('estenderDatasProjetoDialog').show();");
                        return null;
                    }
                }
                if (auxilio.getFkCentroDespesa() == null || auxilio.getFkCentroDespesa().getId().intValue() != centroDespesasSelecionado.getId().intValue()) {
                    if (alterarCentroAuxilio) {
                        auxilio.setFkCentroDespesa(centroDespesasSelecionado);
                    } else if (alterarCentroDespesasVinculadas) {
                        for (AlineaBean alinea : auxilio.getAlineas()) {
                            for (SubcentroDespesasBean subcentro : alinea.getSubcentrosDespesas()) {
                                if (subcentro.isCentroDistribuidor()) {
                                    subcentro.setFkCentroDespesa(centroDespesasSelecionado);
                                }
                            }
                        }
                    } else {
                        RequestContext.getCurrentInstance().execute("PF('mudarCentroDespesasDialog').show();");
                        return null;
                    }
                }
                
                projeto = auxilio.getFkProjeto();//userSessionMB.getProjeto();
                projeto.setFkDepartamento(departamento);
                Service.getInstance().atualizarStatusAuxilio(auxilio);
                Service.getInstance().atualizarAuxilio(auxilio);
                Service.getInstance().atualizarProjeto(projeto);
                AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Atualizar Registro", "Auxilio", "Atualizou auxílio no projeto de id: " + this.projeto.getId() + " - Título: " + this.projeto.getTitulo() + " - Auxílio de nome: " + auxilio.getNomeIdentificacao(), ConfigConstantes.CONFIG_AUDITORIA_EDITAR_AUXILIO);
                atualizarProjetoSessao();
            }
            RequestContext.getCurrentInstance().execute("PF('editAdicionaAuxilio').hide();");
            RequestContext.getCurrentInstance().execute("PF('estenderDatasProjetoDialog').hide();");
            RequestContext.getCurrentInstance().execute("PF('mudarCentroDespesasDialog').hide();");
        }
        return null;
    }
    
    public void atualizarProjetoSessao() {
        projeto = Service.getInstance().pesquisaProjeto(projeto.getId());
        userSessionMB.setProjeto(this.projeto);
    }
    
    public List<SelectItem> getListaPessoasSelectItem(){
        return GenericConverter.createSelectItems(listaPessoas);
        
//        List<SelectItem> listaSelectItens = new ArrayList<SelectItem>();
//        for(SgpcBeanInterface obj : listaPessoas){
//            listaSelectItens.add(new SelectItem(obj.getId(), obj.toString()));
//        }
//        return listaSelectItens;
    }

    public Integer getIdResponsavel() {
        if(responsavel!= null) this.idResponsavel = responsavel.getId();
        return idResponsavel;
    }

    public void setIdResponsavel(Integer idResponsavel) {
        this.idResponsavel = idResponsavel;
        this.responsavel = Service.getInstance().buscarPessoa(idResponsavel);
    }

    public Integer getIdInstituicaoSelecionada() {
        return idInstituicaoSelecionada;
    }

    public void setIdInstituicaoSelecionada(Integer idInstituicaoSelecionada) {
        this.idInstituicaoSelecionada = idInstituicaoSelecionada;
    }

    public Integer getIdUnidadeSelecionada() {
        return idUnidadeSelecionada;
    }

    public void setIdUnidadeSelecionada(Integer idUnidadeSelecionada) {
        this.idUnidadeSelecionada = idUnidadeSelecionada;
    }

    public List<SelectItem> getListaInstituicoes() {
        return listaInstituicoes;
    }

    public void setListaInstituicoes(List<SelectItem> listaInstituicoes) {
        this.listaInstituicoes = listaInstituicoes;
    }

    public List<SelectItem> getListaUnidades() {
        return listaUnidades;
    }

    public void setListaUnidades(List<SelectItem> listaUnidades) {
        this.listaUnidades = listaUnidades;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }
    
//    public void onNodeSelect(NodeSelectEvent event) {
//        logger.debug("---=== SELECIONOU!!! " + event.getTreeNode().toString());
//        this.centroDespesasSelecionado = (CentroDespesaBean) event.getTreeNode().getData();
//    }
    public void criarNodesArvore(){
        HashMap<Integer,TreeNode> hash = new HashMap<Integer,TreeNode>();
        rootNode = new DefaultTreeNode("root", null);
        if(idInstituicaoSelecionada!=null && idInstituicaoSelecionada!=0){
            if(idUnidadeSelecionada != null && idUnidadeSelecionada != 0){
                UnidadeBean unid = Service.getInstance().buscarUnidade(idUnidadeSelecionada);
                
                for(CentroDespesaBean cdb :  unid.getCentrosDespesas()){
                    if(cdb.getCentroDespesasPai()==null){
                        hash.put(cdb.getId(), new DefaultTreeNode(cdb, rootNode));
                    }else{
                        TreeNode nodePai = hash.get(cdb.getCentroDespesasPai().getId());
                        hash.put(cdb.getId(), new DefaultTreeNode(cdb, nodePai));
                    }
                }
                TreeNode expand = rootNode;
                if(this.centroDespesasSelecionado != null){
                    expand =  hash.get(this.centroDespesasSelecionado.getId());
                    if(expand!=null){
                    expand.setSelected(true);
                        while(expand != null){
                            expand.setExpanded(true);
                            expand = expand.getParent();
                        }
                    }
                }
            }else{
                listaUnidades = GenericConverter.createSelectItems(Service.getInstance().buscarInstituicao(idInstituicaoSelecionada).getUnidades());
            }
        }else{
            idUnidadeSelecionada=null;
            listaUnidades = new ArrayList<SelectItem>();
        }
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
        if(selectedNode != null){
            this.centroDespesasSelecionado = (CentroDespesaBean) selectedNode.getData();
        }else{
            this.centroDespesasSelecionado = null;
        }
        //logger.debug("---=== SELECIONOU!!! " + this.centroDespesasSelecionado);
    }
    
}
