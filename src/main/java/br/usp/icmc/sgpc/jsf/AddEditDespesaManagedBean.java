/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.ArquivoAnexoDespesaBean;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.CategoriaAlineaBean;
import br.usp.icmc.sgpc.beans.CentroDespesaBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoDespesaBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoPagamentoBean;
import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.NotaFiscalBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PrestacaoContasBean;
import br.usp.icmc.sgpc.beans.ReciboBean;
import br.usp.icmc.sgpc.beans.SubcentroDespesasBean;
import br.usp.icmc.sgpc.service.ContabilidadeService;
import br.usp.icmc.sgpc.service.Service;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditDespesaMB")
@ViewScoped
public class AddEditDespesaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(AddEditDespesaManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private List<CategoriaAlineaBean> listaCategorias = new ArrayList<CategoriaAlineaBean>();
    private List<AlineaBean> listaAlineasDisponiveis = new ArrayList<AlineaBean>();
    private List<SubcentroDespesasBean> listaSubcentrosDespesa = new ArrayList<SubcentroDespesasBean>();
    private DespesaBean despesa;
    private FornecedorBean fornecedorSelecionado;
    private ArquivoAnexoDespesaBean arquivoAnexo;
    private List<FornecedorBean> listaFornecedores = new ArrayList<FornecedorBean>();
    private DocumentoComprovacaoPagamentoBean documentoPagto;
    private BigDecimal saldoDisponivel;
    private String moedaDespesa;
    private List<String> listaStatusDespesa = new ArrayList<String>();
    private List<String> listaTiposDespesa = new ArrayList<String>();
    private List<CentroDespesaBean> listaCentrosDespesa;
    private AlineaBean alineaBean = new AlineaBean();
    private DocumentoComprovacaoDespesaBean documentoDespesa;
    private AuxilioBean auxilio;
    
    private List<PessoaBean> listaPessoas = new ArrayList<PessoaBean>();
    private PessoaBean pessoaSelecionada;
    

    @PostConstruct
    public void postConstruct() {
        despesa = new DespesaBean();
        auxilio = userSessionMB.getAuxilio();
        
        listaCategorias = Service.getInstance().listarCategoriaAlinea();
        listaFornecedores = Service.getInstance().listarFornecedor();
        listaCentrosDespesa = Service.getInstance().listarCentrosDespesa();

        listaStatusDespesa.add("Em Elaboração");
        listaStatusDespesa.add("Aguardando Entrega");
        listaStatusDespesa.add("Em Validação");
        listaStatusDespesa.add("Finalizada");
        
        listaTiposDespesa.add("CUSTEIO");
        listaTiposDespesa.add("CAPITAL");
        listaTiposDespesa.add("RESERVA TÉCNICA");
        listaTiposDespesa.add("BENEFICIO COMPLEMENTAR");
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public void prepareAdicionar() {
        despesa = new DespesaBean();
    }

    public void prepareEditar() {
        this.setCurrentState(EDITAR_STATE);
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public List<CategoriaAlineaBean> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<CategoriaAlineaBean> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public List<AlineaBean> getListaAlineasDisponiveis() {
        return listaAlineasDisponiveis;
    }

    public void setListaAlineasDisponiveis(List<AlineaBean> listaAlineasDisponiveis) {
        this.listaAlineasDisponiveis = listaAlineasDisponiveis;
    }

    public DespesaBean getDespesa() {
        return despesa;
    }

    public void setDespesa(DespesaBean despesa) {
        this.despesa = despesa;
    }

    public List<SubcentroDespesasBean> getListaSubcentrosDespesa() {
        return listaSubcentrosDespesa;
    }

    public void setListaSubcentrosDespesa(List<SubcentroDespesasBean> listaSubcentrosDespesa) {
        this.listaSubcentrosDespesa = listaSubcentrosDespesa;
    }

    public FornecedorBean getFornecedorSelecionado() {
        return fornecedorSelecionado;
    }

    public void setFornecedorSelecionado(FornecedorBean fornecedorSelecionado) {
        this.fornecedorSelecionado = fornecedorSelecionado;
    }

    public ArquivoAnexoDespesaBean getArquivoAnexo() {
        return arquivoAnexo;
    }

    public void setArquivoAnexo(ArquivoAnexoDespesaBean arquivoAnexo) {
        this.arquivoAnexo = arquivoAnexo;
    }

    public List<FornecedorBean> getListaFornecedores() {
        listaFornecedores = Service.getInstance().listarFornecedor();
        return listaFornecedores;
    }

    public void setListaFornecedores(List<FornecedorBean> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }

    public DocumentoComprovacaoPagamentoBean getDocumentoPagto() {
        return documentoPagto;
    }

    public void setDocumentoPagto(DocumentoComprovacaoPagamentoBean documentoPagto) {
        this.documentoPagto = documentoPagto;
    }

    public BigDecimal getSaldoDisponivel() {
        return saldoDisponivel;
    }

    public void setSaldoDisponivel(BigDecimal saldoDisponivel) {
        this.saldoDisponivel = saldoDisponivel;
    }

    public String getMoedaDespesa() {
        return moedaDespesa;
    }

    public void setMoedaDespesa(String moedaDespesa) {
        this.moedaDespesa = moedaDespesa;
    }

    public List<String> getListaStatusDespesa() {
        return listaStatusDespesa;
    }

    public void setListaStatusDespesa(List<String> listaStatusDespesa) {
        this.listaStatusDespesa = listaStatusDespesa;
    }

    public List<String> getListaTiposDespesa() {
        return listaTiposDespesa;
    }

    public void setListaTiposDespesa(List<String> listaTiposDespesa) {
        this.listaTiposDespesa = listaTiposDespesa;
    }

    public List<CentroDespesaBean> getListaCentrosDespesa() {
        return listaCentrosDespesa;
    }

    public void setListaCentrosDespesa(List<CentroDespesaBean> listaCentrosDespesa) {
        this.listaCentrosDespesa = listaCentrosDespesa;
    }

    public AlineaBean getAlineaBean() {
        return alineaBean;
    }

    public void setAlineaBean(AlineaBean alineaBean) {
        this.alineaBean = alineaBean;
    }

    public DocumentoComprovacaoDespesaBean getDocumentoDespesa() {
        return documentoDespesa;
    }

    public void setDocumentoDespesa(DocumentoComprovacaoDespesaBean documentoDespesa) {
        this.documentoDespesa = documentoDespesa;
    }

    public void atualizarListaAlineas(ValueChangeEvent event) {
        despesa.setTipoDespesa((String) event.getNewValue());

        if ("CAPITAL".equalsIgnoreCase(despesa.getTipoDespesa())) {
            listaAlineasDisponiveis = Service.getInstance().listarAlineaCapital(auxilio);
        }
        if ("CUSTEIO".equalsIgnoreCase(despesa.getTipoDespesa())) {
            listaAlineasDisponiveis = Service.getInstance().listarAlineaCorrente(auxilio);
        }
        if ("RESERVA TÉCNICA".equalsIgnoreCase(despesa.getTipoDespesa())) {
            listaAlineasDisponiveis = Service.getInstance().listarAlineaReservaTecnica(auxilio);
        }
        if ("BENEFICIO COMPLEMENTAR".equalsIgnoreCase(despesa.getTipoDespesa())) {
            listaAlineasDisponiveis = Service.getInstance().listarAlineaBeneficioComplementar(auxilio);
        }
        Collections.sort(listaAlineasDisponiveis,AlineaBean.POR_NOME_CATEGORIA);
        saldoDisponivel = new BigDecimal(0);
        moedaDespesa = "";
    }

    public void prepareEditarDespesa(DespesaBean despesa) {
        logger.debug("--InfoProjeto ----Editar Despesa--");
        this.despesa = despesa;
        this.auxilio = userSessionMB.getAuxilio();
        this.alineaBean = this.despesa.getFkAlinea();
        listaAlineasDisponiveis = Service.getInstance().buscarAlineasAuxilio(this.auxilio);
        Collections.sort(listaAlineasDisponiveis,AlineaBean.POR_NOME_CATEGORIA);
        fornecedorSelecionado = despesa.getFkFornecedor();
        pessoaSelecionada = despesa.getFkPessoa();
        this.setCurrentState(EDITAR_STATE);
        //saldoDisponivel = ContabilidadeService.getInstance().saldoAlinea(alineaBean);
        saldoDisponivel = ContabilidadeService.getInstance().saldoSubcentro(this.despesa.getFkSubcentro());
        if (alineaBean.getMoeda() == 1) {
            moedaDespesa = "R$";
        } else {
            moedaDespesa = "U$";
        }

        this.listaSubcentrosDespesa = alineaBean.getSubcentrosDespesas();
        RequestContext.getCurrentInstance().execute("PF('editDespesas').show();");
    }

    public void prepareCadastrarDespesa2() {
        logger.debug("--InfoProjeto ----Cadastrar Despesa--");
        this.auxilio = userSessionMB.getAuxilio();
        this.despesa = new DespesaBean();
        saldoDisponivel = new BigDecimal(0);
        moedaDespesa = "";
        this.alineaBean = null;
        listaAlineasDisponiveis = new ArrayList<AlineaBean>();//Service.getInstance().buscarAlineasAuxilio(this.auxilio);
        Collections.sort(listaAlineasDisponiveis,AlineaBean.POR_NOME_CATEGORIA);
        this.setCurrentState(ADICIONAR_STATE);
        this.fornecedorSelecionado = null;
        this.pessoaSelecionada = null;

        this.despesa.setDataRealizada(new Date());

        listaSubcentrosDespesa = new ArrayList<SubcentroDespesasBean>();
        RequestContext.getCurrentInstance().execute("PF('selectTipoDocDespesa').show();");
    }

    public void atualizarListaSubcentros() {
        //saldoDisponivel = ContabilidadeService.getInstance().saldoSubcentro(alineaBean.getSubcentrosDespesas().get(0));
        saldoDisponivel = new BigDecimal(0);

        if (alineaBean == null) {
            return;
        }
        if (alineaBean.getMoeda() == 1) {
            moedaDespesa = "R$";
        } else {
            moedaDespesa = "U$";
        }
        this.listaSubcentrosDespesa = alineaBean.getSubcentrosDespesas();

        if (despesa == null) {
            return;
        }
        this.despesa.setFkSubcentro(null);
    }

    public void adicionarDespesa(boolean aceiteDocumentoFiscalDuplicado) {
        BigDecimal valorSomaComprovantes = new BigDecimal(0);
        BigDecimal valorSomaCheques = new BigDecimal(0);

        boolean temErro = false;

        for (DocumentoComprovacaoDespesaBean docTmp : despesa.getDocumentosComprovacaoDespesas()) {
            valorSomaComprovantes = valorSomaComprovantes.add(docTmp.getValor());
        }
        
        Date dataUltimaPrestacaoContas = null;
        for(PrestacaoContasBean prest : auxilio.getPrestacaoContas()){
            if(dataUltimaPrestacaoContas==null || (prest!=null && prest.getDataLimite()!= null && prest.getDataLimite().after(dataUltimaPrestacaoContas))){
                dataUltimaPrestacaoContas = prest.getDataLimite();
            }
        }

        for (DocumentoComprovacaoDespesaBean docTmp : despesa.getDocumentosComprovacaoDespesas()) {
            if (!auxilio.getFkModalidade().isPermiteDespesaAposVigencia() && docTmp.getDataEmissao().after(auxilio.getDataFinal())) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A data de emissão do(s) comprovante(s) deve ser anterior ao final da vigência do auxílio.");
                FacesContext.getCurrentInstance().addMessage("listaComprDespesas", message);
                temErro = true;
                break;
            }
            if(auxilio.getFkModalidade().isPermiteDespesaAposVigencia() && docTmp.getDataEmissao().after(dataUltimaPrestacaoContas)){
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A data de emissão do(s) comprovante(s) deve ser anterior à última prestação de contas do auxílio.");
                FacesContext.getCurrentInstance().addMessage("listaComprDespesas", message);
                temErro = true;
                break;
            }
            if (!auxilio.getFkModalidade().isPermiteDespesaAntesVigencia() && docTmp.getDataEmissao().before(auxilio.getDataInicial())) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A data de emissão do(s) comprovante(s) deve ser posterior ao início da vigência do auxílio.");
                FacesContext.getCurrentInstance().addMessage("listaComprDespesas", message);
                temErro = true;
                break;
            }
        }

        if (despesa.getId() == null && despesa.getValor().compareTo(saldoDisponivel) == 1) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor inválido", "O valor da despesa supera o saldo disponível.");
            FacesContext.getCurrentInstance().addMessage("vlrDesp", message);
            temErro = true;
        }
        if (despesa.getId() != null) {
            DespesaBean despesaBanco = Service.getInstance().buscarDespesa(despesa.getId());
            if (despesa.getValor().subtract(despesaBanco.getValor()).compareTo(saldoDisponivel) == 1) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor inválido", "O valor da despesa supera o saldo disponível.");
                FacesContext.getCurrentInstance().addMessage("vlrDesp", message);
                temErro = true;
            }

        }

        if (!auxilio.getFkModalidade().isPermiteDespesaAntesVigencia()) {
//            Código comentado em virtude da atividade #1191
//            if (despesa.getDataRealizada().after(auxilio.getDataFinal())) {
//                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A despesa deve ser realizada antes do final da vigência do auxílio.");
//                FacesContext.getCurrentInstance().addMessage("dataRealizada", message);
//                temErro = true;
//            }
            if (despesa.getDataRealizada().before(auxilio.getDataInicial())) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A despesa deve ser realizada depois do início da vigência do auxílio.");
                FacesContext.getCurrentInstance().addMessage("dataRealizada", message);
                temErro = true;
            }
            if (despesa.getDataLiberacaoPagamento() != null) {
//            Código comentado em virtude da atividade #1191
//                if (despesa.getDataLiberacaoPagamento().after(auxilio.getDataFinal())) {
//                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A liberação do pagamento ser realizada antes do final da vigência do auxílio.");
//                    FacesContext.getCurrentInstance().addMessage("dataLibPagto", message);
//                    temErro = true;
//                }
                if (despesa.getDataLiberacaoPagamento().before(auxilio.getDataInicial())) {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A liberação do pagamento deve ser realizada depois do início da vigência do auxílio.");
                    FacesContext.getCurrentInstance().addMessage("dataLibPagto", message);
                    temErro = true;
                }
            }
        }

        if (valorSomaComprovantes.compareTo(despesa.getValor()) == 1) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor acima do limite", "O valor dos comprovantes da despesa superam o valor da despesa.");
            FacesContext.getCurrentInstance().addMessage("listaComprDespesas", message);
            temErro = true;
        }
        
        if (valorSomaCheques.compareTo(despesa.getValor()) == 1) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor acima do limite", "O valor dos comprovantes de pagamentos supera o valor da despesa.");
            FacesContext.getCurrentInstance().addMessage("listaComprPagamentos", message);
            temErro = true;
        }

        for (DocumentoComprovacaoPagamentoBean docTmp : despesa.getDocumentosComprovacaoPagamentos()) {
            valorSomaCheques = valorSomaCheques.add(docTmp.getValor());
        }

        for (DocumentoComprovacaoPagamentoBean docTmp : despesa.getDocumentosComprovacaoPagamentos()) {
//            Código comentado em virtude da atividade #1191
//            if (docTmp.getDataEmissao().after(auxilio.getDataFinal())) {
//                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A data de emissão do(s) pagamento(s) deve ser anterior ao final da vigência do auxílio.");
//                FacesContext.getCurrentInstance().addMessage("listaComprPagamentos", message);
//                temErro = true;
//                break;
//            }
            if (!auxilio.getFkModalidade().isPermiteDespesaAntesVigencia() && docTmp.getDataEmissao().before(auxilio.getDataInicial())) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "A data de emissão do(s) pagamento(s) deve ser posterior ao início da vigência do auxílio.");
                FacesContext.getCurrentInstance().addMessage("listaComprPagamentos", message);
                temErro = true;
                break;
            }
        }

        if (despesa.getGenero() == DespesaBean.GENERO_NOTA_FISCAL && !despesa.getDocumentosComprovacaoDespesas().isEmpty() && !aceiteDocumentoFiscalDuplicado ) {
            for (DocumentoComprovacaoDespesaBean docValidar : despesa.getDocumentosComprovacaoDespesas()) {
                Integer idDocValidar = docValidar.getId();
                List<DocumentoComprovacaoDespesaBean> documentosCoincidentes = Service.getInstance().pesquisarCriteriaRegistrosDuplicados(docValidar.getDataEmissao(), docValidar.getNumero(), fornecedorSelecionado);
                if (idDocValidar == null) {
                    if (!documentosCoincidentes.isEmpty()) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "O Documento de Comprovação de Despesa já existe.", "No. " + docValidar.getNumero() + ". Alínea " + docValidar.getFkDespesa().getFkAlinea().getFkCategoriaAlinea().getNome() + ". Auxílio" + docValidar.getFkDespesa().getFkAlinea().getFkAuxilio().getProtocoloFinanciador() + ". Financiador " + docValidar.getFkDespesa().getFkAlinea().getFkAuxilio().getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getSigla());
                        FacesContext.getCurrentInstance().addMessage("documentosFiscaisDuplicados", message);
                        temErro = true;
                        //break;
                        RequestContext.getCurrentInstance().execute("PF('aceiteNotaFiscalDuplicada').show();");
                    }
                } else {
                    for (DocumentoComprovacaoDespesaBean docTemp : documentosCoincidentes) {
                        if (idDocValidar.intValue() != docTemp.getId().intValue()) {
                            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "O Documento de Comprovação de Despesa já existe.", "No. " + docValidar.getNumero() + ". Alínea " + docValidar.getFkDespesa().getFkAlinea().getFkCategoriaAlinea().getNome() + ". Auxílio" + docValidar.getFkDespesa().getFkAlinea().getFkAuxilio().getProtocoloFinanciador() + ". Financiador " + docValidar.getFkDespesa().getFkAlinea().getFkAuxilio().getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getSigla());
                            FacesContext.getCurrentInstance().addMessage("documentosFiscaisDuplicados", message);
                            temErro = true;
                            //break;
                            RequestContext.getCurrentInstance().execute("PF('aceiteNotaFiscalDuplicada').show();");
                        }
                    }
                }
            }
        }

        if (this.despesa.getGenero() == DespesaBean.GENERO_DIARIA || this.despesa.getGenero() == DespesaBean.GENERO_RECIBO) { //despesa com recibo
            despesa.setStatus("Finalizada");
        }

        if (!temErro) {
            this.despesa.setFkFornecedor(fornecedorSelecionado);
            this.despesa.setFkPessoa(pessoaSelecionada);
            if (ADICIONAR_STATE.equals(this.currentState)) {
                this.despesa.setFkAlinea(alineaBean);
                Service.getInstance().cadastraDespesa(this.despesa);
            } else if (EDITAR_STATE.equals(this.currentState)) {
                this.despesa.setFkAlinea(alineaBean);
                Service.getInstance().atualizarDespesa(this.despesa);
            }
            //atualizaAuxilio();
            RequestContext.getCurrentInstance().execute("PF('aceiteNotaFiscalDuplicada').hide();");
            RequestContext.getCurrentInstance().execute("PF('editDespesas').hide();");
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        arquivoAnexo = new ArquivoAnexoDespesaBean();
        arquivoAnexo.setNome(event.getFile().getFileName());
        arquivoAnexo.setConteudo(event.getFile().getContents());
        arquivoAnexo.setContentType(event.getFile().getContentType());

        despesa.getArquivosAnexos().add(arquivoAnexo);

        FacesMessage msg = new FacesMessage("Sucesso ", event.getFile().getFileName() + " foi enviado.");
        FacesContext.getCurrentInstance().addMessage("uploadDeArquivo", msg);
    }

    public StreamedContent downloadArquivoAnexo(ArquivoAnexoDespesaBean arquivo) {
        logger.debug("ArquivoAnexo: " + arquivo.getNome());
        ByteArrayInputStream stream = new ByteArrayInputStream(arquivo.getConteudo());
        StreamedContent file = new DefaultStreamedContent(stream, arquivo.getContentType(), arquivo.getNome());
        return file;
    }

    public void excluirArquivoAnexoDespesa() {
        despesa.getArquivosAnexos().remove(arquivoAnexo);
        RequestContext.getCurrentInstance().execute("PF('deletePopupArquivo').hide();");
    }

    public void addComprovacaoDespesa() {
        if (this.despesa.getGenero() == DespesaBean.GENERO_NOTA_FISCAL) {
            documentoDespesa = new NotaFiscalBean();
            documentoDespesa.setDataEmissao(null);
            //exibeCamposNF = true;
        }
        if (this.despesa.getGenero() == DespesaBean.GENERO_DIARIA || this.despesa.getGenero() == DespesaBean.GENERO_RECIBO) {
            documentoDespesa = new ReciboBean();
            documentoDespesa.setDataEmissao(despesa.getDataRealizada());
            //exibeCamposNF = false;
        }

        BigDecimal valorPagamento = despesa.getValor();
        for (DocumentoComprovacaoDespesaBean nfTemp : this.despesa.getDocumentosComprovacaoDespesas()) {
            valorPagamento = valorPagamento.subtract(nfTemp.getValor());
        }
        if (valorPagamento.compareTo(BigDecimal.ZERO) == -1) {
            valorPagamento = new BigDecimal(0);
        }

        documentoDespesa.setValor(valorPagamento);
        documentoDespesa.setFkDespesa(despesa);
        documentoDespesa.setDataEmissao(despesa.getDataRealizada());
        this.despesa.getDocumentosComprovacaoDespesas().add(documentoDespesa);
    }

    public void addComprovacaoPagto() {
        DocumentoComprovacaoPagamentoBean doc = new DocumentoComprovacaoPagamentoBean();
        if (this.despesa.getGenero() == 0) {
            doc.setDataEmissao(null);
        }
        if (this.despesa.getGenero() == 1) {
            doc.setDataEmissao(despesa.getDataRealizada());
        }
        doc.setFkDespesa(despesa);
        BigDecimal valorPagamento = despesa.getValor();

        for (DocumentoComprovacaoPagamentoBean pagtoTemp : this.despesa.getDocumentosComprovacaoPagamentos()) {
            valorPagamento = valorPagamento.subtract(pagtoTemp.getValor());
        }

        if (valorPagamento.compareTo(BigDecimal.ZERO) == -1) {
            valorPagamento = new BigDecimal(0);
        }

        doc.setValor(valorPagamento);
        doc.setDataEmissao(new Date());
        this.despesa.getDocumentosComprovacaoPagamentos().add(doc);
    }

    public void validateValor(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        BigDecimal oldValue = despesa.getValor();
        if(oldValue == null) oldValue = new BigDecimal(0);
        BigDecimal value = BigDecimal.valueOf(Double.valueOf(newValue.toString()));
        if (value.compareTo(BigDecimal.ZERO) <= 0) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor não pode ser zero (0).", "Valor não pode ser zero (0).");
            facesContext.addMessage(component.getClientId(facesContext), message);
        } else if (value.compareTo(saldoDisponivel.add(oldValue)) == 1) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Valor acima do limite", "O valor da despesa supera o valor disponível da alínea.");
            facesContext.addMessage(component.getClientId(facesContext), message);
        } else {
            despesa.setValor(value);
        }
    }

    public void excluirCheque() {
//        logger.debug("docto: " + documentoPagto.getNumero() + " - " + documentoPagto.getValor());
        despesa.getDocumentosComprovacaoPagamentos().remove(documentoPagto);
        if (documentoPagto != null && documentoPagto.getId() != null) {
            Service.getInstance().excluirCheque(documentoPagto);
        }
        documentoPagto = null;
        RequestContext.getCurrentInstance().execute("PF('deletePopupCheque').hide();");
    }
    
    public void excluirNF(){
        despesa.getDocumentosComprovacaoDespesas().remove(documentoDespesa);
        if(documentoDespesa!= null && documentoDespesa.getId()!=null){
            Service.getInstance().excluirDocumentoFiscal(documentoDespesa);
        }
        documentoDespesa=null;
        RequestContext.getCurrentInstance().execute("PF('deletePopupNF').hide();");
    }

    public void atualizarSaldoSubcentros() {
        //saldoDisponivel = ContabilidadeService.getInstance().saldoAlinea(alineaBean);
        if (despesa.getFkSubcentro() != null) {
            saldoDisponivel = ContabilidadeService.getInstance().saldoSubcentro(despesa.getFkSubcentro());
        }
    }

    public void excluirDespesa() {
        for (AlineaBean alineaTemp : auxilio.getAlineas()) {
            alineaTemp.getDespesas().remove(despesa);
            for (SubcentroDespesasBean subcentroTemp : alineaTemp.getSubcentrosDespesas()) {
                subcentroTemp.getDespesas().remove(despesa);
            }
        }
        for (PrestacaoContasBean prestacaoTemp : auxilio.getPrestacaoContas()) {
            prestacaoTemp.getDespesas().remove(despesa);
        }
        Service.getInstance().excluirDespesa(despesa);
        auxilio = Service.getInstance().atualizarAuxilio(auxilio);
        
        
        despesa = null;
        //atualizaAuxilio();
        RequestContext.getCurrentInstance().execute("PF('deletePopupDespesa').hide();");
    }

    //    public void prepareCadastrarDespesa() {
    //        logger.debug("--InfoProjeto ----prepareDespesa--");
    //        this.despesa = new DespesaBean();
    //        saldoDisponivel = 0;
    //        moedaDespesa = "";
    //        this.setCurrentState(ADICIONAR_STATE);
    //    }
    public AuxilioBean getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(AuxilioBean auxilio) {
        this.auxilio = auxilio;
    }

    public PessoaBean getPessoaSelecionada() {
        return pessoaSelecionada;
    }

    public void setPessoaSelecionada(PessoaBean pessoaSelecionada) {
        this.pessoaSelecionada = pessoaSelecionada;
    }

    public List<PessoaBean> getListaPessoas() {
        listaPessoas = Service.getInstance().pesquisarPessoas();
        return listaPessoas;
    }

    public void setListaPessoas(List<PessoaBean> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }
    
}
