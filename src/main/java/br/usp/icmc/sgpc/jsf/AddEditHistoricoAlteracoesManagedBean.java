/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.EventoBean;
import br.usp.icmc.sgpc.beans.HistoricoAuxilioBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditHistoricoAlteracoesMB")
@ViewScoped
public class AddEditHistoricoAlteracoesManagedBean implements Serializable{
    private static final Logger logger = Logger.getLogger(AddEditHistoricoAlteracoesManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    
    private HistoricoAuxilioBean historicoEditado;
    private HistoricoAuxilioBean novoHistorico;
    
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    
    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
    }
    
    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public HistoricoAuxilioBean getHistoricoEditado() {
        return historicoEditado;
    }

    public void setHistoricoEditado(HistoricoAuxilioBean historicoEditado) {
        this.historicoEditado = historicoEditado;
    }

    public HistoricoAuxilioBean getNovoHistorico() {
        return novoHistorico;
    }

    public void setNovoHistorico(HistoricoAuxilioBean novoHistorico) {
        this.novoHistorico = novoHistorico;
    }
    
    public void prepareEditar(HistoricoAuxilioBean eventoEditar){
        logger.debug("prepareEditar historico");
        this.historicoEditado = eventoEditar;
        currentState = EDITAR_STATE;
        this.novoHistorico = new HistoricoAuxilioBean();
        this.novoHistorico.setAcao(historicoEditado.getAcao());
        this.novoHistorico.setDataAlteracao(historicoEditado.getDataAlteracao());
        this.novoHistorico.setDescricao(historicoEditado.getDescricao());
        this.novoHistorico.setFkAuxilio(historicoEditado.getFkAuxilio());
        //this.novoHistorico.setFkResponsavel(historicoEditado.getFkResponsavel());
        this.novoHistorico.getHistoricosAnteriores().addAll(historicoEditado.getHistoricosAnteriores());
        this.novoHistorico.getHistoricosAnteriores().add(historicoEditado);
        this.novoHistorico.setNomeAlinea(historicoEditado.getNomeAlinea());
        this.novoHistorico.setNomeCentroDespesas(historicoEditado.getNomeCentroDespesas());
        this.novoHistorico.setOperacao(historicoEditado.getOperacao());
        this.novoHistorico.setValor(historicoEditado.getValor());
        RequestContext.getCurrentInstance().execute("PF('editHistoricoAlteracoesPanel').show();");
    }
    
    public void gravarNovoHistorico(){
        this.novoHistorico.setFkResponsavel(userSessionMB.getLoggedUser());
        this.historicoEditado.getHistoricosAnteriores().clear();
        this.historicoEditado.setEditado(true);
        
        Service.getInstance().registrarHistoricoAuxilio(novoHistorico);
        Service.getInstance().registrarHistoricoAuxilio(historicoEditado);
        RequestContext.getCurrentInstance().execute("PF('editHistoricoAlteracoesPanel').hide();");
    }
    
}
