/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.DepartamentoBean;
import br.usp.icmc.sgpc.beans.InstituicaoBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.UnidadeBean;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.AuditoriaService;
import br.usp.icmc.sgpc.service.AutenticacaoService;
import br.usp.icmc.sgpc.service.MailService;
import br.usp.icmc.sgpc.service.Service;
import br.usp.icmc.sgpc.service.ValidaDocumentosService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditPessoaMB")
@ViewScoped
public class AddEditPessoaManagedBean implements Serializable {
    private static final Logger logger = Logger.getLogger(AddEditPessoaManagedBean.class);
    
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    
    private List<SelectItem> listaCadInstituicoes = new ArrayList<SelectItem>();
    private List<SelectItem> listaCadUnidades = new ArrayList<SelectItem>();
    private List<SelectItem> listaCadDepartamentos = new ArrayList<SelectItem>();
    
    private Integer idInstituicaoCadastro;
    private Integer idUnidadeCadastro;
    private Integer idDepartamentoCadastro;
    
    private PessoaBean pessoa;
    
    private DualListModel<PapelBean> listaGrupos = new DualListModel<PapelBean>();
    
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    
    private boolean podeGerenciarGrupos;
    
    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        
        listaCadInstituicoes = GenericConverter.createSelectItems(Service.getInstance().listarInstituicoes());
        podeGerenciarGrupos = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PESSOA_GERENCIAR_GRUPOS);
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public List<SelectItem> getListaCadInstituicoes() {
        return listaCadInstituicoes;
    }

    public void setListaCadInstituicoes(List<SelectItem> listaCadInstituicoes) {
        this.listaCadInstituicoes = listaCadInstituicoes;
    }

    public List<SelectItem> getListaCadUnidades() {
        return listaCadUnidades;
    }

    public void setListaCadUnidades(List<SelectItem> listaCadUnidades) {
        this.listaCadUnidades = listaCadUnidades;
    }

    public List<SelectItem> getListaCadDepartamentos() {
        return listaCadDepartamentos;
    }

    public void setListaCadDepartamentos(List<SelectItem> listaCadDepartamentos) {
        this.listaCadDepartamentos = listaCadDepartamentos;
    }

    public DualListModel<PapelBean> getListaGrupos() {
        return listaGrupos;
    }

    public void setListaGrupos(DualListModel<PapelBean> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }

    public Integer getIdInstituicaoCadastro() {
        return idInstituicaoCadastro;
    }

    public void setIdInstituicaoCadastro(Integer idInstituicaoCadastro) {
        this.idInstituicaoCadastro = idInstituicaoCadastro;
    }

    public Integer getIdUnidadeCadastro() {
        return idUnidadeCadastro;
    }

    public void setIdUnidadeCadastro(Integer idUnidadeCadastro) {
        this.idUnidadeCadastro = idUnidadeCadastro;
    }

    public Integer getIdDepartamentoCadastro() {
        return idDepartamentoCadastro;
    }

    public void setIdDepartamentoCadastro(Integer idDepartamentoCadastro) {
        this.idDepartamentoCadastro = idDepartamentoCadastro;
    }

    public PessoaBean getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaBean pessoa) {
        this.pessoa = pessoa;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public boolean isPodeGerenciarGrupos() {
        return podeGerenciarGrupos;
    }

    public void setPodeGerenciarGrupos(boolean podeGerenciarGrupos) {
        this.podeGerenciarGrupos = podeGerenciarGrupos;
    }
    
    public void prepareAdicionar() {
        pessoa = new PessoaBean();
        this.setCurrentState(ADICIONAR_STATE);
        
        List<PapelBean> listaTodosPapeis = SecurityService.getInstance().listarPapeis();
        listaGrupos = new DualListModel<PapelBean>(listaTodosPapeis, new ArrayList<PapelBean>());
        
        idInstituicaoCadastro = null;
        pesquisarCadUnidades();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    
    public void prepareEditar(PessoaBean pessoa) {
        this.pessoa = pessoa;

        List<PapelBean> listaTodosPapeis = new ArrayList<PapelBean>();
        listaTodosPapeis = SecurityService.getInstance().listarPapeis();
        listaTodosPapeis.removeAll(pessoa.getPapeis());

        List<PapelBean> listaPapeisPessoa = new ArrayList<PapelBean>();
        listaPapeisPessoa.addAll(pessoa.getPapeis());

        listaGrupos = new DualListModel<PapelBean>(listaTodosPapeis, listaPapeisPessoa);
        
        idInstituicaoCadastro = null;

        
        if(pessoa!=null && pessoa.getFkDepartamento()!=null){
            idInstituicaoCadastro = pessoa.getFkDepartamento().getFkUnidade().getFkInstituicao().getId();
            idUnidadeCadastro = pessoa.getFkDepartamento().getFkUnidade().getId();
            idDepartamentoCadastro = pessoa.getFkDepartamento().getId();
            
            InstituicaoBean instituicaoTemp = Service.getInstance().buscarInstituicao(idInstituicaoCadastro);
            UnidadeBean unidadeTemp = Service.getInstance().buscarUnidade(idUnidadeCadastro);
            
            listaCadUnidades = GenericConverter.createSelectItems(instituicaoTemp.getUnidades());
            listaCadDepartamentos = GenericConverter.createSelectItems(unidadeTemp.getDepartamentos());
        }
        
        //pesquisarCadUnidades();
        //pesquisarCadDepartamentos();
        
        this.setCurrentState(EDITAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    
    public void gravar() {
        boolean temErro = false;
        if(pessoa.getNome()==null || "".equals(pessoa.getNome())){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"O nome é obrigatório","O nome de usuário é obrigatório");
            FacesContext.getCurrentInstance().addMessage("nome", message);
            temErro = true;
        }
        if(pessoa.getEscopo()==PessoaBean.ESCOPO_INTERNO){
            if(pessoa.getUsername()==null || "".equals(pessoa.getUsername())){
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"O nome de usuário é obrigatório","O nome de usuário é obrigatório");
                FacesContext.getCurrentInstance().addMessage("username", message);
                temErro = true;
            }
            if(pessoa.getEmail()==null || "".equals(pessoa.getEmail())){
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"O email é obrigatório","O email é obrigatório");
                FacesContext.getCurrentInstance().addMessage("email", message);
                temErro = true;
            }
            if(idInstituicaoCadastro==null || idInstituicaoCadastro==0){
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"É necessário indicar a Instituição","É necessário indicar a Instituição");
                FacesContext.getCurrentInstance().addMessage("comboInstituicao", message);
                temErro = true;
            }
            if(idUnidadeCadastro==null || idUnidadeCadastro==0){
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"É necessário indicar a Unidade","É necessário indicar a Unidade");
                FacesContext.getCurrentInstance().addMessage("comboUnidade", message);
                temErro = true;
            }
            if(idDepartamentoCadastro==null || idDepartamentoCadastro==0){
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"É necessário indicar o Departamento","É necessário indicar o Departamento");
                FacesContext.getCurrentInstance().addMessage("comboDepartamento", message);
                temErro = true;
            }
        }
        if(temErro) return;
        
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            try {
                String senha = AutenticacaoService.getInstance().getRandomPassword(8);
                pessoa.setPassword(AutenticacaoService.getInstance().encryptPassword(senha));
                DepartamentoBean departamentoCadastro = Service.getInstance().buscarDepartamento(idDepartamentoCadastro);
                pessoa.setFkDepartamento(departamentoCadastro);

                pessoa = Service.getInstance().cadastraUsuario(pessoa);
                //oorreção para DetachedEntity Passed to Persist
                //salvar usuário primeiro para depois inserir o papel dele.
                pessoa.getPapeis().addAll(listaGrupos.getTarget());
                pessoa = Service.getInstance().atualizarPessoa(pessoa);
                
                if(pessoa.getEmail()!=null && !pessoa.getEmail().isEmpty() && pessoa.getUsername()!=null && !pessoa.getUsername().isEmpty()){
                    String subject = "Confirmação de Cadastro - SGPC";
                    String setTo = pessoa.getEmail();
                    String message = "Seu usuário foi cadastrado com sucesso no sistema SGPC. \n"
                            + "Nome de Usuário - " + pessoa.getUsername() + " \n"
                            + "Senha - " + senha;
                    MailService.getInstance().sendMail(subject, setTo, message);
                }
                AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Adicionar Registro", "Pessoa", "Cadastro de novo usuário no sistema. Nome: " + pessoa.getNome(), ConfigConstantes.CONFIG_AUDITORIA_ADICIONAR_PESSOA);
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(PessoaManagedBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            pessoa.getPapeis().removeAll(listaGrupos.getSource());
            listaGrupos.getTarget().removeAll(pessoa.getPapeis());
            pessoa.getPapeis().addAll(listaGrupos.getTarget());
            //pessoa.setFkDepartamento(departamentoCadastro);
            DepartamentoBean departamentoCadastro = Service.getInstance().buscarDepartamento(idDepartamentoCadastro);
            pessoa.setFkDepartamento(departamentoCadastro);

            pessoa = Service.getInstance().atualizarPessoa(pessoa);
            AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Pessoa", "Edição de dados de usuário no sistema. Id Usuário: " + pessoa.getId() + " - Nome: " + pessoa.getNome() + " - Username: " + pessoa.getUsername(), ConfigConstantes.CONFIG_AUDITORIA_EDITAR_PESSOA);
        }
        //this.pesquisarUsuarios();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }
    
    public void validateUsername(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        PessoaBean pessoaPreexistente = null;
        if (newValue!=null && !((String)newValue).isEmpty() && ADICIONAR_STATE.equals(this.currentState)) {
            try {
                pessoaPreexistente = Service.getInstance().pesquisarPessoaUsername(newValue.toString());
            } catch (Exception e) {
                logger.debug(e);
            }
            if (pessoaPreexistente != null) {
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nome de usuário já cadastrado", "O nome de usuário já consta na base de dados. É necessário escolher outro nome.");
                facesContext.addMessage(component.getClientId(facesContext), message);
            }
        }
    }

    public void validateEmail(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        PessoaBean pessoaPreexistente = null;
        if(newValue!=null && !((String)newValue).isEmpty()){
            if(!newValue.toString().matches("[\\w\\.-]*[a-zA-Z0-9_]@[\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]")){
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail inválido", "O e-mail digitado não é válido.");
                facesContext.addMessage(component.getClientId(facesContext), message);
            }
            if (((UIInput) component).isValid() && ADICIONAR_STATE.equals(this.currentState)) {
                try {
                    pessoaPreexistente = Service.getInstance().pesquisarPessoaEmail(newValue.toString());
                } catch (Exception e) {
                    logger.debug(e);
                }
                if (pessoaPreexistente != null) {
                    ((UIInput) component).setValid(false);
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail de usuário já cadastrado", "O e-mail de usuário já consta na base de dados. É necessário escolher outro.");
                    facesContext.addMessage(component.getClientId(facesContext), message);
                }
            }
        }
    }

    public void validateCPF(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        PessoaBean pessoaPreexistente = null;
        boolean cpfValido = true;
        if (newValue!=null && !((String)newValue).isEmpty()) {
            if(ADICIONAR_STATE.equals(this.currentState)){
                try {
                    pessoaPreexistente = Service.getInstance().pesquisarPessoaCPF(newValue.toString());
                } catch (Exception e) {
                    logger.debug(e);
                }
            }
            cpfValido = ValidaDocumentosService.getInstance().isValidCPF(newValue.toString());
            if (pessoaPreexistente != null) {
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CPF de usuário já cadastrado", "Este CPF já consta na base de dados!!");
                facesContext.addMessage(component.getClientId(facesContext), message);
            }
            if(!cpfValido){
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CPF inválido", "O dígito verificador não permite validar esse numero de CPF");
                facesContext.addMessage(component.getClientId(facesContext), message);
            }
        }
    }
    
    public void pesquisarCadUnidades() {
        listaCadUnidades.clear();
        if (idInstituicaoCadastro != null) {
            InstituicaoBean instituicaoSelecionada = Service.getInstance().buscarInstituicao(idInstituicaoCadastro);
            listaCadUnidades = GenericConverter.createSelectItems(instituicaoSelecionada.getUnidades());
        }
        if (pessoa.getFkDepartamento()!=null && pessoa.getFkDepartamento().getFkUnidade() != null) {
            idUnidadeCadastro = pessoa.getFkDepartamento().getFkUnidade().getId();
        } else {
            idUnidadeCadastro = null;
        }
        pesquisarCadDepartamentos();
    }

    public void pesquisarCadDepartamentos() {
        listaCadDepartamentos.clear();
        if (idUnidadeCadastro != null) {
            UnidadeBean unidadeSelecionada = Service.getInstance().buscarUnidade(idUnidadeCadastro);
            listaCadDepartamentos = GenericConverter.createSelectItems(unidadeSelecionada.getDepartamentos());
        }
    }
}
