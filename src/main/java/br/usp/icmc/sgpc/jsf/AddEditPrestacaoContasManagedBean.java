/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaProjetoBean;
import br.usp.icmc.sgpc.beans.PrestacaoContasBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditPrestacaoContasMB")
@ViewScoped
public class AddEditPrestacaoContasManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(AddEditPrestacaoContasManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private List<String> listaPrestacaoConta = new ArrayList<String>();
    private PrestacaoContasBean prestacaoContas = new PrestacaoContasBean();
    private List<PessoaBean> listaPessoaFinanceiro = new ArrayList<PessoaBean>();

    @PostConstruct
    public void postConstruct() {
        AuxilioBean aux = userSessionMB.getAuxilio();
        ProjetoBean proj = aux.getFkProjeto();
        
        listaPrestacaoConta.add(PrestacaoContasBean.STATUS_AGENDADA);
        listaPrestacaoConta.add(PrestacaoContasBean.STATUS_CONCLUIDA);
        listaPrestacaoConta.add(PrestacaoContasBean.STATUS_ATRASADA);
        listaPessoaFinanceiro = Service.getInstance().pesquisarPessoasFinanceiro();
        listaPessoaFinanceiro.add(aux.getFkResponsavel());

        List<PessoaProjetoBean> participantesProjeto = Service.getInstance().buscarPessoasProjeto(proj);
        for(PessoaProjetoBean pessoaProjeto : participantesProjeto){
            listaPessoaFinanceiro.add(Service.getInstance().buscarPessoa(pessoaProjeto.getFkPessoa().getId()));
        }
        HashSet hs = new HashSet(listaPessoaFinanceiro);
        listaPessoaFinanceiro.clear();
        listaPessoaFinanceiro.addAll(hs);
        Collections.sort(listaPessoaFinanceiro,PessoaBean.POR_NOME);
        
        prestacaoContas = new PrestacaoContasBean();

    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }
    
    public void prepareAdicionar() {
        this.setCurrentState(ADICIONAR_STATE);
        this.prestacaoContas = new PrestacaoContasBean();
        RequestContext.getCurrentInstance().execute("PF('editPrestacaoContasPanel').show();");
    }
    
    public void prepareEditar(PrestacaoContasBean prest) {
        this.setCurrentState(EDITAR_STATE);
        this.prestacaoContas = prest;
        RequestContext.getCurrentInstance().execute("PF('editPrestacaoContasPanel').show();");
    }

    public List<String> getListaPrestacaoConta() {
        return listaPrestacaoConta;
    }

    public void setListaPrestacaoConta(List<String> listaPrestacaoConta) {
        this.listaPrestacaoConta = listaPrestacaoConta;
    }

    public PrestacaoContasBean getPrestacaoContas() {
        return prestacaoContas;
    }

    public void setPrestacaoContas(PrestacaoContasBean prestacaoContas) {
        this.prestacaoContas = prestacaoContas;
    }

    public List<PessoaBean> getListaPessoaFinanceiro() {
        return listaPessoaFinanceiro;
    }

    public void setListaPessoaFinanceiro(List<PessoaBean> listaPessoaFinanceiro) {
        this.listaPessoaFinanceiro = listaPessoaFinanceiro;
    }
    
    public void adicionaPrestacaoContas() {
        logger.debug("----add prestacao--");
        if (ADICIONAR_STATE.equals(this.currentState)) {
            AuxilioBean auxTemp = userSessionMB.getAuxilio();

            auxTemp.getPrestacaoContas().add(this.prestacaoContas);
            this.prestacaoContas.setFkAuxilio(auxTemp);
            auxTemp = Service.getInstance().atualizarAuxilio(auxTemp);
            userSessionMB.setAuxilio(auxTemp);
            this.prestacaoContas = new PrestacaoContasBean();
            //this.auxilio = Service.getInstance().buscarAuxilios(auxilio.getId());

        } else if (EDITAR_STATE.equals(this.currentState)) {
            Service.getInstance().atualizarPrestacaoContas(this.prestacaoContas);
            //Service.getInstance().atualizarAuxilio(auxilio);
            //this.auxilio = Service.getInstance().buscarAuxilios(auxilio.getId());
            this.prestacaoContas = new PrestacaoContasBean();
        }
        //atualizaAuxilio();
        RequestContext.getCurrentInstance().execute("PF('editPrestacaoContasPanel').hide();");
    }
    public void excluirPrestacaoContas() {
        AuxilioBean auxTemp = userSessionMB.getAuxilio();
        auxTemp.getPrestacaoContas().remove(this.prestacaoContas);
        Service.getInstance().excluirPrestacaoContas(this.prestacaoContas);
        //Service.getInstance().atualizarAuxilio(auxilio);
        //this.auxilio = Service.getInstance().buscarAuxilios(auxilio.getId());
        this.prestacaoContas = new PrestacaoContasBean();
        //atualizaAuxilio();
        RequestContext.getCurrentInstance().execute("PF('deletePopupPrestacao').hide();");
    }
    

}
