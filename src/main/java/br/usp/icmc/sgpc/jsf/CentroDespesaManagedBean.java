/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.CentroDespesaBean;
import br.usp.icmc.sgpc.beans.InstituicaoBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.UnidadeBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ConfiguracaoService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "centroDespesaMB")
@ViewScoped
public class CentroDespesaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(CentroDespesaManagedBean.class);
    private CentroDespesaBean centroDespesa;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private UIPanel panelForm;
    private Integer currentRow;
    private String pesquisa;
    private boolean podeCriarCentroDespesa;
    private boolean podeEditarCentroDespesa;
    private boolean podeExcluirCentroDespesa;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/del.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    
    private Integer idInstituicao;
    private List<SelectItem> listaInstituicoes;
    private Integer idUnidade;
    private List<SelectItem> listaUnidades;
    
    private Integer idInstituicaoEditar;
    private Integer idUnidadeEditar;
    
    private TreeNode rootNode;
    private TreeNode centroDespesasPaiEditar;
    private TreeNode rootNodeEditar;
    private CentroDespesaBean centroDespesaLista;
    
    @PostConstruct
    public void postConstruct(){
        listaInstituicoes = GenericConverter.createSelectItems(Service.getInstance().listarInstituicoes());
        
        String siglaUnidadeDefault = ConfiguracaoService.getInstance().getConfiguracao("CONFIG_DEFAULT_UNIDADE").getValor();
        List<UnidadeBean> unidadeDefault = Service.getInstance().buscarUnidade(siglaUnidadeDefault);
        if(!unidadeDefault.isEmpty()){
            idUnidade = unidadeDefault.get(0).getId();
            idInstituicao = unidadeDefault.get(0).getFkInstituicao().getId();
            listaUnidades = GenericConverter.createSelectItems(unidadeDefault.get(0).getFkInstituicao().getUnidades());
        }else if(listaInstituicoes!=null && !listaInstituicoes.isEmpty()){
            idInstituicao = (Integer) listaInstituicoes.get(0).getValue();
            listaUnidades = GenericConverter.createSelectItems(Service.getInstance().buscarInstituicao(idInstituicao).getUnidades());
            if(listaUnidades!=null && !listaUnidades.isEmpty()){
                idUnidade = (Integer) listaUnidades.get(0).getValue();
            }
        }
        criarNodesArvore();
    }

    public String getImagemCriarRegistro() {
        if(podeCriarCentroDespesa){
            imagemCriarRegistro = "/images/icons/add1.png";
        }else{
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
         if(podeEditarCentroDespesa){
            imagemEditarRegistro = "/images/icons/editar.png";
        }else{
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        if(podeExcluirCentroDespesa){
            imagemExcluirRegistro = "/images/icons/del.png";
        }else{
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public boolean isPodeCriarCentroDespesa() {
        return podeCriarCentroDespesa;
    }

    public void setPodeCriarCentroDespesa(boolean podeCriarCentroDespesa) {
        this.podeCriarCentroDespesa = podeCriarCentroDespesa;
    }

    public boolean isPodeEditarCentroDespesa() {
        return podeEditarCentroDespesa;
    }

    public void setPodeEditarCentroDespesa(boolean podeEditarCentroDespesa) {
        this.podeEditarCentroDespesa = podeEditarCentroDespesa;
    }

    public boolean isPodeExcluirCentroDespesa() {
        return podeExcluirCentroDespesa;
    }

    public void setPodeExcluirCentroDespesa(boolean podeExcluirCentroDespesa) {
        this.podeExcluirCentroDespesa = podeExcluirCentroDespesa;
    }

    public CentroDespesaManagedBean() {
        logger.debug("Instanciando classe");
        UserSessionManagedBean userSessionMB = (UserSessionManagedBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSessionMB");
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();

        podeCriarCentroDespesa = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.CENTROS_DESPESA_CRIAR);
        podeEditarCentroDespesa = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.CENTROS_DESPESA_EDITAR);
        podeExcluirCentroDespesa = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.CENTROS_DESPESA_EXCLUIR);

        this.pesquisar();
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public CentroDespesaBean getCentroDespesa() {
        return centroDespesa;
    }

    public void setCentroDespesa(CentroDespesaBean centroDespesa) {
        this.centroDespesa = centroDespesa;
    }

    public Integer getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(Integer currentRow) {
        this.currentRow = currentRow;
    }

    public UIPanel getPanelForm() {
        return panelForm;
    }

    public void setPanelForm(UIPanel panelForm) {
        this.panelForm = panelForm;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.centroDespesa = null;//new CentroDespesaBean();
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            //this.listaCentrosDespesa = Service.getInstance().listarCentrosDespesa();
            criarNodesArvore();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            //this.listaCentrosDespesa = Service.getInstance().pesquisarCentrosDespesa(pesquisa);
            criarNodesArvore();
        }
    }

    public void prepareAdicionar() {
        this.setCurrentState(ADICIONAR_STATE);
        this.centroDespesa = new CentroDespesaBean();
        this.idUnidadeEditar = this.idUnidade;
        this.idInstituicaoEditar = this.idInstituicao;
        this.centroDespesasPaiEditar = null;
        criarNodesArvoreEditar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    /*
     * Prepara view edita
     */
    public void prepareEditar(CentroDespesaBean centroDesp) {
        this.setCurrentState(EDITAR_STATE);
        this.centroDespesa  = centroDesp;
        this.idUnidadeEditar = this.idUnidade;
        this.idInstituicaoEditar = this.idInstituicao;
        //this.idUnidadeEditar = this.centroDespesa.getFkUnidade().getId();
        //this.centroDespesasPaiEditar = this.centroDespesa.getCentroDespesasPai();
        criarNodesArvoreEditar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    public void prepareExcluir(CentroDespesaBean centroDesp) {
        this.setCurrentState(EDITAR_STATE);
        this.centroDespesa  = centroDesp;
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }

    /*
     * Exclui registro
     */
    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        try{
            CentroDespesaBean cdPai = centroDespesa.getCentroDespesasPai();
            Service.getInstance().excluirCentroDespesa(centroDespesa);
            if(cdPai!=null){
                cdPai.getCentrosDespesaFilhos().remove(centroDespesa);
                Service.getInstance().atualizarCentroDespesa(cdPai);
            }
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "O Centro de Despesas tem auxílios vinculados.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public boolean isPesquisarState() {
        String state = this.getCurrentState();
        return (state == null || PESQUISAR_STATE.equals(state));
    }

    public boolean isAdicionarState() {
        return ADICIONAR_STATE.equals(this.getCurrentState());
    }

    public boolean isEditarState() {
        return EDITAR_STATE.equals(this.getCurrentState());
    }

    public void gravar() {
        UnidadeBean unidSelecionada = Service.getInstance().buscarUnidade(idUnidadeEditar);
        centroDespesa.setFkUnidade(unidSelecionada);
        int ordemVertical = 0;
        try{
            CentroDespesaBean centroPaiGravar = (CentroDespesaBean)this.centroDespesasPaiEditar.getData();
            centroDespesa.setCentroDespesasPai(centroPaiGravar);
            
            CentroDespesaBean centroPaiContarOrdem = (CentroDespesaBean)this.centroDespesasPaiEditar.getData();;
            ordemVertical++;
            while(centroPaiContarOrdem!=null){
                ordemVertical++;
                centroPaiContarOrdem = centroPaiContarOrdem.getCentroDespesasPai();
            }
        }catch(Exception e){
            centroDespesa.setCentroDespesasPai(null);
        }
        centroDespesa.setOrdemVerticalHierarquia(ordemVertical);
        
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO ---------------------------- " + centroDespesa.getNome());
            Service.getInstance().cadastraCentroDespesa(centroDespesa);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarCentroDespesa(centroDespesa);
        }
        this.centroDespesa=null;
        this.pesquisar();
        criarNodesArvore();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }
    
    private List<CentroDespesaBean> gerarLista(){
        List<CentroDespesaBean> listaTemp = new ArrayList<CentroDespesaBean>();
        List<TreeNode> nodesTemp = rootNode.getChildren();
        while(!nodesTemp.isEmpty()){
            TreeNode nodeTemp = nodesTemp.remove(0);
            listaTemp.add((CentroDespesaBean) nodeTemp.getData());
            nodesTemp.addAll(0, nodeTemp.getChildren());
        }
        criarNodesArvore();
        return listaTemp;
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioCentrosDespesas(gerarLista(), ReportService.FORMATO_PDF, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioCentrosDespesas(gerarLista(), ReportService.FORMATO_XLS, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }
    
    public void criarNodesArvore(){
        HashMap<Integer,TreeNode> hash = new HashMap<Integer,TreeNode>();
        rootNode = new DefaultTreeNode("root", null);
        if(idInstituicao!=null && idInstituicao!=0){
            if(idUnidade != null && idUnidade != 0){
                UnidadeBean unid = Service.getInstance().buscarUnidade(idUnidade);
                for(CentroDespesaBean cdb :  Service.getInstance().pesquisarCentrosDespesa(pesquisa, unid, null)){
                    if(cdb.getCentroDespesasPai()==null){
                        hash.put(cdb.getId(), new DefaultTreeNode(cdb, rootNode));
                    }else{
                        hash.put(cdb.getId(), new DefaultTreeNode(cdb, hash.get(cdb.getCentroDespesasPai().getId())));
                    }
                }
            }else{
                if(idInstituicao!=null && idInstituicao!=0){
                    listaUnidades = GenericConverter.createSelectItems(Service.getInstance().buscarInstituicao(idInstituicao).getUnidades());
                }
            }
        }else{
            idUnidade=null;
            listaUnidades = new ArrayList<SelectItem>();
        }
    }

    public Integer getIdInstituicao() {
        return idInstituicao;
    }

    public void setIdInstituicao(Integer idInstituicao) {
        this.idInstituicao = idInstituicao;
    }

    public Integer getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(Integer idUnidade) {
        this.idUnidade = idUnidade;
    }

    public List<SelectItem> getListaInstituicoes() {
        return listaInstituicoes;
    }

    public void setListaInstituicoes(List<SelectItem> listaInstituicoes) {
        this.listaInstituicoes = listaInstituicoes;
    }

    public List<SelectItem> getListaUnidades() {
        return listaUnidades;
    }

    public void setListaUnidades(List<SelectItem> listaUnidades) {
        this.listaUnidades = listaUnidades;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public Integer getIdInstituicaoEditar() {
        return idInstituicaoEditar;
    }

    public void setIdInstituicaoEditar(Integer idInstituicaoEditar) {
        this.idInstituicaoEditar = idInstituicaoEditar;
    }

    public Integer getIdUnidadeEditar() {
        return idUnidadeEditar;
    }

    public void setIdUnidadeEditar(Integer idUnidadeEditar) {
        this.idUnidadeEditar = idUnidadeEditar;
    }

    public TreeNode getCentroDespesasPaiEditar() {
        return centroDespesasPaiEditar;
    }

    public void setCentroDespesasPaiEditar(TreeNode centroDespesasPaiEditar) {
        this.centroDespesasPaiEditar = centroDespesasPaiEditar;
    }
    
//    public void onNodeSelect(NodeSelectEvent event) {
//        logger.debug("---=== SELECIONOU!!! " + event.getTreeNode().toString());
//        this.centroDespesasPaiEditar = event.getTreeNode();
//    }

    public TreeNode getRootNodeEditar() {
        return rootNodeEditar;
    }

    public void setRootNodeEditar(TreeNode rootNodeEditar) {
        this.rootNodeEditar = rootNodeEditar;
    }
    public void criarNodesArvoreEditar(){
        HashMap<Integer,TreeNode> hash = new HashMap<Integer,TreeNode>();
        rootNodeEditar = new DefaultTreeNode("root", null);
        if(idInstituicaoEditar!=null && idInstituicaoEditar!=0){
            if(idUnidadeEditar != null && idUnidadeEditar != 0){
                UnidadeBean unid = Service.getInstance().buscarUnidade(idUnidade);
                
                for(CentroDespesaBean cdb :  unid.getCentrosDespesas()){
                    if(cdb.getCentroDespesasPai()==null){
                        hash.put(cdb.getId(), new DefaultTreeNode(cdb, rootNodeEditar));
                    }else{
                        TreeNode nodePai = hash.get(cdb.getCentroDespesasPai().getId());
                        hash.put(cdb.getId(), new DefaultTreeNode(cdb, nodePai));
                    }
                }
                TreeNode expand = rootNodeEditar;
                CentroDespesaBean centroTemp = null;
                if(this.centroDespesa != null){
                    centroTemp = this.centroDespesa.getCentroDespesasPai();
                }
                
                if(centroTemp!=null){
                    expand =  hash.get(centroTemp.getId());
                }
                
                this.centroDespesasPaiEditar = expand;
                if(expand != null){
                    expand.setSelected(true);
                    while(expand != null){
                        expand.setExpanded(true);
                        expand = expand.getParent();
                    }
                }else{
                    rootNodeEditar.setSelected(true);
                    rootNodeEditar.setExpanded(true);
                }
            }else{
                listaUnidades = GenericConverter.createSelectItems(Service.getInstance().buscarInstituicao(idInstituicaoEditar).getUnidades());
            }
        }else{
            idUnidadeEditar=null;
            listaUnidades = new ArrayList<SelectItem>();
        }
    }
    private List<String> listarFiltrosAtivos(){
        List<String> retorno = new ArrayList<String>();
        
        // Palavra chave
        if(pesquisa!=null && !pesquisa.isEmpty()) retorno.add("Palavra-chave: " + pesquisa);
        
        if(this.idInstituicao!=null){
            InstituicaoBean inst = Service.getInstance().buscarInstituicao(idInstituicao);
            retorno.add("Instituição: " + inst.getNome());
        }
        if(this.idUnidade!=null){
            UnidadeBean unid = Service.getInstance().buscarUnidade(idUnidade);
            retorno.add("Unidade: " + unid.getSigla() + " - " + unid.getNome());
        }
        
        return retorno;
    }

    public CentroDespesaBean getCentroDespesaLista() {
        return centroDespesaLista;
    }

    public void setCentroDespesaLista(CentroDespesaBean centroDespesaLista) {
        this.centroDespesaLista = centroDespesaLista;
    }
}
