/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.ConfiguracaoBean;
import br.usp.icmc.sgpc.beans.InstituicaoBean;
import br.usp.icmc.sgpc.beans.UnidadeBean;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ConfiguracaoService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;

/**
 *
 * @author herick
 */
@ManagedBean(name = "configuracaoMB")
@ViewScoped
public class ConfiguracaoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(ConfiguracaoManagedBean.class);
    private List<ConfiguracaoBean> listaConfiguracoes = new ArrayList<ConfiguracaoBean>();
    private List<ConfiguracaoBean> listaConfiguracoesAuditoria = new ArrayList<ConfiguracaoBean>();
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private boolean acessaConfig = false;
    private ConfiguracaoBean notificacaoPR;
    private ConfiguracaoBean notificacaoCotacao;
    private ConfiguracaoBean corpoTextoRecibo;
    private ConfiguracaoBean unidadeDefault;
    private List<SelectItem> listaInstituicoes;
    private List<UnidadeBean> listaUnidades;
    private Integer idInstituicao;

    public ConfiguracaoManagedBean() {
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        acessaConfig = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.ACESSA_CONFIGURACOES);
        if (acessaConfig) {
            listaConfiguracoes = ConfiguracaoService.getInstance().listarConfiguracoesPorGrupo("mail_config");
            listaConfiguracoesAuditoria = ConfiguracaoService.getInstance().listarConfiguracoesPorGrupo("config_auditoria");
            notificacaoPR = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_NOTIFICACAO_PRESTACAO_RELATORIO);
            notificacaoCotacao = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_NOTIFICACAO_COTACAO);
            corpoTextoRecibo = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_RECIBO_TEXTO);
            unidadeDefault = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_DEFAULT_UNIDADE);
            this.listaInstituicoes = GenericConverter.createSelectItems(Service.getInstance().listarInstituicoes());
            if(unidadeDefault!= null){
                List<UnidadeBean> persistida = Service.getInstance().buscarUnidade(unidadeDefault.getValor());
                if(!persistida.isEmpty()){
                    InstituicaoBean instituicaoPersistida = persistida.get(0).getFkInstituicao();
                    idInstituicao = instituicaoPersistida.getId();
                    listaUnidades = instituicaoPersistida.getUnidades();
                }
            }
        }
    }

    public ConfiguracaoBean getNotificacaoCotacao() {
        return notificacaoCotacao;
    }

    public void setNotificacaoCotacao(ConfiguracaoBean notificacaoCotacao) {
        this.notificacaoCotacao = notificacaoCotacao;
    }

    public ConfiguracaoBean getNotificacaoPR() {
        return notificacaoPR;
    }

    public void setNotificacaoPR(ConfiguracaoBean notificacaoPR) {
        this.notificacaoPR = notificacaoPR;
    }

    public boolean isAcessaConfig() {
        return acessaConfig;
    }

    public void setAcessaConfig(boolean acessaConfig) {
        this.acessaConfig = acessaConfig;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public List<ConfiguracaoBean> getListaConfiguracoesAuditoria() {
        return listaConfiguracoesAuditoria;
    }

    public void setListaConfiguracoesAuditoria(List<ConfiguracaoBean> listaConfiguracoesAuditoria) {
        this.listaConfiguracoesAuditoria = listaConfiguracoesAuditoria;
    }

    public List<ConfiguracaoBean> getListaConfiguracoes() {
        return listaConfiguracoes;
    }

    public void setListaConfiguracoes(List<ConfiguracaoBean> listaConfiguracoes) {
        this.listaConfiguracoes = listaConfiguracoes;
    }

    public void salvarConfigMail() {
        ConfiguracaoService.getInstance().atualizarConfiguracao(listaConfiguracoes);
    }

    public void salvarConfigAuditoria() {
        ConfiguracaoService.getInstance().atualizarConfiguracao(listaConfiguracoesAuditoria);
    }

    public void salvarConfigNotificacao() {
        ConfiguracaoService.getInstance().atualizarConfiguracao(notificacaoPR);
        ConfiguracaoService.getInstance().atualizarConfiguracao(notificacaoCotacao);
    }

    public ConfiguracaoBean getCorpoTextoRecibo() {
        return corpoTextoRecibo;
    }

    public void setCorpoTextoRecibo(ConfiguracaoBean corpoTextoRecibo) {
        this.corpoTextoRecibo = corpoTextoRecibo;
    }

    public void salvarConfigRecibo(){
        ConfiguracaoService.getInstance().atualizarConfiguracao(corpoTextoRecibo);
    }

    public List<SelectItem> getListaInstituicoes() {
        return listaInstituicoes;
    }

    public void setListaInstituicoes(List<SelectItem> listaInstituicoes) {
        this.listaInstituicoes = listaInstituicoes;
    }

    public List<UnidadeBean> getListaUnidades() {
        return listaUnidades;
    }

    public void setListaUnidades(List<UnidadeBean> listaUnidades) {
        this.listaUnidades = listaUnidades;
    }

    public ConfiguracaoBean getUnidadeDefault() {
        return unidadeDefault;
    }

    public void setUnidadeDefault(ConfiguracaoBean unidadeDefault) {
        this.unidadeDefault = unidadeDefault;
    }
    
    public void salvarConfigUnidDefault() {
        ConfiguracaoService.getInstance().atualizarConfiguracao(unidadeDefault);
    }
    
    public void pesquisarUnidades(){
        if(idInstituicao != null){
            InstituicaoBean temp = Service.getInstance().buscarInstituicao(idInstituicao);
            listaUnidades = temp.getUnidades();
        }
    }

    public Integer getIdInstituicao() {
        return idInstituicao;
    }

    public void setIdInstituicao(Integer idInstituicao) {
        this.idInstituicao = idInstituicao;
    }
    
}
