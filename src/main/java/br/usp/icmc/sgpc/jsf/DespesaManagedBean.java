/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.CentroDespesaBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author artur
 */
@ManagedBean(name = "despesaMB")
@ViewScoped
public class DespesaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(DespesaManagedBean.class);
    private DespesaBean despesa;
    private List<DespesaBean> listaDespesas = new ArrayList<DespesaBean>();
    private String pesquisa;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private String imagemEditarRegistro = "/images/icons/editarbw.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    PessoaBean usuarioLogado;
    private boolean exibirColunaOwner;
    private Date dataInicialDocDespesa;
    private Date dataFinalDocDespesa;
    private List<String> listaStatus = new ArrayList<String>();
    String status;
    CentroDespesaBean centroDespesa;
    FinanciadorBean financiador;
    private List<FinanciadorBean> listaFinanciadores = new ArrayList<FinanciadorBean>();
    private List<CentroDespesaBean> listaCentroDespesa = new ArrayList<CentroDespesaBean>();
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    
    private Date dataInicialCheque;
    private Date dataFinalCheque;
    private String pesquisaDespesaNumeroDocFiscal;
    private String pesquisaDespesaNumeroCheque;
    

    public DespesaManagedBean() {
        logger.debug("Instanciando classe");
    }

    public void pesquisar(boolean pesquisaTodosOsCampos) {
        logger.debug("PESQUISAR");
        this.despesa = new DespesaBean();

        PessoaBean user = null;

        if (pesquisaTodosOsCampos) {
            logger.debug("PESQUISAR TODOS");
            if (exibirColunaOwner) {
                logger.debug("listar terceiros");
                this.listaDespesas = Service.getInstance().listarDespesa();
            } else {
                logger.debug("listar owner");
                this.listaDespesas = Service.getInstance().listarDespesa(usuarioLogado);
            }
        } else {
            logger.debug("PESQUISAR COM TERMO");
            if (!exibirColunaOwner) { //se o usuario nao tiver permissao, user=usuarioLogado, lista-se so suas despesas. Caso user tenha permissao de
                logger.debug("listar apenas as despesas filtradas do usuario");//ver todas as despesas, user=null e no JPA user=null significa nao
                user = usuarioLogado;             //restringir a sql com usuario (responsavel), logo lista-se todas as despesas
            }
            this.listaDespesas = Service.getInstance().pesquisarDespesa(user, pesquisa, null, financiador, status, centroDespesa, null, null, null, dataInicialDocDespesa, dataFinalDocDespesa, null, dataInicialCheque, dataFinalCheque, pesquisaDespesaNumeroCheque, pesquisaDespesaNumeroDocFiscal);
        }
    }

    public CentroDespesaBean getCentroDespesa() {
        return centroDespesa;
    }

    public void setCentroDespesa(CentroDespesaBean centroDespesa) {
        this.centroDespesa = centroDespesa;
    }

    public List<CentroDespesaBean> getListaCentroDespesa() {
        listaCentroDespesa.clear();
        listaCentroDespesa = Service.getInstance().listarCentrosDespesa();
        return listaCentroDespesa;
    }

    public void setListaCentroDespesa(List<CentroDespesaBean> listaCentroDespesa) {
        this.listaCentroDespesa = listaCentroDespesa;
    }

    public List<FinanciadorBean> getListaFinanciadores() {
        listaFinanciadores.clear();
        listaFinanciadores = Service.getInstance().listarFinanciadores();

        return listaFinanciadores;
    }

    public void setListaFinanciadores(List<FinanciadorBean> listaFinanciadores) {
        this.listaFinanciadores = listaFinanciadores;
    }
   
    public FinanciadorBean getFinanciador() {
        return financiador;
    }

    public void setFinanciador(FinanciadorBean financiador) {
        this.financiador = financiador;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataInicialDocDespesa() {
        return dataInicialDocDespesa;
    }

    public void setDataInicialDocDespesa(Date dataInicialDocDespesa) {
        this.dataInicialDocDespesa = dataInicialDocDespesa;
    }

    public Date getDataFinalDocDespesa() {
        return dataFinalDocDespesa;
    }

    public void setDataFinalDocDespesa(Date dataFinalDocDespesa) {
        this.dataFinalDocDespesa = dataFinalDocDespesa;
    }

    public List<String> getListaStatus() {
        listaStatus.clear();
        listaStatus.add("Em Elaboração");
        listaStatus.add("Em Análise");
        listaStatus.add("Aprovado");
        listaStatus.add("Não Aprovado");
        listaStatus.add("Em Andamento");
        listaStatus.add("Suspenso");
        listaStatus.add("Desistência");
        listaStatus.add("Transferido");
        listaStatus.add("Cancelado");
        listaStatus.add("Finalizado");

        return listaStatus;
    }

    public void setListaStatus(List<String> listaStatus) {
        this.listaStatus = listaStatus;
    }

    /*
    if ("".equals(this.pesquisa) || this.pesquisa == null) {
    logger.debug("PESQUISAR TODOS");
    this.listaDespesas = Service.getInstance().listarDespesa();
    } else {
    logger.debug("PESQUISAR COM TERMO");
    this.listaDespesas = Service.getInstance().pesquisarDespesa(pesquisa);
    }
    }
     *
     */
    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public DespesaBean getDespesa() {
        return despesa;
    }

    public void setDespesa(DespesaBean despesa) {
        this.despesa = despesa;
    }

    public List<DespesaBean> getListaDespesas() {
        return listaDespesas;
    }

    public void setListaDespesas(List<DespesaBean> listaDespesas) {
        this.listaDespesas = listaDespesas;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public void prepareAdicionar() {
        despesa = new DespesaBean();
        this.setCurrentState(ADICIONAR_STATE);
    }

    public void prepareEditar() {
        this.setCurrentState(EDITAR_STATE);
    }

    public void excluir() {
        Service.getInstance().excluirDespesa(despesa);
        listaDespesas = Service.getInstance().listarDespesa();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void gravar() {
    }

    public void bindDataInicialDocDespesa(SelectEvent event) {
        dataInicialDocDespesa = (Date) event.getObject();
        pesquisar(false);
    }
    
    public void bindDataFinalDocDespesa(SelectEvent event) {
        dataFinalDocDespesa = (Date) event.getObject();
        pesquisar(false);
    }
    
    public void bindDataInicialChequeDespesa(SelectEvent event) {
        dataInicialCheque = (Date) event.getObject();
        pesquisar(false);
    }
    
    public void bindDataFinalChequeDespesa(SelectEvent event) {
        dataFinalCheque = (Date) event.getObject();
        pesquisar(false);
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioDespesas(listaDespesas, ReportService.FORMATO_PDF, listarFiltrosAtivosDespesas());
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioDespesas(listaDespesas, ReportService.FORMATO_XLS, listarFiltrosAtivosDespesas());
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    public String detalharDespesa(){
        userSessionMB.setAuxilio(despesa.getFkAlinea().getFkAuxilio());
        userSessionMB.setInterPageParameter(despesa);
        return "infoDespesa?faces-redirect=true";
    }

    public String detalharAuxilio(AuxilioBean auxilio){
        userSessionMB.setAuxilio(auxilio);
        userSessionMB.setProjeto(auxilio.getFkProjeto());
        return "infoAuxilio?faces-redirect=true";
    }

    @PostConstruct
    public void postConstruct(){
        usuarioLogado = userSessionMB.getLoggedUser();
        exibirColunaOwner = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.DESPESA_LISTAR);
        this.pesquisar(true);
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }
    
    public Date datasEmissaoDocsFiscais(DespesaBean despesa){
        if(!despesa.getDocumentosComprovacaoDespesas().isEmpty()){
            return despesa.getDocumentosComprovacaoDespesas().get(0).getDataEmissao();
        }
        return null;
    }
    
    public Date datasEmissaoCheques(DespesaBean despesa){
        if(!despesa.getDocumentosComprovacaoPagamentos().isEmpty()){
            return despesa.getDocumentosComprovacaoPagamentos().get(0).getDataEmissao();
        }
        return null;
    }
    
    private List<String> listarFiltrosAtivosDespesas(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        List<String> retorno = new ArrayList<String>();
        
        if(pesquisa!=null && !"".equals(pesquisa)){
            retorno.add("Palavra-chave: " + pesquisa);
        }
        if(financiador!=null){
            retorno.add("Financiador: " + financiador.getSigla());
        }
        if(status!=null && !"".equals(status)){
            retorno.add("Status: " + status);
        }
        if(centroDespesa!=null){
            retorno.add("Centro de despesas: " + centroDespesa.getNome());
        }
        if(pesquisaDespesaNumeroDocFiscal!=null){
            retorno.add("No. doc fiscal: " + pesquisaDespesaNumeroDocFiscal);
        }
        if(dataInicialDocDespesa!=null){
            retorno.add("Dt. Inicial doc fiscal: " + sdf.format(dataInicialDocDespesa));
        }
        if(dataFinalDocDespesa!=null){
            retorno.add("Dt. Final doc fiscal: " + sdf.format(dataFinalDocDespesa));
        }
        if(pesquisaDespesaNumeroCheque!=null){
            retorno.add("No. doc cheque: " + pesquisaDespesaNumeroCheque);
        }
        if(dataInicialCheque!=null){
            retorno.add("Dt. Inicial cheque: " + sdf.format(dataInicialCheque));
        }
        if(dataFinalCheque!=null){
            retorno.add("Dt. Final cheque: " + sdf.format(dataFinalCheque));
        }
        
        return retorno;
    }

    public Date getDataInicialCheque() {
        return dataInicialCheque;
    }

    public void setDataInicialCheque(Date dataInicialCheque) {
        this.dataInicialCheque = dataInicialCheque;
    }

    public Date getDataFinalCheque() {
        return dataFinalCheque;
    }

    public void setDataFinalCheque(Date dataFinalCheque) {
        this.dataFinalCheque = dataFinalCheque;
    }

    public String getPesquisaDespesaNumeroDocFiscal() {
        return pesquisaDespesaNumeroDocFiscal;
    }

    public void setPesquisaDespesaNumeroDocFiscal(String pesquisaDespesaNumeroDocFiscal) {
        this.pesquisaDespesaNumeroDocFiscal = pesquisaDespesaNumeroDocFiscal;
    }

    public String getPesquisaDespesaNumeroCheque() {
        return pesquisaDespesaNumeroCheque;
    }

    public void setPesquisaDespesaNumeroCheque(String pesquisaDespesaNumeroCheque) {
        this.pesquisaDespesaNumeroCheque = pesquisaDespesaNumeroCheque;
    }
    
    

}
