/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.EnderecoBean;
import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.RepresentanteVendasBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author david
 */
@ManagedBean(name = "fornecedorMB")
@ViewScoped
public class FornecedorManagedBean implements Serializable {

    private String pesquisa;
    private static final Logger logger = Logger.getLogger(FornecedorManagedBean.class);
    private List<FornecedorBean> listaFornecedores = new ArrayList<FornecedorBean>();
    private String currentState = PESQUISAR_STATE;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String EDITAR_STATE = "editar";
    public static final String ADICIONAR_STATE = "adicionar";
    public EnderecoBean enderecoBean;
    private List<String> listaStatus = new ArrayList<String>();
    private String status;
    private boolean podeEditarFornecedor;
    private boolean podeCriarFornecedor;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;


    public FornecedorManagedBean() {
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        podeEditarFornecedor = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.FORNECEDOR_EDITAR);
        podeCriarFornecedor = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.FORNECEDOR_CRIAR);
        this.pesquisar();
    }

    public String getImagemCriarRegistro() {
        if (podeCriarFornecedor) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarFornecedor) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public boolean isPodeCriarFornecedor() {
        return podeCriarFornecedor;
    }

    public void setPodeCriarFornecedor(boolean podeCriarFornecedor) {
        this.podeCriarFornecedor = podeCriarFornecedor;
    }

    public boolean isPodeEditarFornecedor() {
        return podeEditarFornecedor;
    }

    public void setPodeEditarFornecedor(boolean podeEditarFornecedor) {
        this.podeEditarFornecedor = podeEditarFornecedor;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getListaStatus() {
        listaStatus.clear();
        listaStatus.add("Ativo");
        listaStatus.add("Inativo");
        listaStatus.add("Suspenso");
        return listaStatus;
    }

    public void setListaStatus(List<String> listaStatus) {
        this.listaStatus = listaStatus;
    }

    public List<FornecedorBean> getListaFornecedores() {
        return listaFornecedores;
    }

    public void setListaFornecedores(List<FornecedorBean> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");

        if (("".equals(this.pesquisa) || this.pesquisa == null) && ("".equals(this.status) || this.status == null)) {
            logger.debug("PESQUISAR TODOS");
            this.listaFornecedores = Service.getInstance().listarFornecedor();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaFornecedores = Service.getInstance().pesquisarFornecedorCriteria(pesquisa, status);
        }
    }

}
