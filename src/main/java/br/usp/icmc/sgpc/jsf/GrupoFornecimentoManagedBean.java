/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.CategoriaFornecedorBean;
import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.GrupoFornecimentoBean;
import br.usp.icmc.sgpc.beans.RepresentanteVendasBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author herick
 */
@ManagedBean(name = "grupoFornecimentoMB")
@ViewScoped
public class GrupoFornecimentoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(GrupoFornecimentoManagedBean.class);
    private GrupoFornecimentoBean grupoFornecimento;
    private String pesquisa;
    private List<GrupoFornecimentoBean> listaGrupos = new ArrayList<GrupoFornecimentoBean>();
    private String currentState = PESQUISAR_STATE;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String EDITAR_STATE = "editar";
    public static final String ADICIONAR_STATE = "adicionar";
    private boolean podeEditarGrupo;
    private boolean podeCriarGrupo;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private CategoriaFornecedorBean categoria;
    private List<FornecedorBean> listaFornecedores = new ArrayList<FornecedorBean>();
    private List<RepresentanteVendasBean> listaRepresentantes = new ArrayList<RepresentanteVendasBean>();

    //###----------Construtor-------------------###//
    public GrupoFornecimentoManagedBean() {
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        podeEditarGrupo = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.GRUPO_FORNECIMENTO_EDITAR);
        podeCriarGrupo = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.GRUPO_FORNECIMENTO_CRIAR);

        this.listaFornecedores = Service.getInstance().listarFornecedor();
        this.pesquisar();
    }

    public CategoriaFornecedorBean getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaFornecedorBean categoria) {
        this.categoria = categoria;
    }

    public List<FornecedorBean> getListaFornecedores() {
        return listaFornecedores;
    }

    public void setListaFornecedores(List<FornecedorBean> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }

    public List<RepresentanteVendasBean> getListaRepresentantes() {
        return listaRepresentantes;
    }

    public void setListaRepresentantes(List<RepresentanteVendasBean> listaRepresentantes) {
        this.listaRepresentantes = listaRepresentantes;
    }

    public String getImagemCriarRegistro() {
        if (podeCriarGrupo) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarGrupo) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public boolean isPodeCriarGrupo() {
        return podeCriarGrupo;
    }

    public void setPodeCriarGrupo(boolean podeCriarGrupo) {
        this.podeCriarGrupo = podeCriarGrupo;
    }

    public boolean isPodeEditarGrupo() {
        return podeEditarGrupo;
    }

    public void setPodeEditarGrupo(boolean podeEditarGrupo) {
        this.podeEditarGrupo = podeEditarGrupo;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public GrupoFornecimentoBean getGrupoFornecimento() {
        return grupoFornecimento;
    }

    public void setGrupoFornecimento(GrupoFornecimentoBean grupoFornecimento) {
        this.grupoFornecimento = grupoFornecimento;
    }

    public List<GrupoFornecimentoBean> getListaGrupos() {
        return listaGrupos;
    }

    public void setListaGrupos(List<GrupoFornecimentoBean> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }

    //###----------------------------------------------###//
    //###----------Geters and Seters-------------------###//
    //###----------------------------------------------###//
    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    //###----------------------------------------------###//
    //###--------Demais Métodos------------------------###//
    //###----------------------------------------------###//
    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.grupoFornecimento = new GrupoFornecimentoBean();
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaGrupos = Service.getInstance().listarGruposFornecimento();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaGrupos = Service.getInstance().pesquisarGruposFornecimento(pesquisa);
        }
    }

    public void prepareEditar(CategoriaFornecedorBean cat) {
        this.setCurrentState(EDITAR_STATE);
        this.categoria = cat;
        RequestContext.getCurrentInstance().execute("PF('addEditCategoriaPopup').show();");
    }

    public void prepareAdicionar() {
        this.clear();
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        Service.getInstance().excluirGruposFornecimento(grupoFornecimento);
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void prepareAdicionarCategoria() {
        this.setCurrentState(ADICIONAR_STATE);
        this.categoria = new CategoriaFornecedorBean();
        RequestContext.getCurrentInstance().execute("PF('addEditCategoriaPopup').show();");
    }

    public void clear() {
        this.grupoFornecimento = new GrupoFornecimentoBean();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            Service.getInstance().cadastraGruposFornecimento(grupoFornecimento);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarGruposFornecimento(grupoFornecimento);
        }
        this.pesquisar();
    }

    public void excluirCategoria() {
        logger.debug("EXCLUIR REGISTRO");
        Service.getInstance().excluirCategoriaFornecedor(categoria);
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deleteCategoriaPopup').hide();");
    }

    public void gravarCategoria() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            categoria.setFkGrupoFornecimento(grupoFornecimento);
            Service.getInstance().cadastrarCategoriaFornecedor(categoria);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarCategoriaFornecedor(categoria);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditCategoriaPopup').hide();");
    }

    public void atualizaRepresentante() {
        logger.debug("atualizar...");
        listaRepresentantes.clear();
        listaRepresentantes.addAll(categoria.getFkFornecedor().getRepresentantes());
    }
}
