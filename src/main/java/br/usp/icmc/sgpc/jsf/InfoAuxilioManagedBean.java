/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.BolsaAlocadaBean;
import br.usp.icmc.sgpc.beans.CentroDespesaBean;
import br.usp.icmc.sgpc.beans.CotaBolsaBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoDespesaBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoPagamentoBean;
import br.usp.icmc.sgpc.beans.HistoricoAuxilioBean;
import br.usp.icmc.sgpc.beans.LancamentoContaCorrenteBean;
import br.usp.icmc.sgpc.beans.ParticipanteAuxilioBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaProjetoBean;
import br.usp.icmc.sgpc.beans.PrestacaoContasBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.beans.ReciboBean;
import br.usp.icmc.sgpc.beans.RelatorioCientificoBean;
import br.usp.icmc.sgpc.beans.SubcentroDespesasBean;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.AuditoriaService;
import br.usp.icmc.sgpc.service.ContabilidadeService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author artur
 */
@ManagedBean(name = "infoAuxilioMB")
@ViewScoped
public class InfoAuxilioManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(InfoAuxilioManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private AuxilioBean auxilio;
    private AlineaBean alineaBean = new AlineaBean();
    private RelatorioCientificoBean relatorioCientifico = new RelatorioCientificoBean();
    private PrestacaoContasBean prestacaoContas = new PrestacaoContasBean();
    
    
    private List<String> listaMoedas = new ArrayList<String>();
    private String moeda;
    private BigDecimal valorTransposicaoOrigem;
    private BigDecimal valorTransposicaoDestino;
    private AlineaBean alineaRecebedora = new AlineaBean();
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/del.png";
    private String imagemCancelarBolsa =  "/images/icons/despesabw.png";
    private String imagemExcluirRegistroBolsa = "/images/icons/del.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private String imagemTransporRegistro = "/images/icons/transp.png";
    private String imagemExcluirRegistroCheque = "/images/icons/delbw.png";
    private String imagemRegistrarLiberacaoVerba = "/images/icons/edit-redo.png";
    private boolean podeExcluirDespesa;
    private String imagemVerDocDespesa = "/images/icons/visualizar.png";
    private String imagemFlagPrestacao = "/images/icons/flag_black.png";
    private String imagemFlagRelatorio = "/images/icons/flag_black.png";
    private DespesaBean despesa = new DespesaBean();
    
    private List<String> listaTiposDespesa = new ArrayList<String>();
    private List<DespesaBean> listaDespesas = new ArrayList<DespesaBean>();
    
    
    private List<CentroDespesaBean> listaCentrosDespesa;
    private CentroDespesaBean centroDespesa;
    private BigDecimal saldoDisponivel;
    
    private List<AlineaBean> listaAlineasDisponiveis = new ArrayList<AlineaBean>();
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private List<Integer> listaPeriodo = new ArrayList<Integer>();
    private int periodo;
    private boolean exibeBolsa = false;
    private List<BolsaAlocadaBean> listaBolsaAlocada = new ArrayList<BolsaAlocadaBean>();
    private BolsaAlocadaBean bolsaAlocada;
    private List<AlineaBean> listaAlineasBolsa = new ArrayList<AlineaBean>();
    private List<PessoaBean> listaPessoas = new ArrayList<PessoaBean>();
    private String pesquisaDespesa;
    private String statusDespesa;
    private List<String> listaStatusDespesas = new ArrayList<String>();
    private Date dataInicialDocFiscalPesquisaDespesas;
    private Date dataFinalDocFiscalPesquisaDespesas;
    private Date dataInicialChequePesquisaDespesas;
    private Date dataFinalChequePesquisaDespesas;
    
    
    private AlineaBean alineaPesquisa;
    
    
    private String tipoDocDespesa = "Nota Fiscal";
    private List<String> listaTiposDocDespesa = new ArrayList<String>();
    
    private String tipoDespesaPesquisa;
    private CotaBolsaBean cotaBolsa;
    private Integer idTipoAuxilioSelecionado;
    private List<SelectItem> listaTiposAuxilios = new ArrayList<SelectItem>();
    private Integer idModalidadeSelecionada;
    private List<SelectItem> listaModalidades = new ArrayList<SelectItem>();
    private UIComponent comboTipoAux;
    
    
    private List<SubcentroDespesasBean> listaSubcentrosDespesa = new ArrayList<SubcentroDespesasBean>();
    private SubcentroDespesasBean subcentroBolsaAlocada;
    
    
    //@ManagedProperty()
    private int idAuxilioParametro;
    
    private List<SelectItem> listaInstituicoes = new ArrayList<SelectItem>();
    private List<SelectItem> listaUnidades = new ArrayList<SelectItem>();
    
    HistoricoAuxilioBean hist = new HistoricoAuxilioBean();
    List<HistoricoAuxilioBean> historicoSubCentros = new ArrayList<HistoricoAuxilioBean>();
    
    ParticipanteAuxilioBean participanteAuxilio;
    List<PessoaProjetoBean> listaPessoasProjeto = new ArrayList<PessoaProjetoBean>();
    private List<String> listaTipoParticipacao = new ArrayList<String>();
    
    private List<HistoricoAuxilioBean> listaHistoricoAlteracoes = new ArrayList<HistoricoAuxilioBean>();
    private Date dataInicialPesquisaHistorico;
    private Date dataFinalPesquisaHistorico;
    private String alineaPesquisaHistorico;
    private int tipoPesquisaHistorico;
    private String centroDespesaPesquisaHistorico;
    private PessoaBean pessoaResponsavelPesquisaHistorico;
    private String acaoPesquisaHistorico;
    private List<String> listaAcoesHistorico;
    
    private String imagemRegistrarRecolhimentoVerba = "/images/icons/edit-undo.png";
    private String imagemRegistrarRecolhimentoVerbaGde = "/images/icons/edit-undo-gde.png";
    
    private BigDecimal valorPesquisaDespesa;
    private SubcentroDespesasBean subcentroAlineaRecebedora;
    private SubcentroDespesasBean subcentroAlineaDoadora;
    private BigDecimal saldoSubcentroRecebedor;
    private BigDecimal saldoSubcentroDoador;
    
    private String pesquisaDespesaNumeroDocFiscal;
    private String pesquisaDespesaNumeroCheque;
    
    public List<Integer> getListaPeriodo() {
        listaPeriodo.clear();
        for (int i = 1; i <= 24; i++) {
            listaPeriodo.add(i);
        }
        return listaPeriodo;
    }

    public void setListaPeriodo(List<Integer> listaPeriodo) {
        this.listaPeriodo = listaPeriodo;
    }

    public String getImagemCancelarBolsa() {
        return imagemCancelarBolsa;
    }

    public void setImagemCancelarBolsa(String imagemCancelarBolsa) {
        this.imagemCancelarBolsa = imagemCancelarBolsa;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public List<AlineaBean> getListaAlineasDisponiveis() {
        return listaAlineasDisponiveis;
    }

    public void setListaAlineasDisponiveis(List<AlineaBean> listaAlineasDisponiveis) {
        this.listaAlineasDisponiveis = listaAlineasDisponiveis;
    }

    public String getImagemExcluirRegistroBolsa() {
        return imagemExcluirRegistroBolsa;
    }

    public void setImagemExcluirRegistroBolsa(String imagemExcluirRegistroBolsa) {
        this.imagemExcluirRegistroBolsa = imagemExcluirRegistroBolsa;
    }

    public BigDecimal getSaldoDisponivel() {
        return saldoDisponivel;
    }

    public void setSaldoDisponivel(BigDecimal saldoDisponivel) {
        this.saldoDisponivel = saldoDisponivel;
    }

    public String getImagemFlagRelatorio() {
        return imagemFlagRelatorio;
    }

    public void setImagemFlagRelatorio(String imagemFlagRelatorio) {
        this.imagemFlagRelatorio = imagemFlagRelatorio;
    }

    public String getImagemFlagPrestacao() {
        return imagemFlagPrestacao;
    }

    public void setImagemFlagPrestacao(String imagemFlagPrestacao) {
        this.imagemFlagPrestacao = imagemFlagPrestacao;
    }

    public CentroDespesaBean getCentroDespesa() {
        return centroDespesa;
    }

    public void setCentroDespesa(CentroDespesaBean centroDespesa) {
        this.centroDespesa = centroDespesa;
    }

    public List<CentroDespesaBean> getListaCentrosDespesa() {
        return listaCentrosDespesa;
    }

    public void setListaCentrosDespesa(List<CentroDespesaBean> listaCentrosDespesa) {
        this.listaCentrosDespesa = listaCentrosDespesa;
    }

    public String getImagemVerDocDespesa() {
        return imagemVerDocDespesa;
    }

    public void setImagemVerDocDespesa(String imagemVerDocDespesa) {
        this.imagemVerDocDespesa = imagemVerDocDespesa;
    }
    /*Esses dois atributos boolean vão controlar o acesso do sistema de acordo com o Auxilio,
     * ou seja, o controle de Editar e Criar para Alineas, Relatorios e Prestacao de Contas
     * estarão atrelados ao Auxílio por essas duas variáveis.
     */
    private boolean podeCriarAuxilio;
    private boolean podeEditarAuxilio;

    public List<DespesaBean> getListaDespesas() {
        return listaDespesas;
    }

    public void setListaDespesas(List<DespesaBean> listaDespesas) {
        this.listaDespesas = listaDespesas;
    }

    public DespesaBean getDespesa() {
        return despesa;
    }

    public void setDespesa(DespesaBean despesa) {
        this.despesa = despesa;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public String getImagemTransporRegistro() {
        return imagemTransporRegistro;
    }

    public void setImagemTransporRegistro(String imagemTransporRegistro) {
        this.imagemTransporRegistro = imagemTransporRegistro;
    }

    public InfoAuxilioManagedBean() {
        logger.debug("INSTANCIADO Info Auxilio");
        //this.auxilio = staticAux;

    }

    public AlineaBean getAlineaRecebedora() {
        return alineaRecebedora;
    }

    public void setAlineaRecebedora(AlineaBean alineaRecebedora) {
        this.alineaRecebedora = alineaRecebedora;
    }

    public BigDecimal getValorTransposicaoOrigem() {
        return valorTransposicaoOrigem;
    }

    public void setValorTransposicaoOrigem(BigDecimal valorTransposicaoOrigem) {
        this.valorTransposicaoOrigem = valorTransposicaoOrigem;
    }

    public BigDecimal getValorTransposicaoDestino() {
        return valorTransposicaoDestino;
    }

    public void setValorTransposicaoDestino(BigDecimal valorTransposicaoDestino) {
        this.valorTransposicaoDestino = valorTransposicaoDestino;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public List<String> getListaTiposDespesa() {
        this.listaTiposDespesa.clear();
        this.listaTiposDespesa.add("CUSTEIO");
        this.listaTiposDespesa.add("CAPITAL");
        this.listaTiposDespesa.add("RESERVA TÉCNICA");
        this.listaTiposDespesa.add("BENEFICIO COMPLEMENTAR");
        return listaTiposDespesa;
    }

    public void setListaTiposDespesa(List<String> listaTiposDespesa) {
        this.listaTiposDespesa = listaTiposDespesa;
    }

    public List<String> getListaMoedas() {
        listaMoedas.clear();
        listaMoedas.add("Real");
        listaMoedas.add("Dólar US");
        return listaMoedas;
    }

    public void setListaMoedas(List<String> listaMoedas) {
        this.listaMoedas = listaMoedas;
    }

    public AlineaBean getAlineaBean() {
        return alineaBean;
    }

    public void setAlineaBean(AlineaBean alineaBean) {
        this.alineaBean = alineaBean;
    }

    public PrestacaoContasBean getPrestacaoContas() {
        return prestacaoContas;
    }

    public void setPrestacaoContas(PrestacaoContasBean prestacaoContas) {
        this.prestacaoContas = prestacaoContas;
    }

    public RelatorioCientificoBean getRelatorioCientifico() {
        return relatorioCientifico;
    }

    public void setRelatorioCientifico(RelatorioCientificoBean relatorioCientifico) {
        this.relatorioCientifico = relatorioCientifico;
    }

    public AuxilioBean getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(AuxilioBean auxilio) {
        this.auxilio = auxilio;
    }

    public String returnInfoProjeto() {
        atualizaProjeto();
        return "infoProjeto?faces-redirect=true";
    }

    public BigDecimal somaVerbaAprovada(AuxilioBean auxilio, int moeda) {
        return ContabilidadeService.getInstance().verbaAprovadaAuxilio(auxilio, moeda);
    }

    public void prepareAdicionar() {
        logger.debug("-------- Info AUX 2 ----Prepare Adicionar--");
        prestacaoContas = new PrestacaoContasBean();
        relatorioCientifico = new RelatorioCientificoBean();
        cotaBolsa = new CotaBolsaBean();
        cotaBolsa.setFkAuxilio(auxilio);
        idModalidadeSelecionada=null;
        idTipoAuxilioSelecionado = null;
        listaModalidades.clear();
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('editCotasBolsasPanel').show();");
    }

    public void prepareEditar() {
        this.setCurrentState(EDITAR_STATE);
    }

    public void excluirAlinea() {
        for(SubcentroDespesasBean subcentro : alineaBean.getSubcentrosDespesas()){
            hist = new HistoricoAuxilioBean();
            hist.setDataAlteracao(new Date());
            if(subcentro.isCentroDistribuidor()){
                hist.setAcao("Exclusão de centro distribuidor");
            } else {
                hist.setAcao("Exclusão de subcentro");
            }
            hist.setDescricao("Exclusão subcentro");
            hist.setValor(subcentro.getValor());
            hist.setNomeCentroDespesas(subcentro.getFkCentroDespesa().getNome());
            hist.setOperacao(HistoricoAuxilioBean.OPERACAO_DEBITO);
            hist.setNomeAlinea(alineaBean.getFkCategoriaAlinea().getNome());
            hist.setFkAuxilio(auxilio);
            hist.setFkResponsavel(userSessionMB.getLoggedUser());
            auxilio.getHistoricosAuxilio().add(hist);
            Service.getInstance().registrarHistoricoAuxilio(hist);
        }
        hist = new HistoricoAuxilioBean();
        hist.setDataAlteracao(new Date());
        DecimalFormat df = new DecimalFormat("###,##0.00");
        hist.setAcao("Exclusão de alínea.");
        hist.setDescricao("Exclusão de alínea.");
        hist.setValor(alineaBean.getVerbaAprovada());
        hist.setFkAuxilio(auxilio);
        hist.setNomeAlinea(alineaBean.getFkCategoriaAlinea().getNome());
        hist.setFkResponsavel(userSessionMB.getLoggedUser());
        
        this.auxilio.getAlineas().remove(this.alineaBean);
        Service.getInstance().excluirAlinea(this.alineaBean);
        //Service.getInstance().atualizarAuxilio(auxilio);
        //this.auxilio = Service.getInstance().buscarAuxilios(auxilio.getId());
        this.alineaBean = new AlineaBean();
        Service.getInstance().registrarHistoricoAuxilio(hist);
        atualizaAuxilio();
        listaAlineasDisponiveis = auxilio.getAlineas();
        pesquisarHistorico();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }



    public void prepareTransporAlineas(AlineaBean alinea) {
        this.alineaBean = alinea;
        alineaRecebedora = null;
        saldoSubcentroRecebedor = new BigDecimal(0);
        saldoSubcentroDoador = new BigDecimal(0);
        valorTransposicaoOrigem = new BigDecimal(0);
        valorTransposicaoDestino = new BigDecimal(0);
        if(alineaBean != null && alineaBean.getSubcentrosDespesas() != null && !alineaBean.getSubcentrosDespesas().isEmpty()){
            subcentroAlineaDoadora = alineaBean.getSubcentrosDespesas().get(0);
            saldoSubcentroDoador = saldoSubcentro(subcentroAlineaDoadora);
        }else{
            subcentroAlineaRecebedora = null;
        }
        RequestContext.getCurrentInstance().execute("PF('editTranspAlineaPanel').show();");
    }


    public void transporAlinea() {
        if(alineaRecebedora==null){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"É obrigatório indicar a Alínea Recebedora","É obrigatório indicar a Alínea Recebedora");
            FacesContext.getCurrentInstance().addMessage("comboAlineaRecebedoraTransposicao", message);
            return;
        }
        if(subcentroAlineaRecebedora==null){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"É obrigatório indicar o Subcentro da Alínea Recebedora","É obrigatório indicar a Alínea Recebedora");
            FacesContext.getCurrentInstance().addMessage("comboAlineaRecebedoraSubcentro", message);
            return;
        }
        if(alineaBean.equals(alineaRecebedora)){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Alínea Recebedora deve ser diferente da Alínea Doadora","Alínea Recebedora deve ser diferente da Alínea Doadora");
            FacesContext.getCurrentInstance().addMessage("comboAlineaRecebedoraTransposicao", message);
            return;
        }
        if(alineaRecebedora.getMoeda().compareTo(alineaBean.getMoeda()) == 0 && valorTransposicaoOrigem.compareTo(valorTransposicaoDestino) != 0){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Os valores de origem e destino são diferentes, apesar de a moeda ser a mesma.","Os valores de origem e destino são diferentes, apesar de a moeda ser a mesma.");
            FacesContext.getCurrentInstance().addMessage("valorTransposicaoDestino", message);
            return;
        }
        if (!this.alineaBean.equals(this.alineaRecebedora)) {
            this.alineaBean.setVerbaAprovada(this.alineaBean.getVerbaAprovada().subtract(valorTransposicaoOrigem));
            this.alineaRecebedora.setVerbaAprovada(this.alineaRecebedora.getVerbaAprovada().add(valorTransposicaoDestino));
            
            //subcentroAlineaDoadora.setValor(subcentroAlineaDoadora.getValor() - valorTransposicaoOrigem);
            for (SubcentroDespesasBean s : alineaBean.getSubcentrosDespesas()){
                if(s.equals(subcentroAlineaDoadora)){
                    s.setValor(s.getValor().subtract(valorTransposicaoOrigem));
                    break;
                }
            }
            //subcentroAlineaRecebedora.setValor(subcentroAlineaRecebedora.getValor() + valorTransposicaoDestino);
            for (SubcentroDespesasBean s : alineaRecebedora.getSubcentrosDespesas()){
                if(s.equals(subcentroAlineaRecebedora)){
                    s.setValor(s.getValor().add(valorTransposicaoDestino));
                    break;
                }
            }
            
            
            Service.getInstance().atualizarAlinea(this.alineaBean);
            Service.getInstance().atualizarAlinea(this.alineaRecebedora);
            atualizaAuxilio();
            AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Transpor Alínea", "Info Auxílio", "Transposição de alínea. Auxilio "
                    + this.auxilio.getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getNome() + " "
                    + this.auxilio.getProtocoloFinanciador()
                    + ". De " + this.alineaBean.getFkCategoriaAlinea().getNome() + "/" + subcentroAlineaDoadora.getFkCentroDespesa().getNome() + " Moeda: " + this.alineaBean.getMoeda() + " Valor: " + valorTransposicaoOrigem
                    + ". Para " + this.alineaRecebedora.getFkCategoriaAlinea().getNome() + "/" + subcentroAlineaRecebedora.getFkCentroDespesa().getNome() + " Moeda: " + this.alineaRecebedora.getMoeda() + " Valor: " + valorTransposicaoDestino
                    + ". Valor de " + valorTransposicaoOrigem
                    , ConfigConstantes.CONFIG_AUDITORIA_TRANSPOSICAO_ALINEA);
            // registrar historico
            DecimalFormat df = new DecimalFormat("###,##0.00");
            hist = new HistoricoAuxilioBean();
            hist.setDataAlteracao(new Date());
            hist.setAcao("Transposição de verba entre alíneas.");
            hist.setDescricao("Estorno de concessão");
            hist.setValor(valorTransposicaoOrigem);
            hist.setOperacao(HistoricoAuxilioBean.OPERACAO_DEBITO);
            hist.setFkAuxilio(auxilio);
            hist.setNomeAlinea(alineaBean.getFkCategoriaAlinea().getNome());
            hist.setNomeCentroDespesas(subcentroAlineaDoadora.getFkCentroDespesa().getNome());
            hist.setFkResponsavel(userSessionMB.getLoggedUser());
            Service.getInstance().registrarHistoricoAuxilio(hist);
            
            hist = new HistoricoAuxilioBean();
            hist.setDataAlteracao(new Date());
            hist.setAcao("Transposição de verba entre alíneas.");
            hist.setDescricao("Aditivo");
            hist.setValor(valorTransposicaoDestino);
            hist.setOperacao(HistoricoAuxilioBean.OPERACAO_CREDITO);
            hist.setFkAuxilio(auxilio);
            hist.setNomeAlinea(alineaRecebedora.getFkCategoriaAlinea().getNome());
            hist.setNomeCentroDespesas(subcentroAlineaRecebedora.getFkCentroDespesa().getNome());
            hist.setFkResponsavel(userSessionMB.getLoggedUser());
            Service.getInstance().registrarHistoricoAuxilio(hist);
        }
        this.alineaBean = new AlineaBean();
        this.alineaRecebedora = new AlineaBean();
        this.valorTransposicaoOrigem = new BigDecimal(0);
        pesquisarHistorico();
        RequestContext.getCurrentInstance().execute("PF('editTranspAlineaPanel').hide();");
    }
    
    public void cancelarTransporAlinea(){
        RequestContext.getCurrentInstance().execute("PF('editTranspAlineaPanel').hide();");
    }

    
    public void validateValorTransposicao(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        BigDecimal value = BigDecimal.valueOf(Double.valueOf(newValue.toString()));
        if (value.compareTo(saldoSubcentro(subcentroAlineaDoadora))==1) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Valor acima do limite","O valor da transposição supera o valor disponível da alínea.");
            facesContext.addMessage(component.getClientId(facesContext), message);
        }
    }

    public void atualizaProjeto() {
        ProjetoBean projeto = Service.getInstance().buscarProjeto(userSessionMB.getProjeto().getId());
        userSessionMB.setProjeto(projeto);
    }

    public void atualizaAuxilio() {
        this.auxilio = Service.getInstance().buscarAuxilios(this.auxilio.getId());
        userSessionMB.setAuxilio(this.auxilio);
        this.listaDespesas.clear();
        this.listaDespesas = Service.getInstance().listarDespesa(this.auxilio);

        atualizarBolsas();
        pesquisarHistorico();
    }

    public BolsaAlocadaBean getBolsaAlocada() {
        return bolsaAlocada;
    }

    public void setBolsaAlocada(BolsaAlocadaBean bolsaAlocada) {
        this.bolsaAlocada = bolsaAlocada;
    }

    public boolean isExibeBolsa() {
        return exibeBolsa;
    }

    public void setExibeBolsa(boolean exibeBolsa) {
        this.exibeBolsa = exibeBolsa;
    }

    public List<AlineaBean> getListaAlineasBolsa() {
        return listaAlineasBolsa;
    }

    public void setListaAlineasBolsa(List<AlineaBean> listaAlineasBolsa) {
        this.listaAlineasBolsa = listaAlineasBolsa;
    }

    public List<BolsaAlocadaBean> getListaBolsaAlocada() {
        return listaBolsaAlocada;
    }

    public void setListaBolsaAlocada(List<BolsaAlocadaBean> listaBolsaAlocada) {
        this.listaBolsaAlocada = listaBolsaAlocada;
    }

    public List<PessoaBean> getListaPessoas() {
        return listaPessoas;
    }

    public void setListaPessoas(List<PessoaBean> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }

    public void verificaAcesso() {
//        if (auxilio.getStatus().equals("Finalizado") || auxilio.getStatus().equals("Suspenso") || auxilio.getStatus().equals("Desistência") || auxilio.getStatus().equals("Cancelado") || auxilio.getStatus().equals("Não Aprovado")) {
//
//            imagemCriarRegistro = "/images/icons/add1bw.png";
//            imagemTransporRegistro = "/images/icons/transpbw.png";
//            imagemEditarRegistro = "/images/icons/editarbw.png";
//
//        } else {
//            if (podeCriarAuxilio) {
//
//                imagemCriarRegistro = "/images/icons/add1.png";
//                imagemTransporRegistro = "/images/icons/transp.png";
//            } else {
//                imagemCriarRegistro = "/images/icons/add1bw.png";
//                imagemTransporRegistro = "/images/icons/transpbw.png";
//            }
//            if (podeEditarAuxilio) {
//
//                imagemEditarRegistro = "/images/icons/editar.png";
//            } else {
//                imagemEditarRegistro = "/images/icons/editarbw.png";
//            }
//
//        }
    }


    public BigDecimal verbaAprovada(AuxilioBean auxilio, int moeda) {
        return ContabilidadeService.getInstance().verbaAprovadaAuxilio(auxilio,moeda);
    }

    public BigDecimal verbaDisponivel(AuxilioBean auxilio, int moeda) {
        return ContabilidadeService.getInstance().saldoAuxilio(auxilio,moeda);
    }

    public BigDecimal verbaGasta(AuxilioBean auxilio, int moeda) {
        return ContabilidadeService.getInstance().totalDespesaAuxilio(auxilio,moeda);
    }

    public BigDecimal despesaAlinea(AlineaBean alinea) {
        return ContabilidadeService.getInstance().totalDespesaAlinea(alinea);
    }

    public BigDecimal saldoAlinea(AlineaBean alinea) {
        return ContabilidadeService.getInstance().saldoAlinea(alinea);
    }

    public boolean exibeFlagPrestacao(PrestacaoContasBean prest) {
        if ("Agendada".equalsIgnoreCase(prest.getStatus()) || "Agendado".equalsIgnoreCase(prest.getStatus())) {
            imagemFlagPrestacao = "/images/icons/flag_yellow.png";
        }
        if ("Concluída".equalsIgnoreCase(prest.getStatus()) || "Concluído".equalsIgnoreCase(prest.getStatus())) {
            imagemFlagPrestacao = "/images/icons/flag_green.png";
        }
        if ("Atrasada".equalsIgnoreCase(prest.getStatus()) || "Atrasado".equalsIgnoreCase(prest.getStatus())) {
            imagemFlagPrestacao = "/images/icons/flag_red.png";
        }
        return true;
    }

    public boolean exibeFlagRelatorio(RelatorioCientificoBean rel) {

        if ("Agendada".equalsIgnoreCase(rel.getStatus()) || "Agendado".equalsIgnoreCase(rel.getStatus())) {
            imagemFlagRelatorio = "/images/icons/flag_yellow.png";
        }
        if ("Aprovada".equalsIgnoreCase(rel.getStatus()) || "Aprovado".equalsIgnoreCase(rel.getStatus())) {
            imagemFlagRelatorio = "/images/icons/flag_green.png";
        }
        if ("Atrasada".equalsIgnoreCase(rel.getStatus()) || "Atrasado".equalsIgnoreCase(rel.getStatus())) {
            imagemFlagRelatorio = "/images/icons/flag_red.png";
        }
        if ("Não Aprovada".equalsIgnoreCase(rel.getStatus()) || "Não Aprovado".equalsIgnoreCase(rel.getStatus())) {
            imagemFlagRelatorio = "/images/icons/flag_black.png";
        }
        if ("Entregue".equalsIgnoreCase(rel.getStatus())){
            imagemFlagRelatorio = "/images/icons/flag_blue.png";
        }
        return true;
    }




    public void atualizarListaAlineas(ValueChangeEvent event) {
        despesa.setTipoDespesa((String)event.getNewValue());
        this.alineaPesquisa = null;
        if(despesa.getTipoDespesa() == null){
            listaAlineasDisponiveis.clear();
            alineaPesquisa = null;
        }

        if ("CAPITAL".equalsIgnoreCase(despesa.getTipoDespesa())) {
            listaAlineasDisponiveis = Service.getInstance().listarAlineaCapital(auxilio);
        }
        if ("CUSTEIO".equalsIgnoreCase(despesa.getTipoDespesa())) {
            listaAlineasDisponiveis = Service.getInstance().listarAlineaCorrente(auxilio);
        }
        if ("RESERVA TÉCNICA".equalsIgnoreCase(despesa.getTipoDespesa())) {
            listaAlineasDisponiveis = Service.getInstance().listarAlineaReservaTecnica(auxilio);
        }
        if ("BENEFICIO COMPLEMENTAR".equalsIgnoreCase(despesa.getTipoDespesa())) {
            listaAlineasDisponiveis = Service.getInstance().listarAlineaBeneficioComplementar(auxilio);
        }
        saldoDisponivel = new BigDecimal(0);
        
    }

    public List<LancamentoContaCorrenteBean> getLancamentosContaCorrente() {
        return ContabilidadeService.getInstance().getLancamentosContaCorrente(auxilio);
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    @PostConstruct
    public void postConstruct() {
        //idAuxilioParametro = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("idAuxilioSelecionado"));
        logger.debug("@PostConstruct");
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();
        //this.auxilio = userSessionMB.getAuxilio();
        //this.auxilio = Service.getInstance().buscarAuxilios(idAuxilioParametro);
        this.auxilio = Service.getInstance().buscarAuxilios(userSessionMB.getAuxilio().getId());

        podeCriarAuxilio = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.AUXILIO_CRIAR);
        podeEditarAuxilio = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.AUXILIO_EDITAR);
        podeExcluirDespesa = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.DESPESA_EXCLUIR);

        
        listaDespesas = Service.getInstance().listarDespesa(auxilio);
        listaPessoas = Service.getInstance().listarPessoas();
        
        listaTiposAuxilios = GenericConverter.createSelectItems(auxilio.getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getTiposAuxilio());
        listaCentrosDespesa = Service.getInstance().listarCentrosDespesa();
        

        atualizarBolsas();
        verificaAcesso();
        
        listaTipoParticipacao.add("Bolsista");
        listaTipoParticipacao.add("Colaborador");
        listaTipoParticipacao.add("Pesquisador principal com BC");
        listaTipoParticipacao.add("Pesquisador principal sem BC");
        
        pesquisarHistorico();
        listaAlineasDisponiveis = auxilio.getAlineas();
        listaAcoesHistorico = Service.getInstance().getUniqueHistoryActions();
    }

    public StreamedContent downloadBalancete() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteBalancete(auxilio, ReportService.FORMATO_PDF);
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public void prepareEditarBolsa(BolsaAlocadaBean bolsa) {
        this.setCurrentState(EDITAR_STATE);
        this.bolsaAlocada = bolsa;
        this.alineaBean = bolsaAlocada.getFkAlinea();
        List<DespesaBean> despesasBolsa = Service.getInstance().listarDespesaBolsa(bolsaAlocada);
        if(despesasBolsa!=null && !despesasBolsa.isEmpty()){
            this.subcentroBolsaAlocada = despesasBolsa.get(0).getFkSubcentro();
        }
        listaCentrosDespesa = Service.getInstance().listarCentrosDespesa();
        RequestContext.getCurrentInstance().execute("PF('editBolsaPanel').show();");
    }

    public void prepareCadastrarBolsa() {
        bolsaAlocada = new BolsaAlocadaBean();
        listaCentrosDespesa = Service.getInstance().listarCentrosDespesa();
        this.despesa = new DespesaBean();
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('editBolsaPanel').show();");
    }

    public void gravarBolsa() {
        boolean temErro = false;
        if(subcentroBolsaAlocada==null){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Dados inválidos","É necessário indicar o centro de despesas.");
            FacesContext.getCurrentInstance().addMessage("valorBolsa", message);
            return;
        }
        saldoDisponivel = ContabilidadeService.getInstance().saldoSubcentro(subcentroBolsaAlocada);
        if(bolsaAlocada.getValor().multiply(new BigDecimal(periodo)).compareTo(saldoDisponivel) == 1){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Valor acima do limite","O valor da despesa supera o valor disponível do centro de despesas.");
            FacesContext.getCurrentInstance().addMessage("valorBolsa", message);
            return;
        }
        
        if(bolsaAlocada.getDataInicio().after(auxilio.getDataFinal())){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Data inválida","A bolsa deve ter início antes do final da vigência do auxílio.");
            FacesContext.getCurrentInstance().addMessage("dataIni", message);
            temErro = true;
        }
        
        if(bolsaAlocada.getDataInicio().before(auxilio.getDataInicial())){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Data inválida","A bolsa deve ter início depois do início da vigência do auxílio.");
            FacesContext.getCurrentInstance().addMessage("dataIni", message);
            temErro = true;
        }
        
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(bolsaAlocada.getDataInicio());
        gc.add(GregorianCalendar.MONTH, periodo);
        Date dataFim = gc.getTime();
        
        if(dataFim.after(auxilio.getDataFinal())){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Data inválida","A bolsa deve ter fim antes do final da vigência do auxílio.");
            FacesContext.getCurrentInstance().addMessage("dataIni", message);
            temErro = true;
        }        
        
        if(!temErro){
            if (ADICIONAR_STATE.equals(this.currentState)) {
                logger.debug("CADASTRAR BOLSA");
                logger.debug("---> Períodos:" + periodo);

                bolsaAlocada.setDataFim(dataFim);
                bolsaAlocada.setStatus("Vigente");
                bolsaAlocada.setFkAlinea(alineaBean);
                bolsaAlocada = Service.getInstance().cadastrarBolsaAlocada(bolsaAlocada);

                //for (int i = 1; i <= periodo; i++) {
                DespesaBean despesa = new DespesaBean();
                //despesa.setObservacao("Bolsa Alocada: " + bolsaAlocada.getFkPessoa().getNome() + " - Período " + periodo + " mês(es)");
                despesa.setObservacao("Despesa gerada automaticamente durante alocação de bolsa.");

                despesa.setDataRealizada(dataFim);

                despesa.setStatus("Em Elaboração");
                despesa.setFkAlinea(bolsaAlocada.getFkAlinea());
                if (bolsaAlocada.getFkAlinea().getFkCategoriaAlinea().isCapital()) {
                    despesa.setTipoDespesa("capital");
                } else {
                    despesa.setTipoDespesa("CUSTEIO");
                }
                despesa.setValor(bolsaAlocada.getValor().multiply(new BigDecimal(periodo)));
                despesa.setFkBolsaAlocada(bolsaAlocada);
                despesa.setGenero(1);//1=recibo; 0=notafiscal
                despesa.setFavorecidoCpf(bolsaAlocada.getFkPessoa().getCpf());
                despesa.setFavorecidoNome(bolsaAlocada.getFkPessoa().getNome());
                despesa.setDescricao("Bolsa alocada pelo período de " + periodo + " mes(es)");
                despesa.setFkSubcentro(subcentroBolsaAlocada);
                Service.getInstance().cadastraDespesa(despesa);

                PessoaProjetoBean pessoaProjeto = new PessoaProjetoBean();
                pessoaProjeto.setFkProjeto(auxilio.getFkProjeto());
                pessoaProjeto.setFkPessoa(bolsaAlocada.getFkPessoa());
                pessoaProjeto.setDataAssociacao(bolsaAlocada.getDataInicio());
                pessoaProjeto.setTipo("Bolsista");
                
                ParticipanteAuxilioBean participanteAux = new ParticipanteAuxilioBean();
                participanteAux.setDataAssociacao(new Date());
                participanteAux.setFkAuxilio(auxilio);
                participanteAux.setFkPessoaProjeto(pessoaProjeto);
                participanteAux.setTipo("Bolsista");
                
                List<PessoaProjetoBean> listaPessoasProjeto = Service.getInstance().buscarPessoasProjeto(auxilio.getFkProjeto());

                if (!listaPessoasProjeto.contains(pessoaProjeto)) {
                    Service.getInstance().cadastraPessoaProjeto(pessoaProjeto);
                    AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Info Projeto", "Adicionou participante ao projeto de id: " + this.auxilio.getFkProjeto().getId() + " - Título: " + this.auxilio.getFkProjeto().getTitulo()
                            + " - Nome do Participante: " + pessoaProjeto.getFkPessoa().getNome() + " - Username: " + pessoaProjeto.getFkPessoa().getUsername(), ConfigConstantes.CONFIG_AUDITORIA_ADD_PARTICIPANTE_PROJETO);
                }
                
                DecimalFormat df = new DecimalFormat("###,##0.00");
                hist = new HistoricoAuxilioBean();
                hist.setAcao("Inclusão de Bolsa");
                hist.setDataAlteracao(new Date());
                hist.setNomeAlinea(bolsaAlocada.getFkAlinea().getFkCategoriaAlinea().getNome());
                hist.setDescricao("Valor mensal: " + df.format(bolsaAlocada.getValor()) 
                        + ". De " + bolsaAlocada.getDataInicio() + " até " + bolsaAlocada.getDataFim() 
                        + ". Bolsista: " + bolsaAlocada.getFkPessoa().getNome());
                hist.setFkAuxilio(auxilio);
                hist.setValor(bolsaAlocada.getValor().multiply(new BigDecimal(periodo)));
                hist.setOperacao(HistoricoAuxilioBean.OPERACAO_DEBITO);
                hist.setNomeCentroDespesas(subcentroBolsaAlocada.getFkCentroDespesa().getNome());
                hist.setFkResponsavel(userSessionMB.getLoggedUser());
                Service.getInstance().registrarHistoricoAuxilio(hist);

            } else if (EDITAR_STATE.equals(this.currentState)) {
                logger.debug("ATUALIZAR BOLSA");
                bolsaAlocada.setFkAlinea(alineaBean);

                List<DespesaBean> despesasBolsa = Service.getInstance().listarDespesaBolsa(bolsaAlocada);
                for (int i = despesasBolsa.size() - 1; i >= 0; i--) {
                    DespesaBean despesa = despesasBolsa.get(i);
                    Service.getInstance().excluirDespesa(despesa);

                    despesa.setObservacao("Despesa gerada automaticamente durante alocação de bolsa.");
                    despesa.setDataRealizada(dataFim);
                    despesa.setStatus("Em Elaboração");
                    despesa.setFkAlinea(bolsaAlocada.getFkAlinea());
                    if (bolsaAlocada.getFkAlinea().getFkCategoriaAlinea().isCapital()) {
                        despesa.setTipoDespesa("CAPITAL");
                    } else {
                        despesa.setTipoDespesa("CUSTEIO");
                    }
                    despesa.setValor(bolsaAlocada.getValor().multiply(new BigDecimal(periodo)));
                    despesa.setGenero(1);//1=recibo; 0=notafiscal
                    despesa.setFavorecidoCpf(bolsaAlocada.getFkPessoa().getCpf());
                    despesa.setFavorecidoNome(bolsaAlocada.getFkPessoa().getNome());
                    despesa.setDescricao("Bolsa alocada pelo período de " + periodo + " mes(es)");
                    despesa.setFkSubcentro(subcentroBolsaAlocada);

                    Service.getInstance().atualizarDespesa(despesa);
                    
                    DecimalFormat df = new DecimalFormat("###,##0.00");
                    hist = new HistoricoAuxilioBean();
                    hist.setAcao("Alteração de Bolsa");
                    hist.setDataAlteracao(new Date());
                    hist.setNomeAlinea(bolsaAlocada.getFkAlinea().getFkCategoriaAlinea().getNome());
                    hist.setDescricao("Valor mensal: " + df.format(bolsaAlocada.getValor())
                            + ". De " + bolsaAlocada.getDataInicio() + " até " + bolsaAlocada.getDataFim() 
                            + ". Bolsista: " + bolsaAlocada.getFkPessoa().getNome());
                    hist.setValor(bolsaAlocada.getValor().multiply(new BigDecimal(periodo)));
                    hist.setNomeCentroDespesas(subcentroBolsaAlocada.getFkCentroDespesa().getNome());
                    hist.setFkAuxilio(auxilio);
                    hist.setFkResponsavel(userSessionMB.getLoggedUser());
                    Service.getInstance().registrarHistoricoAuxilio(hist);
                }

                bolsaAlocada.setDataFim(dataFim);
                Service.getInstance().atualizarBolsaAlocada(bolsaAlocada);
            }
            atualizaAuxilio();
        }
        RequestContext.getCurrentInstance().execute("PF('editBolsaPanel').hide();");
    }

    public void cancelarBolsa() {
        List<DespesaBean> despesasBolsa = Service.getInstance().listarDespesaBolsa(bolsaAlocada);
        GregorianCalendar inicio = new GregorianCalendar();
        GregorianCalendar hoje = new GregorianCalendar();
        hoje.setTime(new Date());

        for (int i = despesasBolsa.size() - 1; i >= 0; i--) {
            DespesaBean despesa = despesasBolsa.get(i);
            inicio.setTime(bolsaAlocada.getDataInicio());
            int meses = 0;
            while (inicio.before(hoje)) {
                inicio.add(GregorianCalendar.MONTH, 1);
                meses++;
            }
            inicio.setTime(bolsaAlocada.getDataInicio());
            inicio.add(GregorianCalendar.MONTH, meses);
            despesa.setValor(bolsaAlocada.getValor().multiply(new BigDecimal(meses)));
            despesa.setObservacao("Bolsa Alocada: " + bolsaAlocada.getFkPessoa().getNome() + " - Período " + meses + " mes(es)");

            despesa.setFkBolsaAlocada(null);
            Service.getInstance().atualizarDespesa(despesa);
        }
        bolsaAlocada.setStatus("Cancelada");
        Service.getInstance().atualizarBolsaAlocada(bolsaAlocada);
        atualizaAuxilio();
        
        DecimalFormat df = new DecimalFormat("###,##0.00");
        hist = new HistoricoAuxilioBean();
        hist.setAcao("Cancelamento de Bolsa");
        hist.setDataAlteracao(new Date());
        hist.setNomeAlinea(bolsaAlocada.getFkAlinea().getFkCategoriaAlinea().getNome());
        hist.setDescricao("Valor mensal: " + df.format(bolsaAlocada.getValor()) 
                + ". De " + bolsaAlocada.getDataInicio() + " até " + bolsaAlocada.getDataFim() 
                + ". Bolsista: " + bolsaAlocada.getFkPessoa().getNome());
        hist.setFkAuxilio(auxilio);
        hist.setValor(bolsaAlocada.getValor().multiply(new BigDecimal(periodo)));
        hist.setOperacao(HistoricoAuxilioBean.OPERACAO_CREDITO);
        hist.setNomeCentroDespesas(subcentroBolsaAlocada.getFkCentroDespesa().getNome());
        hist.setFkResponsavel(userSessionMB.getLoggedUser());
        Service.getInstance().registrarHistoricoAuxilio(hist);
        RequestContext.getCurrentInstance().execute("PF('cancelaBolsaPopup').hide();");
    }

    public void atualizarBolsas() {
        listaAlineasBolsa.clear();
        listaBolsaAlocada.clear();
        List<AlineaBean> listaAlineas = auxilio.getAlineas();
        for (AlineaBean alinea : listaAlineas) {
            if (alinea.getFkCategoriaAlinea().isBolsa()) {
                exibeBolsa = true;
                listaAlineasBolsa.add(alinea);
            }
        }
        for (AlineaBean alinea : listaAlineasBolsa) {
            listaBolsaAlocada.addAll(alinea.getBolsasAlocadas());
        }
    }

    public void excluirBolsa() {
        DecimalFormat df = new DecimalFormat("###,##0.00");
        List<DespesaBean> despesasBolsa = Service.getInstance().listarDespesaBolsa(bolsaAlocada);
        for(DespesaBean desp : despesasBolsa){
            desp.setFkBolsaAlocada(null);
            Service.getInstance().excluirDespesa(desp);
        }
//        PessoaProjetoBean pp = Service.getInstance().buscarPessoaProjeto(bolsaAlocada.getFkPessoa());
        //auxilio.getFkProjeto().getPessoasProjeto().remove(pp);
        //Service.getInstance().excluirPessoaProjeto(pp);
        Service.getInstance().atualizarProjeto(auxilio.getFkProjeto());
        Service.getInstance().excluirBolsaAlocada(bolsaAlocada);
        atualizaAuxilio();
        
        hist = new HistoricoAuxilioBean();
        hist.setAcao("Exclusão de Bolsa");
        hist.setDataAlteracao(new Date());
        hist.setNomeAlinea(bolsaAlocada.getFkAlinea().getFkCategoriaAlinea().getNome());
        hist.setDescricao("Valor mensal: " + df.format(bolsaAlocada.getValor()) 
                + ". De " + bolsaAlocada.getDataInicio() + " até " + bolsaAlocada.getDataFim() 
                + ". Bolsista: " + bolsaAlocada.getFkPessoa().getNome());
        hist.setValor(bolsaAlocada.getValor().multiply(new BigDecimal(periodo)));
        hist.setOperacao(HistoricoAuxilioBean.OPERACAO_CREDITO);
        hist.setNomeCentroDespesas(subcentroBolsaAlocada.getFkCentroDespesa().getNome());
        hist.setFkAuxilio(auxilio);
        hist.setFkResponsavel(userSessionMB.getLoggedUser());
        Service.getInstance().registrarHistoricoAuxilio(hist);
        RequestContext.getCurrentInstance().execute("PF('deleteBolsaPopup').hide();");
    }

    public String getPesquisaDespesa() {
        return pesquisaDespesa;
    }

    public void setPesquisaDespesa(String pesquisaDespesa) {
        this.pesquisaDespesa = pesquisaDespesa;
    }

    public String getStatusDespesa() {
        return statusDespesa;
    }

    public void setStatusDespesa(String statusDespesa) {
        this.statusDespesa = statusDespesa;
    }

    public void pesquisarDespesas(boolean pesquisaTodosOsCampos) {
        logger.debug("PESQUISAR DESPESAS");

        this.listaDespesas = Service.getInstance().pesquisarDespesa(null, pesquisaDespesa, null, null, statusDespesa, centroDespesa, this.auxilio, alineaPesquisa, tipoDespesaPesquisa, dataInicialDocFiscalPesquisaDespesas, dataFinalDocFiscalPesquisaDespesas, valorPesquisaDespesa, dataInicialChequePesquisaDespesas, dataFinalChequePesquisaDespesas, pesquisaDespesaNumeroCheque, pesquisaDespesaNumeroDocFiscal);
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioDespesas(listaDespesas, ReportService.FORMATO_PDF, listarFiltrosAtivosDespesas());
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioDespesas(listaDespesas, ReportService.FORMATO_XLS, listarFiltrosAtivosDespesas());
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    public void bindDataRealizadaInicial(SelectEvent event) {
        dataInicialDocFiscalPesquisaDespesas = (Date) event.getObject();
        pesquisarDespesas(false);
    }

    public Date getDataInicialDocFiscalPesquisaDespesas() {
        return dataInicialDocFiscalPesquisaDespesas;
    }

    public void setDataInicialDocFiscalPesquisaDespesas(Date dataInicialDocFiscalPesquisaDespesas) {
        this.dataInicialDocFiscalPesquisaDespesas = dataInicialDocFiscalPesquisaDespesas;
    }

    public Date getDataFinalDocFiscalPesquisaDespesas() {
        return dataFinalDocFiscalPesquisaDespesas;
    }

    public void setDataFinalDocFiscalPesquisaDespesas(Date dataFinalDocFiscalPesquisaDespesas) {
        this.dataFinalDocFiscalPesquisaDespesas = dataFinalDocFiscalPesquisaDespesas;
    }
    
    public void bindDataRealizadaFinal(SelectEvent event) {
        dataFinalDocFiscalPesquisaDespesas = (Date) event.getObject();
        pesquisarDespesas(false);
    }

    public List<String> getListaStatusDespesas() {
        listaStatusDespesas.clear();
        listaStatusDespesas.add("Em Elaboração");
        listaStatusDespesas.add("Em Análise");
        listaStatusDespesas.add("Aprovado");
        listaStatusDespesas.add("Não Aprovado");
        listaStatusDespesas.add("Em Andamento");
        listaStatusDespesas.add("Suspenso");
        listaStatusDespesas.add("Desistência");
        listaStatusDespesas.add("Transferido");
        listaStatusDespesas.add("Cancelado");
        listaStatusDespesas.add("Finalizado");

        return listaStatusDespesas;
    }

    public void setListaStatusDespesas(List<String> listaStatusDespesas) {
        this.listaStatusDespesas = listaStatusDespesas;
    }

    public boolean isPodeCriarAuxilio() {
        return podeCriarAuxilio;
    }

    public void setPodeCriarAuxilio(boolean podeCriarAuxilio) {
        this.podeCriarAuxilio = podeCriarAuxilio;
    }

    public boolean isPodeEditarAuxilio() {
        return podeEditarAuxilio;
    }

    public void setPodeEditarAuxilio(boolean podeEditarAuxilio) {
        this.podeEditarAuxilio = podeEditarAuxilio;
    }



    public AlineaBean getAlineaPesquisa() {
        return alineaPesquisa;
    }

    public void setAlineaPesquisa(AlineaBean alineaPesquisa) {
        this.alineaPesquisa = alineaPesquisa;
    }

    public List<String> getListaTiposDocDespesa() {
        listaTiposDocDespesa.clear();
        listaTiposDocDespesa.add("Nota Fiscal");
        listaTiposDocDespesa.add("Recibo");
        return listaTiposDocDespesa;
    }

    public void setListaTiposDocDespesa(List<String> listaTiposDocDespesa) {
        this.listaTiposDocDespesa = listaTiposDocDespesa;
    }

    public String getTipoDocDespesa() {
        return tipoDocDespesa;
    }

    public void setTipoDocDespesa(String tipoDocDespesa) {
        this.tipoDocDespesa = tipoDocDespesa;
    }

    public String getTipoDespesaPesquisa() {
        return tipoDespesaPesquisa;
    }

    public void setTipoDespesaPesquisa(String tipoDespesaPesquisa) {
        this.tipoDespesaPesquisa = tipoDespesaPesquisa;
    }

    public StreamedContent emitirReciboRTF(DocumentoComprovacaoDespesaBean doccd){
        ReciboBean r = (ReciboBean) doccd;
        InputStream stream = ReportService.getInstance().emiteRelatorioReciboDespesa(r, ReportService.FORMATO_RTF);
        StreamedContent file = new DefaultStreamedContent(stream, "application/msword", "report.doc");
        return file;
    }

    public String notasFiscais(DespesaBean despesa){
        StringBuilder retorno = new StringBuilder();
        for(DocumentoComprovacaoDespesaBean doc : despesa.getDocumentosComprovacaoDespesas()){
            retorno.append(doc.getNumero());
            retorno.append(" ");
        }
        return retorno.toString();
    }

    public void excluirDespesa(){
        for(AlineaBean alineaTemp : auxilio.getAlineas()){
            alineaTemp.getDespesas().remove(despesa);
            for(SubcentroDespesasBean subcentroTemp : alineaTemp.getSubcentrosDespesas()){
                subcentroTemp.getDespesas().remove(despesa);
            }
        }
        for(PrestacaoContasBean prestacaoTemp : auxilio.getPrestacaoContas()){
            prestacaoTemp.getDespesas().remove(despesa);
        }
        Service.getInstance().excluirDespesa(despesa);
        despesa=null;
        atualizaAuxilio();
    }

    public boolean isPodeExcluirDespesa() {
        return podeExcluirDespesa;
    }

    public void setPodeExcluirDespesa(boolean podeExcluirDespesa) {
        this.podeExcluirDespesa = podeExcluirDespesa;
    }

    public Date datasEmissaoDocsFiscais(DespesaBean despesa){
        if(!despesa.getDocumentosComprovacaoDespesas().isEmpty()){
            return despesa.getDocumentosComprovacaoDespesas().get(0).getDataEmissao();
        }
        return null;
    }
    
    public Date datasEmissaoCheques(DespesaBean despesa){
        if(!despesa.getDocumentosComprovacaoPagamentos().isEmpty()){
            return despesa.getDocumentosComprovacaoPagamentos().get(0).getDataEmissao();
        }
        return null;
    }

    public void validateValorBolsa(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        saldoDisponivel = ContabilidadeService.getInstance().saldoAlinea(alineaBean);
//        logger.debug("Saldo disponível: " + saldoDisponivel);
        logger.debug("periodo: " + periodo);
        BigDecimal value = BigDecimal.valueOf(Double.valueOf(newValue.toString()));
        logger.debug("valor mensal: " + value);
        logger.debug("valor total: " + value.multiply(new BigDecimal(periodo)));
        if (value.compareTo(new BigDecimal(0)) == 0) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Valor não pode ser zero (0).","Valor não pode ser zero (0).");
            facesContext.addMessage(component.getClientId(facesContext), message);
        }else if(value.multiply(new BigDecimal(periodo)).compareTo(saldoDisponivel) == 1){
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Valor acima do limite","O valor da despesa supera o valor disponível da alínea.");
            facesContext.addMessage(component.getClientId(facesContext), message);
        }else{
            despesa.setValor(value);
        }
    }


    public String getImagemExcluirRegistroCheque() {
        return imagemExcluirRegistroCheque;
    }

    public void setImagemExcluirRegistroCheque(String imagemExcluirRegistroCheque) {
        this.imagemExcluirRegistroCheque = imagemExcluirRegistroCheque;
    }



    public int getIdAuxilioParametro() {
        return idAuxilioParametro;
    }

    public void setIdAuxilioParametro(int idAuxilioParametro) {
        this.idAuxilioParametro = idAuxilioParametro;
    }
    
    public String numerosCheques(DespesaBean despesa){
        String retorno="";
        if(!despesa.getDocumentosComprovacaoPagamentos().isEmpty()){
            retorno = despesa.getDocumentosComprovacaoPagamentos().get(0).getNumero();
        }
        if(despesa.getDocumentosComprovacaoPagamentos().size()>1){
            retorno += " +";
        }
        return retorno;
    }

    public CotaBolsaBean getCotaBolsa() {
        return cotaBolsa;
    }

    public void setCotaBolsa(CotaBolsaBean cotaBolsa) {
        this.cotaBolsa = cotaBolsa;
    }
    
    public void salvarCotaBolsa(){
        this.cotaBolsa.setFkModalidade(Service.getInstance().buscarModalidade(idModalidadeSelecionada));
        if (ADICIONAR_STATE.equals(this.currentState)) {
            this.auxilio.getCotasBolsas().add(this.cotaBolsa);
            Service.getInstance().atualizarAuxilio(auxilio);
            this.cotaBolsa = new CotaBolsaBean();
        } else if (EDITAR_STATE.equals(this.currentState)) {
            Service.getInstance().atualizarCotaBolsa(this.cotaBolsa);
            this.cotaBolsa = new CotaBolsaBean();
        }
        atualizaAuxilio();
        RequestContext.getCurrentInstance().execute("PF('editCotasBolsasPanel').hide();");
    }
    
    public void excluirCotaBolsa(){
        this.auxilio.getCotasBolsas().remove(this.cotaBolsa);
        Service.getInstance().excluirCotaBolsa(this.cotaBolsa);
        this.cotaBolsa = new CotaBolsaBean();
        atualizaAuxilio();
        RequestContext.getCurrentInstance().execute("PF('deletePopupCotasBolsas').hide();");
    }
 
    public void prepareEditarCotasBolsas(CotaBolsaBean cota){
        this.setCurrentState(EDITAR_STATE);
        this.cotaBolsa = cota;
        this.idTipoAuxilioSelecionado = cotaBolsa.getFkModalidade().getFkTipoAuxilio().getId();
        atualizaModalidade();
        this.idModalidadeSelecionada = cotaBolsa.getFkModalidade().getId();
        RequestContext.getCurrentInstance().execute("PF('editCotasBolsasPanel').show();");
    }

    public List<SelectItem> getListaModalidades() {
        return listaModalidades;
    }

    public void setListaModalidades(List<SelectItem> listaModalidades) {
        this.listaModalidades = listaModalidades;
    }

    public List<SelectItem> getListaTiposAuxilios() {
        return listaTiposAuxilios;
    }

    public void setListaTiposAuxilios(List<SelectItem> listaTiposAuxilios) {
        this.listaTiposAuxilios = listaTiposAuxilios;
    }
    
    public Integer getIdTipoAuxilioSelecionado() {
        return idTipoAuxilioSelecionado;
    }

    public void setIdTipoAuxilioSelecionado(Integer idTipoAuxilioSelecionado) {
        this.idTipoAuxilioSelecionado = idTipoAuxilioSelecionado;
    }

    public Integer getIdModalidadeSelecionada() {
        return idModalidadeSelecionada;
    }

    public void setIdModalidadeSelecionada(Integer idModalidadeSelecionada) {
        this.idModalidadeSelecionada = idModalidadeSelecionada;
    }

    public void atualizaModalidade() {
        logger.debug("========== atualizaModalidade");
        this.listaModalidades.clear();
        if(idTipoAuxilioSelecionado!=null){
            logger.debug("     ===== idTipoAuxilioSelecionado!=null");
            TipoAuxilioBean tipo = Service.getInstance().buscarTiposAuxilio(idTipoAuxilioSelecionado);
            this.listaModalidades = GenericConverter.createSelectItems(tipo.getModalidades());
        }else{
            logger.debug("     ===== idTipoAuxilioSelecionado==null");
        }
        idModalidadeSelecionada = null;
    }

    public UIComponent getComboTipoAux() {
        return comboTipoAux;
    }

    public void setComboTipoAux(UIComponent comboTipoAux) {
        this.comboTipoAux = comboTipoAux;
    }
    
    public List<SelectItem> getListaInstituicoes() {
        return listaInstituicoes;
    }

    public void setListaInstituicoes(List<SelectItem> listaInstituicoes) {
        this.listaInstituicoes = listaInstituicoes;
    }

    public List<SelectItem> getListaUnidades() {
        return listaUnidades;
    }

    public void setListaUnidades(List<SelectItem> listaUnidades) {
        this.listaUnidades = listaUnidades;
    }

    public String alineas(DespesaBean despesa){
        StringBuilder retorno = new StringBuilder();
        retorno.append(despesa.getFkAlinea().getFkCategoriaAlinea().getNome());
        if(despesa.getSubalinea()!=null && !despesa.getSubalinea().isEmpty()){
            retorno.append(" / ");
            retorno.append(despesa.getSubalinea());
        }
        return retorno.toString();
    }
    
    public StreamedContent gerarPrestacaoContasPdf(PrestacaoContasBean prest){
        logger.debug("GERAR PRESTACAO CONTAS PDF");
//        InputStream stream = ReportService.getInstance().emitePrestacaoDeContas(prest, ReportService.FORMATO_PDF);
//        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
//        return file;
        Date dataInicial = Service.getInstance().calcularDataInicialPrestacao(prest);
        Date dataFinal = prest.getDataLimite();
        List<DespesaBean> lista = Service.getInstance().pesquisar(auxilio, dataInicial, dataFinal);

        InputStream stream = ReportService.getInstance().emitePrestacaoDeContas(lista, dataInicial, dataFinal, ReportService.FORMATO_PDF);
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }
    
    public void filtraTipoDespesaPesquisa(){
        
        pesquisarDespesas(false);
    }
    
    public void validateDataInicialPesquisaDespesas(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        if(newValue == null){
            dataInicialDocFiscalPesquisaDespesas = null;
        }
    }
    
    public void validateDataFinalPesquisaDespesas(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        if(newValue == null){
            dataFinalDocFiscalPesquisaDespesas = null;
        }
    }

    public List<HistoricoAuxilioBean> getListaHistoricoAlteracoes() {
        return listaHistoricoAlteracoes;
    }

    public void setListaHistoricoAlteracoes(List<HistoricoAuxilioBean> listaHistoricoAlteracoes) {
        // metodo nao implementado
    }

    public String getImagemRegistrarLiberacaoVerba() {
        return imagemRegistrarLiberacaoVerba;
    }

    public void setImagemRegistrarLiberacaoVerba(String imagemRegistrarLiberacaoVerba) {
        this.imagemRegistrarLiberacaoVerba = imagemRegistrarLiberacaoVerba;
    }

    public HistoricoAuxilioBean getHist() {
        return hist;
    }

    public void setHist(HistoricoAuxilioBean hist) {
        this.hist = hist;
    }
    
    public List<SubcentroDespesasBean> getListaSubcentrosDespesa() {
        return listaSubcentrosDespesa;
    }

    public void setListaSubcentrosDespesa(List<SubcentroDespesasBean> listaSubcentrosDespesa) {
        this.listaSubcentrosDespesa = listaSubcentrosDespesa;
    }

    
    public BigDecimal despesaSubcentro(SubcentroDespesasBean subcentro) {
        return ContabilidadeService.getInstance().totalDespesaSubcentroPorAlinea(subcentro);
    }
    
    public BigDecimal saldoSubcentro(SubcentroDespesasBean subcentro) {
        return ContabilidadeService.getInstance().saldoSubcentro(subcentro);
    }
    public StreamedContent gerarRelatorioSubcentroPDF(SubcentroDespesasBean subcentro) {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioDespesasSubcentro(subcentro, ReportService.FORMATO_PDF);
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }
    public StreamedContent gerarRelatorioSubcentroXLS(SubcentroDespesasBean subcentro) {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioDespesasSubcentro(subcentro, ReportService.FORMATO_XLS);
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    public ParticipanteAuxilioBean getParticipanteAuxilio() {
        return participanteAuxilio;
    }

    public void setParticipanteAuxilio(ParticipanteAuxilioBean participanteAuxilio) {
        this.participanteAuxilio = participanteAuxilio;
    }

    public List<PessoaProjetoBean> getListaPessoasProjeto() {
        return listaPessoasProjeto;
    }

    public void setListaPessoasProjeto(List<PessoaProjetoBean> listaPessoasProjeto) {
        this.listaPessoasProjeto = listaPessoasProjeto;
    }

    public List<String> getListaTipoParticipacao() {
        return listaTipoParticipacao;
    }

    public void setListaTipoParticipacao(List<String> listaTipoParticipacao) {
        this.listaTipoParticipacao = listaTipoParticipacao;
    }
    public void prepareEditarParticipante(ParticipanteAuxilioBean participanteAuxilio){
        this.setCurrentState(EDITAR_STATE);
        this.participanteAuxilio = participanteAuxilio;
        listaPessoasProjeto = Service.getInstance().buscarPessoasProjeto(this.auxilio.getFkProjeto());
        RequestContext.getCurrentInstance().execute("PF('addParticipante').show();");
    }
    public void prepareAdicionarParticipante() {
        this.setCurrentState(ADICIONAR_STATE);
        listaPessoasProjeto = Service.getInstance().buscarPessoasProjeto(this.auxilio.getFkProjeto());
        participanteAuxilio = new ParticipanteAuxilioBean();
        participanteAuxilio.setDataAssociacao(new Date());
        RequestContext.getCurrentInstance().execute("PF('addParticipante').show();");
    }
    
    public void adicionaParticipante() {
        if(this.getCurrentState().equals(ADICIONAR_STATE)){
            logger.debug(" -----> adicionaParticipante");
            if(this.auxilio.getParticipantesAuxilio()==null) this.auxilio.setParticipantesAuxilio(new ArrayList<ParticipanteAuxilioBean>());
            for(ParticipanteAuxilioBean parti : this.auxilio.getParticipantesAuxilio()){
                if(parti.getFkPessoaProjeto().getId().equals(participanteAuxilio.getFkPessoaProjeto().getId())){
                    FacesContext.getCurrentInstance().addMessage("participanteError", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Associação inválida. Participante já cadastrado." , "Pessoa: " + participanteAuxilio.getFkPessoaProjeto().getFkPessoa().getNome() + " já participa desse auxílio!"));
                    return;
                }
            }
            participanteAuxilio.setFkAuxilio(auxilio);
            this.auxilio.getParticipantesAuxilio().add(participanteAuxilio);
            
            AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Adicionar Participante", "Info Auxilio", "Adicionou participante ao auxílio de id: " + auxilio.getId() + " - Título: " + auxilio.getNomeIdentificacao()
                        + " - Nome do Participante: " + participanteAuxilio.getFkPessoaProjeto().getFkPessoa().getNome() + " - Username: " + participanteAuxilio.getFkPessoaProjeto().getFkPessoa().getUsername(), ConfigConstantes.CONFIG_AUDITORIA_ADD_PARTICIPANTE_AUXILIO);

            hist = new HistoricoAuxilioBean();
            hist.setAcao("Associação de participante");
            hist.setDataAlteracao(new Date());
            hist.setNomeAlinea("N/A");
            hist.setDescricao("Associação de participante. Nome: " + participanteAuxilio.getFkPessoaProjeto().getFkPessoa().getNome() + ". Tipo: " + participanteAuxilio.getTipo());
            hist.setFkAuxilio(auxilio);
            hist.setFkResponsavel(userSessionMB.getLoggedUser());
            Service.getInstance().registrarHistoricoAuxilio(hist);
        }else if(this.getCurrentState().equals(EDITAR_STATE)){
            hist = new HistoricoAuxilioBean();
            hist.setAcao("Edição de participante");
            hist.setDataAlteracao(new Date());
            hist.setNomeAlinea("N/A");
            hist.setDescricao("Edição de participante. Nome: " + participanteAuxilio.getFkPessoaProjeto().getFkPessoa().getNome() + ". Tipo: " + participanteAuxilio.getTipo());
            hist.setFkAuxilio(auxilio);
            hist.setFkResponsavel(userSessionMB.getLoggedUser());
            Service.getInstance().registrarHistoricoAuxilio(hist);
        }

        auxilio = Service.getInstance().atualizarAuxilio(auxilio);
        
        RequestContext.getCurrentInstance().execute("PF('addParticipante').hide();");
    }
    public void excluirParticipante(){
        logger.debug(" -----> excluirParticipante");
        this.auxilio.getParticipantesAuxilio().remove(participanteAuxilio);
        //Service.getInstance().excluirParticipanteAuxilio(participanteAuxilio);
        auxilio = Service.getInstance().atualizarAuxilio(auxilio);
        
        hist = new HistoricoAuxilioBean();
        hist.setAcao("Remoção de participante");
        hist.setDataAlteracao(new Date());
        hist.setNomeAlinea("N/A");
        hist.setDescricao("Remoção de participante. Nome: " + participanteAuxilio.getFkPessoaProjeto().getFkPessoa().getNome() + ". Tipo: " + participanteAuxilio.getTipo());
        hist.setFkAuxilio(auxilio);
        hist.setFkResponsavel(userSessionMB.getLoggedUser());
        Service.getInstance().registrarHistoricoAuxilio(hist);
        
        //auxilio = Service.getInstance().atualizarAuxilio(auxilio);
        RequestContext.getCurrentInstance().execute("PF('deleteParticipantePopup').hide();");
    }

    public SubcentroDespesasBean getSubcentroBolsaAlocada() {
        return subcentroBolsaAlocada;
    }

    public void setSubcentroBolsaAlocada(SubcentroDespesasBean subcentroBolsaAlocada) {
        this.subcentroBolsaAlocada = subcentroBolsaAlocada;
    }

    public List<HistoricoAuxilioBean> getHistoricoSubCentros() {
        return historicoSubCentros;
    }

    public void setHistoricoSubCentros(List<HistoricoAuxilioBean> historicoSubCentros) {
        this.historicoSubCentros = historicoSubCentros;
    }

    public Date getDataInicialPesquisaHistorico() {
        return dataInicialPesquisaHistorico;
    }

    public void setDataInicialPesquisaHistorico(Date dataInicialPesquisaHistorico) {
        this.dataInicialPesquisaHistorico = dataInicialPesquisaHistorico;
    }
    
    public void bindDataInicialPesquisaHistorico(SelectEvent event) {
        dataInicialPesquisaHistorico = (Date) event.getObject();
        pesquisarHistorico();
    }

    public Date getDataFinalPesquisaHistorico() {
        return dataFinalPesquisaHistorico;
    }

    public void setDataFinalPesquisaHistorico(Date dataFinalPesquisaHistorico) {
        this.dataFinalPesquisaHistorico = dataFinalPesquisaHistorico;
    }
    
    public void bindDataFinalPesquisaHistorico(SelectEvent event) {
        dataFinalPesquisaHistorico = (Date) event.getObject();
        pesquisarHistorico();
    }

    public String getAlineaPesquisaHistorico() {
        return alineaPesquisaHistorico;
    }

    public void setAlineaPesquisaHistorico(String alineaPesquisaHistorico) {
        this.alineaPesquisaHistorico = alineaPesquisaHistorico;
    }

    public String getCentroDespesaPesquisaHistorico() {
        return centroDespesaPesquisaHistorico;
    }

    public void setCentroDespesaPesquisaHistorico(String centroDespesaPesquisaHistorico) {
        this.centroDespesaPesquisaHistorico = centroDespesaPesquisaHistorico;
    }

    public int getTipoPesquisaHistorico() {
        return tipoPesquisaHistorico;
    }

    public void setTipoPesquisaHistorico(int tipoPesquisaHistorico) {
        this.tipoPesquisaHistorico = tipoPesquisaHistorico;
    }

    public PessoaBean getPessoaResponsavelPesquisaHistorico() {
        return pessoaResponsavelPesquisaHistorico;
    }

    public void setPessoaResponsavelPesquisaHistorico(PessoaBean pessoaResponsavelPesquisaHistorico) {
        this.pessoaResponsavelPesquisaHistorico = pessoaResponsavelPesquisaHistorico;
    }
    
    public void pesquisarHistorico(){
        this.listaHistoricoAlteracoes = Service.getInstance().buscarHistoricoAlteracoes(0,0,auxilio,dataInicialPesquisaHistorico,dataFinalPesquisaHistorico,alineaPesquisaHistorico, centroDespesaPesquisaHistorico, tipoPesquisaHistorico, pessoaResponsavelPesquisaHistorico, acaoPesquisaHistorico);
    }
    
    public StreamedContent downloadReportHistoricoPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioHistoricoAuxilio(listaHistoricoAlteracoes, ReportService.FORMATO_PDF, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }
    
    public StreamedContent downloadReportHistoricoXls() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioHistoricoAuxilio(listaHistoricoAlteracoes, ReportService.FORMATO_XLS, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }
    
    private List<String> listarFiltrosAtivos(){
        List<String> retorno = new ArrayList<String>();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        if(dataInicialPesquisaHistorico!=null){
            retorno.add("Data inicial: " + sdf.format(dataInicialPesquisaHistorico));
        }
        if(dataFinalPesquisaHistorico!=null){
            retorno.add("Data final: " + sdf.format(dataFinalPesquisaHistorico));
        }
        
        if(alineaPesquisaHistorico!=null && !alineaPesquisaHistorico.isEmpty()) retorno.add("Alínea: " + alineaPesquisaHistorico);
        
        if(centroDespesaPesquisaHistorico!=null && !centroDespesaPesquisaHistorico.isEmpty()) retorno.add("Subcentro: " + centroDespesaPesquisaHistorico);
        
        if(tipoPesquisaHistorico == 1) retorno.add("Operações: C");
        if(tipoPesquisaHistorico == 2) retorno.add("Operações: D");
        
        if(pessoaResponsavelPesquisaHistorico!=null) retorno.add("Responsável: " + pessoaResponsavelPesquisaHistorico.getNome());
        
        return retorno;
    }

    public String getAcaoPesquisaHistorico() {
        return acaoPesquisaHistorico;
    }

    public void setAcaoPesquisaHistorico(String acaoPesquisaHistorico) {
        this.acaoPesquisaHistorico = acaoPesquisaHistorico;
    }

    public List<String> getListaAcoesHistorico() {
        return listaAcoesHistorico;
    }

    public void setListaAcoesHistorico(List<String> listaAcoesHistorico) {
        this.listaAcoesHistorico = listaAcoesHistorico;
    }

    public String getImagemRegistrarRecolhimentoVerba() {
        return imagemRegistrarRecolhimentoVerba;
    }

    public void setImagemRegistrarRecolhimentoVerba(String imagemRegistrarRecolhimentoVerba) {
        this.imagemRegistrarRecolhimentoVerba = imagemRegistrarRecolhimentoVerba;
    }

    public String getImagemRegistrarRecolhimentoVerbaGde() {
        return imagemRegistrarRecolhimentoVerbaGde;
    }

    public void setImagemRegistrarRecolhimentoVerbaGde(String imagemRegistrarRecolhimentoVerbaGde) {
        this.imagemRegistrarRecolhimentoVerbaGde = imagemRegistrarRecolhimentoVerbaGde;
    }
    
    private List<String> listarFiltrosAtivosDespesas(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        List<String> retorno = new ArrayList<String>();
        
        if(pesquisaDespesa!=null && !"".equals(pesquisaDespesa)){
            retorno.add("Palavra-chave: " + pesquisaDespesa);
        }
        if(statusDespesa!=null && !"".equals(statusDespesa)){
            retorno.add("Status: " + statusDespesa);
        }
        if(centroDespesa!=null){
            retorno.add("Centro de despesas: " + centroDespesa.getNome());
        }
        if(auxilio!=null){
            retorno.add("Auxílio: " + auxilio.getNomeIdentificacao());
            retorno.add("\t" + auxilio.getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getSigla() + " " + auxilio.getProtocoloFinanciador());
        }
        if(alineaPesquisa!=null){
            retorno.add("Alínea: " + alineaPesquisa.getFkCategoriaAlinea().getNome());
        }
        if(tipoDespesaPesquisa!=null && !"".equals(tipoDespesaPesquisa)){
            retorno.add("Tipo de despesa: " + tipoDespesaPesquisa);
        }
        if(pesquisaDespesaNumeroDocFiscal!=null && !pesquisaDespesaNumeroDocFiscal.isEmpty()){
            retorno.add("No. doc fiscal: " + pesquisaDespesaNumeroDocFiscal);
        }
        if(dataInicialDocFiscalPesquisaDespesas!=null){
            retorno.add("Dt. Inicial doc fiscal: " + sdf.format(dataInicialDocFiscalPesquisaDespesas));
        }
        if(dataFinalDocFiscalPesquisaDespesas!=null){
            retorno.add("Dt. Final doc fiscal: " + sdf.format(dataFinalDocFiscalPesquisaDespesas));
        }
        if(pesquisaDespesaNumeroCheque!=null && !pesquisaDespesaNumeroCheque.isEmpty()){
            retorno.add("No. doc cheque: " + pesquisaDespesaNumeroCheque);
        }
        if(dataInicialChequePesquisaDespesas!=null){
            retorno.add("Dt. Inicial cheque: " + sdf.format(dataInicialChequePesquisaDespesas));
        }
        if(dataFinalChequePesquisaDespesas!=null){
            retorno.add("Dt. Final cheque: " + sdf.format(dataFinalChequePesquisaDespesas));
        }
        
        return retorno;
    }
    public void atualizarListaSubcentros() {
        //saldoDisponivel = ContabilidadeService.getInstance().saldoSubcentro(alineaBean.getSubcentrosDespesas().get(0));
        saldoDisponivel = new BigDecimal(0);

        if (alineaBean == null) {
            return;
        }
//        if (alineaBean.getMoeda() == 1) {
//            moedaDespesa = "R$";
//        } else {
//            moedaDespesa = "U$";
//        }
        this.listaSubcentrosDespesa = alineaBean.getSubcentrosDespesas();

        if (despesa == null) {
            return;
        }
        this.despesa.setFkSubcentro(null);
    }

    public BigDecimal getValorPesquisaDespesa() {
        return valorPesquisaDespesa;
    }

    public void setValorPesquisaDespesa(BigDecimal valorPesquisaDespesa) {
        this.valorPesquisaDespesa = valorPesquisaDespesa;
    }

    public SubcentroDespesasBean getSubcentroAlineaRecebedora() {
        return subcentroAlineaRecebedora;
    }

    public void setSubcentroAlineaRecebedora(SubcentroDespesasBean subcentroAlineaRecebedora) {
        this.subcentroAlineaRecebedora = subcentroAlineaRecebedora;
    }

    public SubcentroDespesasBean getSubcentroAlineaDoadora() {
        return subcentroAlineaDoadora;
    }

    public void setSubcentroAlineaDoadora(SubcentroDespesasBean subcentroAlineaDoadora) {
        this.subcentroAlineaDoadora = subcentroAlineaDoadora;
    }

    public BigDecimal getSaldoSubcentroRecebedor() {
        return saldoSubcentroRecebedor;
    }

    public void setSaldoSubcentroRecebedor(BigDecimal saldoSubcentroRecebedor) {
        this.saldoSubcentroRecebedor = saldoSubcentroRecebedor;
    }

    public BigDecimal getSaldoSubcentroDoador() {
        return saldoSubcentroDoador;
    }

    public void setSaldoSubcentroDoador(BigDecimal saldoSubcentroDoador) {
        this.saldoSubcentroDoador = saldoSubcentroDoador;
    }
    
    public void selecionarSubcentroDoador(){
        System.out.println("=====> " + subcentroAlineaDoadora);
        saldoSubcentroDoador = saldoSubcentro(subcentroAlineaDoadora);
    }
    
    public void selecionarSubcentroRecebedor(){
        System.out.println("=====> " + subcentroAlineaRecebedora);
        if(alineaRecebedora != null && !alineaRecebedora.getSubcentrosDespesas().isEmpty()){
            subcentroAlineaRecebedora = alineaRecebedora.getSubcentrosDespesas().get(0);
        }
        if(subcentroAlineaRecebedora==null){
            saldoSubcentroRecebedor = new BigDecimal(0);
        }else{
            saldoSubcentroRecebedor = saldoSubcentro(subcentroAlineaRecebedora);
        }
    }
    
    public void prepareExcluirAlinea(AlineaBean alinea){
        this.alineaBean = alinea;
        if(alinea.getDespesas().isEmpty()){
            RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
        }else{
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir o registro", "A alínea selecionada possui despesas vinculadas.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            RequestContext.getCurrentInstance().execute("PF('error').show();");
        }
    }

    public Date getDataInicialChequePesquisaDespesas() {
        return dataInicialChequePesquisaDespesas;
    }

    public void setDataInicialChequePesquisaDespesas(Date dataInicialChequePesquisaDespesas) {
        this.dataInicialChequePesquisaDespesas = dataInicialChequePesquisaDespesas;
    }

    public Date getDataFinalChequePesquisaDespesas() {
        return dataFinalChequePesquisaDespesas;
    }

    public void setDataFinalChequePesquisaDespesas(Date dataFinalChequePesquisaDespesas) {
        this.dataFinalChequePesquisaDespesas = dataFinalChequePesquisaDespesas;
    }
    
    public void bindDataInicialChequeBuscaDespesas(SelectEvent event) {
        dataInicialChequePesquisaDespesas = (Date) event.getObject();
        pesquisarDespesas(false);
    }
    public void bindDataFinalChequeBuscaDespesas(SelectEvent event) {
        dataFinalChequePesquisaDespesas = (Date) event.getObject();
        pesquisarDespesas(false);
    }

    public String getPesquisaDespesaNumeroDocFiscal() {
        return pesquisaDespesaNumeroDocFiscal;
    }

    public void setPesquisaDespesaNumeroDocFiscal(String pesquisaDespesaNumeroDocFiscal) {
        this.pesquisaDespesaNumeroDocFiscal = pesquisaDespesaNumeroDocFiscal;
    }

    public String getPesquisaDespesaNumeroCheque() {
        return pesquisaDespesaNumeroCheque;
    }

    public void setPesquisaDespesaNumeroCheque(String pesquisaDespesaNumeroCheque) {
        this.pesquisaDespesaNumeroCheque = pesquisaDespesaNumeroCheque;
    }
    
    public StreamedContent downloadReportChequesPdf() {
        logger.debug("GERAR RELATORIO CHEQUES PDF");
        List<DocumentoComprovacaoPagamentoBean> listaCheques = new ArrayList<DocumentoComprovacaoPagamentoBean>();
        for(DespesaBean despTmp : listaDespesas){
            listaCheques.addAll(despTmp.getDocumentosComprovacaoPagamentos());
        }
        Collections.sort(listaCheques,DocumentoComprovacaoPagamentoBean.POR_DATA);
        List<String> filtros = listarFiltrosAtivosDespesas();
        filtros.add(0, "Outorgado: " + auxilio.getFkResponsavel().getNome());
        InputStream stream = ReportService.getInstance().emiteRelatorioCheques(listaCheques, ReportService.FORMATO_PDF, filtros);
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportChequesXls() {
        logger.debug("GERAR RELATORIO CHEQUES XLS");
        List<DocumentoComprovacaoPagamentoBean> listaCheques = new ArrayList<DocumentoComprovacaoPagamentoBean>();
        for(DespesaBean despTmp : listaDespesas){
            listaCheques.addAll(despTmp.getDocumentosComprovacaoPagamentos());
        }
        Collections.sort(listaCheques,DocumentoComprovacaoPagamentoBean.POR_DATA);
        List<String> filtros = listarFiltrosAtivosDespesas();
        filtros.add(0, "Outorgado: " + auxilio.getFkResponsavel().getNome());
        InputStream stream = ReportService.getInstance().emiteRelatorioCheques(listaCheques, ReportService.FORMATO_XLS, filtros);
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }
}
