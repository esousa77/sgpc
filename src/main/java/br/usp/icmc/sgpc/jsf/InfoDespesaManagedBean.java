/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.ArquivoDocDespesaBean;
import br.usp.icmc.sgpc.beans.ArquivoDocPagamentoBean;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.BemPatrimoniadoBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoDespesaBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoPagamentoBean;
import br.usp.icmc.sgpc.beans.NotaFiscalBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.beans.ReciboBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author herick
 */
@ManagedBean(name = "infoDespesaMB")
@ViewScoped
public class InfoDespesaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(InfoDespesaManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private DespesaBean despesa;
    private String imagemDocumentoDespesa = "/images/icons/add1.png";
    private String imagemDocumentoPagamento = "/images/icons/add1.png";
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private List<String> listaTiposDocDespesa = new ArrayList<String>();
    private DocumentoComprovacaoDespesaBean documentoDespesa;
    private DocumentoComprovacaoPagamentoBean documentoPagamento;
    private BemPatrimoniadoBean bemPatrimoniado;
    private String tipoDocDespesa;
    private String danfe;
    private String tipoNF;
    private boolean exibeCamposNF = false;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private boolean exibeBemPatrimoniado = false;
    private boolean podeCadastrarPagamento = false;
    private ArquivoDocDespesaBean arquivoDespesa;
    private ArquivoDocPagamentoBean arquivoPagamento;
    private String imagemExcluirArquivo = "/images/icons/del.png";

    @PostConstruct
    public void postConstruct() {
        this.despesa = (DespesaBean) userSessionMB.getInterPageParameter();
        if (despesa.getTipoDespesa().equals("capital")) {
            exibeBemPatrimoniado = true;
        }
    }

    public ArquivoDocDespesaBean getArquivoDespesa() {
        return arquivoDespesa;
    }

    public void setArquivoDespesa(ArquivoDocDespesaBean arquivoDespesa) {
        this.arquivoDespesa = arquivoDespesa;
    }

    public ArquivoDocPagamentoBean getArquivoPagamento() {
        return arquivoPagamento;
    }

    public void setArquivoPagamento(ArquivoDocPagamentoBean arquivoPagamento) {
        this.arquivoPagamento = arquivoPagamento;
    }

    public String getImagemExcluirArquivo() {
        return imagemExcluirArquivo;
    }

    public void setImagemExcluirArquivo(String imagemExcluirArquivo) {
        this.imagemExcluirArquivo = imagemExcluirArquivo;
    }

    public boolean isPodeCadastrarPagamento() {
        if (despesa.getDocumentosComprovacaoDespesas().size() > 0) {
            podeCadastrarPagamento = true;
        } else {
            podeCadastrarPagamento = false;
        }
        return podeCadastrarPagamento;
    }

    public void setPodeCadastrarPagamento(boolean podeCadastrarPagamento) {
        this.podeCadastrarPagamento = podeCadastrarPagamento;
    }

    public DespesaBean getDespesa() {
        return despesa;
    }

    public void setDespesa(DespesaBean despesa) {
        this.despesa = despesa;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getImagemDocumentoDespesa() {
        return imagemDocumentoDespesa;
    }

    public void setImagemDocumentoDespesa(String imagemDocumentoDespesa) {
        this.imagemDocumentoDespesa = imagemDocumentoDespesa;
    }

    public String getImagemDocumentoPagamento() {
        return imagemDocumentoPagamento;
    }

    public void setImagemDocumentoPagamento(String imagemDocumentoPagamento) {
        this.imagemDocumentoPagamento = imagemDocumentoPagamento;
    }

    public List<String> getListaTiposDocDespesa() {
        listaTiposDocDespesa.clear();
        listaTiposDocDespesa.add("Nota Fiscal");
        listaTiposDocDespesa.add("Recibo");
        return listaTiposDocDespesa;
    }

    public void setListaTiposDocDespesa(List<String> listaTiposDocDespesa) {
        this.listaTiposDocDespesa = listaTiposDocDespesa;
    }

    public boolean isExibeBemPatrimoniado() {
        return exibeBemPatrimoniado;
    }

    public void setExibeBemPatrimoniado(boolean exibeBemPatrimoniado) {
        this.exibeBemPatrimoniado = exibeBemPatrimoniado;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public DocumentoComprovacaoDespesaBean getDocumentoDespesa() {
        return documentoDespesa;
    }

    public void setDocumentoDespesa(DocumentoComprovacaoDespesaBean documentoDespesa) {
        this.documentoDespesa = documentoDespesa;
    }

    public DocumentoComprovacaoPagamentoBean getDocumentoPagamento() {
        return documentoPagamento;
    }

    public void setDocumentoPagamento(DocumentoComprovacaoPagamentoBean documentoPagamento) {
        this.documentoPagamento = documentoPagamento;
    }

    public String getTipoDocDespesa() {
        return tipoDocDespesa;
    }

    public void setTipoDocDespesa(String tipoDocDespesa) {
        this.tipoDocDespesa = tipoDocDespesa;
    }

    public boolean isExibeCamposNF() {
        return exibeCamposNF;
    }

    public void setExibeCamposNF(boolean exibeCamposNF) {
        this.exibeCamposNF = exibeCamposNF;
    }

    public String getDanfe() {
        return danfe;
    }

    public void setDanfe(String danfe) {
        this.danfe = danfe;
    }

    public String getTipoNF() {
        return tipoNF;
    }

    public void setTipoNF(String tipoNF) {
        this.tipoNF = tipoNF;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public BemPatrimoniadoBean getBemPatrimoniado() {
        return bemPatrimoniado;
    }

    public void setBemPatrimoniado(BemPatrimoniadoBean bemPatrimoniado) {
        this.bemPatrimoniado = bemPatrimoniado;
    }

    public void prepareCadastrarDocDespesa() {
        logger.debug("--InfoDespesa ----prepareDocDespesa--");
        if (tipoDocDespesa.equals("Nota Fiscal")) {
            this.documentoDespesa = new NotaFiscalBean();
            exibeCamposNF = true;
            logger.debug("--InfoDespesa ----nfnffffffff--");
        }
        if (tipoDocDespesa.equals("Recibo")) {
            this.documentoDespesa = new ReciboBean();
            logger.debug("--InfoDespesa ----reciboooooo--");
            exibeCamposNF = false;
        }
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('selectTipoDocDespesa').hide();");
        RequestContext.getCurrentInstance().execute("PF('editDocDespesas').show();");
    }

    public void validateValor(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        double value = Double.valueOf(newValue.toString());
        if (value <= 0) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage("Valor não pode ser zero (0).");
            facesContext.addMessage(component.getClientId(facesContext), message);
        }
    }

    public void handleFileUploadDocDespesa(FileUploadEvent event) {

        arquivoDespesa = new ArquivoDocDespesaBean();
        arquivoDespesa.setNome(event.getFile().getFileName());
        arquivoDespesa.setConteudo(event.getFile().getContents());

        //documentoDespesa.setScanDocumento(event.getFile().getContents());
        documentoDespesa.getArquivosDespesa().add(arquivoDespesa);
        FacesMessage msg = new FacesMessage("Succesful ", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage("uploadDeArquivo", msg);
    }

    public void adicionarDocDespesa() {
        if (documentoDespesa.getTipoDocumento() == 0) {
            if (ADICIONAR_STATE.equals(this.currentState)) {
                logger.debug("- Add despesa");
                NotaFiscalBean nf = (NotaFiscalBean) documentoDespesa;
                nf.setDanfe(danfe);
                nf.setTipo(tipoNF);
                nf.setFkDespesa(despesa);
                //Service.getInstance().cadastraDocumentoComprovacaoDespesa(documentoDespesa);
                Service.getInstance().atualizarDocumentoComprovacaoDespesa(documentoDespesa);
                Service.getInstance().atualizarDespesa(despesa);

            } else if (EDITAR_STATE.equals(this.currentState)) {
                logger.debug("- Edit despesa");
                NotaFiscalBean nf = (NotaFiscalBean) documentoDespesa;
                nf.setDanfe(danfe);
                nf.setTipo(tipoNF);
                Service.getInstance().atualizarDocumentoComprovacaoDespesa(documentoDespesa);
            }
        }
        if (documentoDespesa.getTipoDocumento() == 1) {
            if (ADICIONAR_STATE.equals(this.currentState)) {
                logger.debug("- Add despesa");
                ReciboBean recibo = (ReciboBean) documentoDespesa;
                recibo.setFkDespesa(despesa);
                Service.getInstance().cadastraDocumentoComprovacaoDespesa(documentoDespesa);

            } else if (EDITAR_STATE.equals(this.currentState)) {
                logger.debug("- Edit despesa");
                Service.getInstance().atualizarDocumentoComprovacaoDespesa(documentoDespesa);
            }
        }
        exibeCamposNF = false;
        atualizaDespesa();
        prepareCadastrarDocPagamento();
        RequestContext.getCurrentInstance().execute("PF('editDocDespesas').hide();");
        RequestContext.getCurrentInstance().execute("PF('editDocPagamentos').show();");
    }

    public void prepareEditarDocDespesa() {
        logger.debug("--InfoDespesa ----prepareDocDespesa--");

        if (this.documentoDespesa.getTipoDocumento() == 0) {
            NotaFiscalBean nf = (NotaFiscalBean) this.documentoDespesa;
            danfe = nf.getDanfe();
            tipoNF = nf.getTipo();
            exibeCamposNF = true;
        }
        if (this.documentoDespesa.getTipoDocumento() == 1) {
            ReciboBean r = (ReciboBean) this.documentoDespesa;
            exibeCamposNF = false;
        }
        this.setCurrentState(EDITAR_STATE);
    }

    public void prepareEditarDocPagamento() {
        logger.debug("--InfoDespesa ----prepareDocDespesa--");
        this.setCurrentState(EDITAR_STATE);
    }

    public void prepareCadastrarDocPagamento() {
        logger.debug("--InfoDespesa ----prepareDocDespesa--");
        this.documentoPagamento = new DocumentoComprovacaoPagamentoBean();
        this.setCurrentState(ADICIONAR_STATE);
    }

    public void handleFileUploadDocPagamento(FileUploadEvent event) {
        arquivoPagamento = new ArquivoDocPagamentoBean();
        arquivoPagamento.setNome(event.getFile().getFileName());
        arquivoPagamento.setConteudo(event.getFile().getContents());

        //documentoPagamento.setScanDocumento(event.getFile().getContents());
        documentoPagamento.getArquivosPagamento().add(arquivoPagamento);
        FacesMessage msg = new FacesMessage("Succesful ", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage("uploadDeArquivo", msg);
    }

    public void adicionarDocPagamento() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("- Add pagamento");

            this.documentoPagamento.setFkDespesa(despesa);
            Service.getInstance().cadastraDocumentoComprovacaoPagamento(documentoPagamento);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("- Edit pagamento");
            Service.getInstance().atualizarDocumentoComprovacaoPagamento(documentoPagamento);
        }
        atualizaDespesa();
        RequestContext.getCurrentInstance().execute("PF('editDocPagamentos').hide();");
    }

    public StreamedContent downloadArquivoDespesa(ArquivoDocDespesaBean desp) {
        logger.debug("GERAR RELATORIO PDF");
        ByteArrayInputStream stream = new ByteArrayInputStream(desp.getConteudo());
        StreamedContent file = new DefaultStreamedContent(stream, "image/jpg", "document.jpg");
        return file;
    }

    public StreamedContent downloadArquivoPagamento(ArquivoDocPagamentoBean desp) {
        logger.debug("GERAR RELATORIO PDF");
        ByteArrayInputStream stream = new ByteArrayInputStream(desp.getConteudo());
        StreamedContent file = new DefaultStreamedContent(stream, "image/jpg", "document.jpg");
        return file;
    }

    public void atualizaDespesa() {
        this.despesa = Service.getInstance().buscarDespesa(this.despesa.getId());
        userSessionMB.setInterPageParameter(despesa);
    }

    public String returnInfoAuxilio() {
        atualizaAuxilio();
        return "infoAuxilio?faces-redirect=true";
    }

    public void atualizaAuxilio() {
        AuxilioBean auxilio = Service.getInstance().buscarAuxilios(userSessionMB.getAuxilio().getId());
        ProjetoBean projeto = auxilio.getFkProjeto();
        userSessionMB.setProjeto(projeto);
        userSessionMB.setAuxilio(auxilio);
    }

    public boolean podeFazerDownloadDesp(DocumentoComprovacaoDespesaBean desp) {
        if (!desp.getArquivosDespesa().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean podeFazerDownloadPag(DocumentoComprovacaoPagamentoBean pag) {
        if (!pag.getArquivosPagamento().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public void prepareEditarBemPatrimoniado() {
        this.setCurrentState(EDITAR_STATE);
    }

    public void prepareCadastrarBemPatrimoniado() {
        this.bemPatrimoniado = new BemPatrimoniadoBean();
        this.setCurrentState(ADICIONAR_STATE);
    }

    public void adicionarBemPatrimoniado() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            this.bemPatrimoniado.setFkDespesa(despesa);
            Service.getInstance().cadastraBemPatrimoniado(bemPatrimoniado);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            Service.getInstance().atualizarBemPatrimoniado(bemPatrimoniado);
        }
        this.bemPatrimoniado = new BemPatrimoniadoBean();
        atualizaDespesa();
        RequestContext.getCurrentInstance().execute("PF('addBemPatrimoniado').hide();");
    }

    public void excluirBemPatrimoniado() {
        logger.debug("EXCLUIR REGISTRO");
        despesa.getBensPatrimoniados().remove(bemPatrimoniado);
        Service.getInstance().excluirBemPatrimoniado(bemPatrimoniado);
        atualizaDespesa();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void excluirArquivoPagamento() {
        documentoPagamento.getArquivosPagamento().remove(arquivoPagamento);
        Service.getInstance().atualizarDocumentoComprovacaoPagamento(documentoPagamento);
        atualizaDespesa();
        RequestContext.getCurrentInstance().execute("PF('deletePopupDocPagamento').hide();");
    }

    public void excluirArquivoDespesa() {
        documentoDespesa.getArquivosDespesa().remove(arquivoDespesa);
        Service.getInstance().atualizarDocumentoComprovacaoDespesa(documentoDespesa);
        atualizaDespesa();
        RequestContext.getCurrentInstance().execute("PF('deletePopupDocDespesa').hide();");
    }
}
