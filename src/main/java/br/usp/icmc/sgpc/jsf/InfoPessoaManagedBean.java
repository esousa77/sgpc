/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.ContaCorrenteBean;
import br.usp.icmc.sgpc.beans.InstituicaoBancariaBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "infoPessoaMB")
@ViewScoped
public class InfoPessoaManagedBean implements Serializable {
    private static final Logger logger = Logger.getLogger(InfoPessoaManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private PessoaBean pessoa;
    private ContaCorrenteBean contaCorrente;
    private List<InstituicaoBancariaBean> listaInstituicaoBancaria = new ArrayList<InstituicaoBancariaBean>();
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private List<ContaCorrenteBean> listaContasCorrente = new ArrayList<ContaCorrenteBean>();

    @PostConstruct
    public void postConstruct() {
        this.pessoa = (PessoaBean) userSessionMB.getInterPageParameter();
        this.listaInstituicaoBancaria = Service.getInstance().listarInstituicoesBancarias();
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public PessoaBean getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaBean pessoa) {
        this.pessoa = pessoa;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public ContaCorrenteBean getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrenteBean contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public List<InstituicaoBancariaBean> getListaInstituicaoBancaria() {
        return listaInstituicaoBancaria;
    }

    public void setListaInstituicaoBancaria(List<InstituicaoBancariaBean> listaInstituicaoBancaria) {
        this.listaInstituicaoBancaria = listaInstituicaoBancaria;
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            this.contaCorrente.setFkPessoa(this.pessoa);
            this.pessoa.getContasCorrentes().add(this.contaCorrente);
            Service.getInstance().atualizarPessoa(this.pessoa);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarContaCorrente(contaCorrente);
        }
    }

    public boolean validarDatas(FacesContext facesContext, UIComponent uiComponent, Object newValue) {
        Date minhaData = (Date) newValue;
        if (1 == 2) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "Data Inválida"));
        }
        return true;
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        this.pessoa.getContasCorrentes().remove(this.contaCorrente);
        Service.getInstance().atualizarPessoa(this.pessoa);
        //Service.getInstance().excluirContaCorrente(contaCorrente);
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    private void clear() {
        this.contaCorrente = new ContaCorrenteBean();
    }

    public List<ContaCorrenteBean> getListaContasCorrente() {
        atualizaContasCorrentes();
        return listaContasCorrente;
    }

    public void setListaContasCorrente(List<ContaCorrenteBean> listaContasCorrente) {
        this.listaContasCorrente = listaContasCorrente;
    }
    
    public void atualizaContasCorrentes(){
        listaContasCorrente.clear();
        if(pessoa!=null){
            this.pessoa = Service.getInstance().buscarPessoa(pessoa.getId());
            listaContasCorrente = pessoa.getContasCorrentes();
        }
    }

}
