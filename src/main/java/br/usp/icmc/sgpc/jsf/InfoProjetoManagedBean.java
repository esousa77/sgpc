/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.ArquivoAnexoBean;
import br.usp.icmc.sgpc.beans.AtividadeBean;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.CentroDespesaBean;
import br.usp.icmc.sgpc.beans.ContaCorrenteBean;
import br.usp.icmc.sgpc.beans.CotacaoBean;
import br.usp.icmc.sgpc.beans.EventoBean;
import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.InstituicaoBancariaBean;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean;
import br.usp.icmc.sgpc.beans.ModalidadeBean;
import br.usp.icmc.sgpc.beans.PesquisaBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaProjetoBean;
import br.usp.icmc.sgpc.beans.ProgramaEspecialFinanciamentoBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.AuditoriaService;
import br.usp.icmc.sgpc.service.ContabilidadeService;
import br.usp.icmc.sgpc.service.MailService;
import br.usp.icmc.sgpc.service.Service;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author artur
 */
@ManagedBean(name = "infoProjetoMB")
@ViewScoped
public class InfoProjetoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(InfoProjetoManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private ProjetoBean projeto;
    private AuxilioBean auxilio = new AuxilioBean();
    private List<String> listaStatusAuxilio = new ArrayList<String>();
    private List<String> listaStatusProjeto = new ArrayList<String>();
    private List<FinanciadorBean> listaFinanciador = new ArrayList<FinanciadorBean>();
    private List<ContaCorrenteBean> listaContaCorrente = new ArrayList<ContaCorrenteBean>();
    private List<CentroDespesaBean> listaCentroDespesa = new ArrayList<CentroDespesaBean>();
    private ProjetoBean projetoRecebedor;
    private List<ProjetoBean> listaProjetos = new ArrayList<ProjetoBean>();
    private String statusProjeto;
    private boolean podeAlterarStatus = false;
    private boolean alterarStatusAuxilio = false;
    private boolean podeCriarAuxilio = false;
    private boolean podeEditarAuxilio = false;
    private boolean podeRemanejarAuxilio = false;
    private boolean podeExcluirAuxilio = false;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemEditarCotacao = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private String imagemRemanejarAuxilio = "/images/icons/remanejar.png";
    private String imagemExcluirParticipante = "/images/icons/delbw.png";
    private String imagemExcluirArquivo = "/images/icons/del.png";
    private String imagemArquivoAnexo = "/images/icons/attach.png";
    private String pesquisa;
    private List<PessoaBean> listaPessoas = new ArrayList<PessoaBean>();
    private PessoaProjetoBean participanteProjeto;
    private boolean podeAdicionarParticipante = false;
    private FinanciadorBean financiador;
    private TipoAuxilioBean tipoAuxilioBean;
    private ModalidadeBean modalidade;
    private List<TipoAuxilioBean> listaTiposAuxilio = new ArrayList<TipoAuxilioBean>();
    private List<ModalidadeBean> listaModalidades = new ArrayList<ModalidadeBean>();
    private boolean podeAlterarStatusAuxilioFinanceiro = false;
    private List<AtividadeBean> listaAtividade = new ArrayList<AtividadeBean>();
    private AtividadeBean atividade;
    private String imagemInscricao = "/images/icons/add_user.png";
    private String imagemExcluirInscricao = "/images/icons/del.png";
    private PessoaBean[] pessoasAtividade;
    private EventoBean evento;
    private boolean exibeAtividade = false;
    private String statusAntigo;
    private List<String> listaTipoParticipacao = new ArrayList<String>();
    private List<String> listaTipoArquivo = new ArrayList<String>();
    private String tipoArquivoUpload;
    ArquivoAnexoBean arquivoAnexo;
    private PessoaBean participanteAtividade;
    private List<ProgramaEspecialFinanciamentoBean> listaProgramasEspeciaisFinanciamento = new ArrayList<ProgramaEspecialFinanciamentoBean>();
    private CotacaoBean cotacao;
    private List<String> listaStatusCotacao = new ArrayList<String>();
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private ContaCorrenteBean contaCorrente;
    private List<InstituicaoBancariaBean> listaInstituicaoBancaria = new ArrayList<InstituicaoBancariaBean>();
    private List<PessoaBean> listaPessoasBolsistas = new ArrayList<PessoaBean>();
    private Boolean indicarBolsista = false;
    private List<PessoaProjetoBean> listapessoasProjeto = new ArrayList<PessoaProjetoBean>();

    public ContaCorrenteBean getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrenteBean contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public List<InstituicaoBancariaBean> getListaInstituicaoBancaria() {
        return listaInstituicaoBancaria;
    }

    public void setListaInstituicaoBancaria(List<InstituicaoBancariaBean> listaInstituicaoBancaria) {
        this.listaInstituicaoBancaria = listaInstituicaoBancaria;
    }

    public CotacaoBean getCotacao() {
        return cotacao;
    }

    public void setCotacao(CotacaoBean cotacao) {
        this.cotacao = cotacao;
    }

    public List<ProgramaEspecialFinanciamentoBean> getListaProgramasEspeciaisFinanciamento() {
        return listaProgramasEspeciaisFinanciamento;
    }

    public void setListaProgramasEspeciaisFinanciamento(List<ProgramaEspecialFinanciamentoBean> listaProgramasEspeciaisFinanciamento) {
        this.listaProgramasEspeciaisFinanciamento = listaProgramasEspeciaisFinanciamento;
    }

    public PessoaBean getParticipanteAtividade() {
        return participanteAtividade;
    }

    public void setParticipanteAtividade(PessoaBean participanteAtividade) {
        this.participanteAtividade = participanteAtividade;
    }

    public String getImagemEditarCotacao() {
        return imagemEditarCotacao;
    }

    public void setImagemEditarCotacao(String imagemEditarCotacao) {
        this.imagemEditarCotacao = imagemEditarCotacao;
    }

    public ArquivoAnexoBean getArquivoAnexo() {
        return arquivoAnexo;
    }

    public void setArquivoAnexo(ArquivoAnexoBean arquivoAnexo) {
        this.arquivoAnexo = arquivoAnexo;
    }

    public String getTipoArquivoUpload() {
        return tipoArquivoUpload;
    }

    public void setTipoArquivoUpload(String tipoArquivoUpload) {
        this.tipoArquivoUpload = tipoArquivoUpload;
    }

    public List<String> getListaTipoArquivo() {
        this.listaTipoArquivo.clear();
        this.listaTipoArquivo.add("Despacho");
        this.listaTipoArquivo.add("Parecer");
        this.listaTipoArquivo.add("Projeto Original");
        this.listaTipoArquivo.add("Termo Aditivo");
        this.listaTipoArquivo.add("Termo de Outorga");
        this.listaTipoArquivo.add("Outros Documentos");

        return listaTipoArquivo;
    }

    public void setListaTipoArquivo(List<String> listaTipoArquivo) {
        this.listaTipoArquivo = listaTipoArquivo;
    }

    public List<String> getListaTipoParticipacao() {
        this.listaTipoParticipacao.clear();
        this.listaTipoParticipacao.add("Bolsista");
        this.listaTipoParticipacao.add("Colaborador");
        this.listaTipoParticipacao.add("Pesquisador Principal");

        return listaTipoParticipacao;
    }

    public void setListaTipoParticipacao(List<String> listaTipoParticipacao) {
        this.listaTipoParticipacao = listaTipoParticipacao;
    }

    public AtividadeBean getAtividade() {
        return atividade;
    }

    public void setAtividade(AtividadeBean atividade) {
        this.atividade = atividade;
    }

    public EventoBean getEvento() {
        return evento;
    }

    public void setEvento(EventoBean evento) {
        this.evento = evento;
    }

    public boolean isExibeAtividade() {
        return exibeAtividade;
    }

    public void setExibeAtividade(boolean exibeAtividade) {
        this.exibeAtividade = exibeAtividade;
    }

    public String getImagemExcluirInscricao() {
        return imagemExcluirInscricao;
    }

    public void setImagemExcluirInscricao(String imagemExcluirInscricao) {
        this.imagemExcluirInscricao = imagemExcluirInscricao;
    }

    public String getImagemArquivoAnexo() {
        return imagemArquivoAnexo;
    }

    public void setImagemArquivoAnexo(String imagemArquivoAnexo) {
        this.imagemArquivoAnexo = imagemArquivoAnexo;
    }

    public String getImagemExcluirArquivo() {
        return imagemExcluirArquivo;
    }

    public void setImagemExcluirArquivo(String imagemExcluirArquivo) {
        this.imagemExcluirArquivo = imagemExcluirArquivo;
    }

    public String getImagemInscricao() {
        return imagemInscricao;
    }

    public void setImagemInscricao(String imagemInscricao) {
        this.imagemInscricao = imagemInscricao;
    }

    public List<AtividadeBean> getListaAtividade() {
        return listaAtividade;
    }

    public void setListaAtividade(List<AtividadeBean> listaAtividade) {
        this.listaAtividade = listaAtividade;
    }

    public PessoaBean[] getPessoasAtividade() {
        return pessoasAtividade;
    }

    public void setPessoasAtividade(PessoaBean[] pessoasAtividade) {
        this.pessoasAtividade = pessoasAtividade;
    }

    public boolean isPodeAlterarStatusAuxilioFinanceiro() {
        return podeAlterarStatusAuxilioFinanceiro;
    }

    public void setPodeAlterarStatusAuxilioFinanceiro(boolean podeAlterarStatusAuxilioFinanceiro) {
        this.podeAlterarStatusAuxilioFinanceiro = podeAlterarStatusAuxilioFinanceiro;
    }

    public List<TipoAuxilioBean> getListaTiposAuxilio() {
        return listaTiposAuxilio;
    }

    public void setListaTiposAuxilio(List<TipoAuxilioBean> listaTiposAuxilio) {
        this.listaTiposAuxilio = listaTiposAuxilio;
    }

    public List<ModalidadeBean> getListaModalidades() {
        return listaModalidades;
    }

    public void setListaModalidades(List<ModalidadeBean> listaModalidades) {
        this.listaModalidades = listaModalidades;
    }

    public FinanciadorBean getFinanciador() {
        return financiador;
    }

    public void setFinanciador(FinanciadorBean financiador) {
        this.financiador = financiador;
    }

    public TipoAuxilioBean getTipoAuxilioBean() {
        return tipoAuxilioBean;
    }

    public void setTipoAuxilioBean(TipoAuxilioBean tipoAuxilioBean) {
        this.tipoAuxilioBean = tipoAuxilioBean;
    }

    public ModalidadeBean getModalidade() {
        return modalidade;
    }

    public void setModalidade(ModalidadeBean modalidade) {
        this.modalidade = modalidade;
    }

    public PessoaProjetoBean getParticipanteProjeto() {
        return participanteProjeto;
    }

    public void setParticipanteProjeto(PessoaProjetoBean participanteProjeto) {
        this.participanteProjeto = participanteProjeto;
    }

    public String getImagemExcluirParticipante() {
        return imagemExcluirParticipante;
    }

    public void setImagemExcluirParticipante(String imagemExcluirParticipante) {
        this.imagemExcluirParticipante = imagemExcluirParticipante;
    }

    public boolean isPodeAdicionarParticipante() {
        if (this.projeto.getStatus().equals("Finalizado") || this.projeto.getStatus().equals("Cancelado") || this.projeto.getStatus().equals("Abortado")) {
            podeAdicionarParticipante = false;
        } else {
            podeAdicionarParticipante = true;
        }
        return podeAdicionarParticipante;
    }

    public void setPodeAdicionarParticipante(boolean podeAdicionarParticipante) {
        this.podeAdicionarParticipante = podeAdicionarParticipante;
    }

    public boolean podeRemoverParticipante(PessoaProjetoBean participante) {
        if (projeto.getFkResponsavel() == participante.getFkPessoa()) {
            imagemExcluirParticipante = "/images/icons/delbw.png";
            return false;
        } else {
            if (this.projeto.getStatus().equals("Finalizado") || this.projeto.getStatus().equals("Cancelado") || this.projeto.getStatus().equals("Abortado")) {
                imagemExcluirParticipante = "/images/icons/delbw.png";
                return false;
            } else {
                imagemExcluirParticipante = "/images/icons/del.png";
                return true;
            }
        }
    }

    public List<PessoaBean> getListaPessoas() {
        pesquisar();
        return listaPessoas;
    }

    public void setListaPessoas(List<PessoaBean> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public InfoProjetoManagedBean() {
        logger.debug("INSTANCIADO Info PROJETO");

    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public String getImagemRemanejarAuxilio() {
        return imagemRemanejarAuxilio;
    }

    public void setImagemRemanejarAuxilio(String imagemRemanejarAuxilio) {
        this.imagemRemanejarAuxilio = imagemRemanejarAuxilio;
    }

    public boolean isPodeRemanejarAuxilio() {
        return podeRemanejarAuxilio;
    }

    public void setPodeRemanejarAuxilio(boolean podeRemanejarAuxilio) {
        this.podeRemanejarAuxilio = podeRemanejarAuxilio;
    }

    public boolean isPodeEditarAuxilio() {
        return podeEditarAuxilio;
    }

    public void setPodeEditarAuxilio(boolean podeEditarAuxilio) {
        this.podeEditarAuxilio = podeEditarAuxilio;
    }

    public boolean isPodeCriarAuxilio() {
        return podeCriarAuxilio;
    }

    public void setPodeCriarAuxilio(boolean podeCriarAuxilio) {
        this.podeCriarAuxilio = podeCriarAuxilio;
    }

    public boolean isAlterarStatusAuxilio() {
        return alterarStatusAuxilio;
    }

    public void setAlterarStatusAuxilio(boolean alterarStatusAuxilio) {
        this.alterarStatusAuxilio = alterarStatusAuxilio;
    }

    public boolean isPodeAlterarStatus() {
        return podeAlterarStatus;
    }

    public void setAlteraStatus(boolean podeAlterarStatus) {
        this.podeAlterarStatus = podeAlterarStatus;
    }

    public String getStatusProjeto() {
        return statusProjeto;
    }

    public void setStatusProjeto(String statusProjeto) {
        this.statusProjeto = statusProjeto;
    }

    public void setListaStatusProjeto(List<String> listaStatusProjeto) {
        this.listaStatusProjeto = listaStatusProjeto;
    }

    public List<String> getListaStatusProjeto() {
        listaStatusProjeto.clear();
        listaStatusProjeto.add("Em Planejamento");
        listaStatusProjeto.add("Em Andamento");
        listaStatusProjeto.add("Finalizado");
        listaStatusProjeto.add("Cancelado");
        listaStatusProjeto.add("Abortado");
        listaStatusProjeto.add("Desistência");
        listaStatusProjeto.add("Atrasado");

        return listaStatusProjeto;
    }

    public List<ProjetoBean> getListaProjetos() {
        return listaProjetos;
    }

    public void setListaProjetos(List<ProjetoBean> listaProjetos) {
        this.listaProjetos = listaProjetos;
    }

    public ProjetoBean getProjetoRecebedor() {
        return projetoRecebedor;
    }

    public void setProjetoRecebedor(ProjetoBean projetoRecebedor) {
        this.projetoRecebedor = projetoRecebedor;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public ProjetoBean getProjeto() {
        return projeto;
    }

    public void setProjeto(ProjetoBean projeto) {
        this.projeto = projeto;
    }

    public AuxilioBean getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(AuxilioBean auxilio) {
        this.auxilio = auxilio;
    }

    public List<FinanciadorBean> getListaFinanciador() {
        return listaFinanciador;
    }

    public void setListaFinanciador(List<FinanciadorBean> listaFinanciador) {
        this.listaFinanciador = listaFinanciador;
    }

    public List<CentroDespesaBean> getListaCentroDespesa() {
        return listaCentroDespesa;
    }

    public void setListaCentroDespesa(List<CentroDespesaBean> listaCentroDespesa) {
        this.listaCentroDespesa = listaCentroDespesa;
    }

    public List<ContaCorrenteBean> getListaContaCorrente() {
        return listaContaCorrente;
    }

    public void setListaContaCorrente(List<ContaCorrenteBean> listaContaCorrente) {
        this.listaContaCorrente = listaContaCorrente;
    }

    public List<String> getListaStatusAuxilio() {
        listaStatusAuxilio.clear();
        if (ADICIONAR_STATE.equals(currentState)) {
            listaStatusAuxilio.add("Em Elaboração");
        } else if (podeAlterarStatusAuxilioFinanceiro) {
            listaStatusAuxilio.add("Em Elaboração");
            listaStatusAuxilio.add("Em Análise");
            listaStatusAuxilio.add("Aprovado");
            listaStatusAuxilio.add("Não Aprovado");
            listaStatusAuxilio.add("Em Andamento");
            listaStatusAuxilio.add("Suspenso");
            listaStatusAuxilio.add("Desistência");
            listaStatusAuxilio.add("Transferido");
            listaStatusAuxilio.add("Cancelado");
            listaStatusAuxilio.add("Finalizado");
            listaStatusAuxilio.add("Atrasado");
        } else { //Depois de criado pode ser editado para esses status
            listaStatusAuxilio.add("Em Elaboração");
            listaStatusAuxilio.add("Em Análise");
            listaStatusAuxilio.add("Aprovado");
        }
        return listaStatusAuxilio;
    }

    public void setListaStatusAuxilio(List<String> listaStatusAuxilio) {
        this.listaStatusAuxilio = listaStatusAuxilio;
    }

    public String verAuxilio(AuxilioBean auxilio) {
        userSessionMB.setAuxilio(auxilio);

        return "infoAuxilio?faces-redirect=true";
    }

    public BigDecimal somaVerbaAprovada(AuxilioBean auxilio, int moeda) {
        return ContabilidadeService.getInstance().verbaAprovadaAuxilio(auxilio, moeda);
    }

    public void prepareEditar() {
        logger.debug("--InfoProjeto ----prepareEditar--");
        if (this.projeto.getStatus().equals("Em Andamento")) {
            alterarStatusAuxilio = true;
        } else {
            alterarStatusAuxilio = false;
        }
        listaTiposAuxilio.clear();
        listaModalidades.clear();
        statusAntigo = auxilio.getStatus();
        financiador = this.auxilio.getFkModalidade().getFkTipoAuxilio().getFkFinanciador();
        this.listaTiposAuxilio.addAll(financiador.getTiposAuxilio());
        tipoAuxilioBean = this.auxilio.getFkModalidade().getFkTipoAuxilio();
        this.listaModalidades.addAll(tipoAuxilioBean.getModalidades());
        modalidade = this.auxilio.getFkModalidade();
        this.setCurrentState(EDITAR_STATE);
    }

    public void prepareAdicionar() {
        logger.debug("--InfoProjeto ----prepareAdicionar--");
        this.auxilio = new AuxilioBean();
        financiador = new FinanciadorBean();
        this.atividade = new AtividadeBean();
        contaCorrente = new ContaCorrenteBean();
        listaTiposAuxilio.clear();
        listaModalidades.clear();
        this.setCurrentState(ADICIONAR_STATE);
    }

    public void prepareRemanejar() {
        logger.debug("--InfoProjeto ----prepareRemanejar--");
        if (auxilio.getFkProjeto().getTipo() == 1) {
            this.projetoRecebedor = new EventoBean();
        } else {
            this.projetoRecebedor = new PesquisaBean();
        }

    }

    public void gravarAuxilio() {
        logger.debug("--InfoProjeto ----gravarAuxilio--");
        if (this.auxilio.getDataInicial().after(this.auxilio.getDataFinal())) {
            String message = "Data Inicial não pode ser depois da Data Final";
            FacesContext.getCurrentInstance().addMessage("erroInfoProjeto", new FacesMessage(message));
        } else {

            if (ADICIONAR_STATE.equals(this.currentState)) {
                Service.getInstance().atualizarStatusAuxilio(auxilio);
                auxilio.setFkModalidade(modalidade);

                this.projeto.getAuxilios().add(auxilio);
                Service.getInstance().atualizarStatusProjeto(projeto);
                Service.getInstance().atualizarProjeto(projeto);

                atualizarProjetoSessao();

                AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Adicionar Registro", "Info Projeto", "Adicionou auxílio no projeto de id: " + this.projeto.getId() + " - Título: " + this.projeto.getTitulo() + " - Auxílio de nome: " + auxilio.getNomeIdentificacao(), ConfigConstantes.CONFIG_AUDITORIA_ADICIONAR_AUXILIO);

            } else if (EDITAR_STATE.equals(this.currentState)) {
                if (!auxilio.getStatus().equals(statusAntigo)) {
                    AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Info Projeto", "Alterou Status do auxílio no projeto de id: " + this.projeto.getId()
                            + " - Título: " + this.projeto.getTitulo() + " - Auxílio de nome: " + auxilio.getNomeIdentificacao()
                            + " - Status alterado de:  " + statusAntigo + " para: " + auxilio.getStatus(), ConfigConstantes.CONFIG_AUDITORIA_STATUS_AUXILIO);

                    String subject = "Alteração de Status - SGPC";
                    String setTo = projeto.getFkResponsavel().getEmail();
                    String message = "O Status do auxílio de nome - " + auxilio.getNomeIdentificacao() + " \n"
                            + "Referente ao Projeto - " + projeto.getTitulo() + " \n"
                            + "Sofreu alteração de Status. Foi alterado de:  " + statusAntigo + " para: " + auxilio.getStatus();
                    MailService.getInstance().sendMail(subject, setTo, message);
                }
                auxilio.setFkModalidade(modalidade);
                Service.getInstance().atualizarAuxilio(auxilio);
                AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Info Projeto", "Editou auxílio no projeto de id: " + this.projeto.getId()
                        + " - Título: " + this.projeto.getTitulo() + " - Auxílio de nome: " + auxilio.getNomeIdentificacao(), ConfigConstantes.CONFIG_AUDITORIA_EDITAR_AUXILIO);
            }
            auxilio = new AuxilioBean();
        }
    }

    public void excluirAuxilio() {
        this.projeto.getAuxilios().remove(auxilio);
        Service.getInstance().excluirAuxilio(auxilio);
        Service.getInstance().atualizarProjeto(projeto);
        auxilio = new AuxilioBean();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void remanejarAuxilio() {
        if (projetoRecebedor.equals(projeto)) {
            FacesContext.getCurrentInstance().addMessage("remanejarErro", new FacesMessage("Remanejamento não efetuado!"));
        } else {
            this.projeto.getAuxilios().remove(auxilio);
            projetoRecebedor.getAuxilios().add(auxilio);
            Service.getInstance().atualizarProjeto(projetoRecebedor);
            Service.getInstance().atualizarProjeto(projeto);

            atualizarProjetoSessao();
            AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Remanejar Auxílio", "Info Projeto", "Remanejou auxílio do projeto de id: " + this.projeto.getId()
                    + " - Título: " + this.projeto.getTitulo()
                    + " para o projeto de id: " + projetoRecebedor.getId() + " - Título: " + projetoRecebedor.getTitulo(), ConfigConstantes.CONFIG_AUDITORIA_REMANEJAR_AUXILIO);
            
            RequestContext.getCurrentInstance().execute("PF('editRemanejaAuxilio').hide();");
        }
    }

    public void ChangeStatusProjeto() {
        if (!projeto.getStatus().equals("Finalizado") && !projeto.getStatus().equals("Cancelado") && !projeto.getStatus().equals("Abortado") && !projeto.getStatus().equals("Desistência")) {
            podeAlterarStatus = true;
        } else {
            podeAlterarStatus = false;
        }
    }

    public void alterarStatusProjeto() {

        if (statusProjeto.equals("Finalizado") || statusProjeto.equals("Cancelado") || statusProjeto.equals("Abortado")) {

            List<AuxilioBean> listaAuxilios = this.projeto.getAuxilios();
            List<String> listErro = new ArrayList<String>();

            for (int i = 0; i < listaAuxilios.size(); i++) {
                AuxilioBean auxilio = listaAuxilios.get(i);
                if (!auxilio.getStatus().equals("Finalizado")) {
                    listErro.add("Auxílio: " + auxilio.getNomeIdentificacao() + " - não Finalizado!");
                }
            }
            if (this.projeto.getDataFinal().after(new Date())) {
                listErro.add("Data Final do Projeto ainda não foi atingida!");
            }

            if (listErro.isEmpty()) {
                this.projeto.setStatus(statusProjeto);
                Service.getInstance().atualizarProjeto(projeto);

                atualizarProjetoSessao();
                AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Info Projeto", "Alterou o status do projeto de id: " + projeto.getId() + " - Título: " + this.projeto.getTitulo() + " - para o status: " + statusProjeto, ConfigConstantes.CONFIG_AUDITORIA_STATUS_PROJETO);
            } else {
                FacesContext.getCurrentInstance().addMessage("statusError", new FacesMessage("Status não pode ser alterado!"));
                for (int i = 0; i < listErro.size(); i++) {
                    FacesContext.getCurrentInstance().addMessage("statusError", new FacesMessage(listErro.get(i)));
                }
            }

        } else {
            this.projeto.setStatus(statusProjeto);
            Service.getInstance().atualizarProjeto(projeto);
            atualizarProjetoSessao();
            AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Info Projeto", "Alterou o status do projeto de id: " + projeto.getId() + " - Título: " + this.projeto.getTitulo() + " - para o status: " + statusProjeto, ConfigConstantes.CONFIG_AUDITORIA_STATUS_PROJETO);
        }
        podeAlterarStatus = false;
    }

    public void verificaAcesso() {
        if (podeCriarAuxilio && !projeto.getStatus().equals("Finalizado") && !projeto.getStatus().equals("Cancelado") && !projeto.getStatus().equals("Abortado") && !projeto.getStatus().equals("Desistência")) {
            imagemCriarRegistro = "/images/icons/add1.png";
            //podeCriarAuxilio = true;
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
            podeCriarAuxilio = false;
        }
    }

    public boolean podeEditar(AuxilioBean aux) {
        if (podeEditarAuxilio && !projeto.getStatus().equals("Finalizado") && !projeto.getStatus().equals("Cancelado") && !projeto.getStatus().equals("Abortado") && !projeto.getStatus().equals("Desistência") && !aux.getStatus().equals("Finalizado")) {
            imagemEditarRegistro = "/images/icons/editar.png";
            return true;
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
            return false;
        }
    }

    public boolean podeRemanejar(AuxilioBean aux) {
        if (podeRemanejarAuxilio && !projeto.getStatus().equals("Finalizado") && !projeto.getStatus().equals("Cancelado") && !projeto.getStatus().equals("Abortado") && !projeto.getStatus().equals("Desistência") && !aux.getStatus().equals("Finalizado")) {
            imagemRemanejarAuxilio = "/images/icons/remanejar.png";
            return true;
        } else {
            imagemRemanejarAuxilio = "/images/icons/remanejarbw.png";
            return false;
        }
    }

    public void excluirArquivoUpload() {
        auxilio.getArquivosAnexos().remove(arquivoAnexo);
        Service.getInstance().atualizarAuxilio(auxilio);
        RequestContext.getCurrentInstance().execute("PF('deletePopupArquivo').hide();");
    }

    public void atualizarAuxilioUpload() {
        arquivoAnexo.setDescricao(tipoArquivoUpload);

        if (auxilio.getArquivosAnexos() == null) {
            auxilio.setArquivosAnexos(new ArrayList<ArquivoAnexoBean>());
        }
        auxilio.getArquivosAnexos().add(arquivoAnexo);

        Service.getInstance().atualizarAuxilio(auxilio);
        RequestContext.getCurrentInstance().execute("PF('addArquivo').hide();");
    }

    public void handleFileUpload(FileUploadEvent event) {
        arquivoAnexo = new ArquivoAnexoBean();
        arquivoAnexo.setNome(event.getFile().getFileName());
        arquivoAnexo.setConteudo(event.getFile().getContents());
        
        Service.getInstance().atualizarProjeto(projeto);

        FacesMessage msg = new FacesMessage("Succesfull ", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage("uploadDeArquivo", msg);
    }

    public void pesquisar() {
        if ((pesquisa == null || pesquisa.trim().length() == 0)) {
            logger.debug("PESQUISAR TODOS");
            this.listaPessoas = Service.getInstance().listarPessoas();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaPessoas = Service.getInstance().pesquisarPessoas(pesquisa);
        }
    }

    public void prepareAdicionarParticipante() {
        participanteProjeto = new PessoaProjetoBean();
    }

    public void adicionaParticipante() {
        participanteProjeto.setFkProjeto(projeto);
        List<PessoaProjetoBean> listaPessoasProjeto = Service.getInstance().buscarPessoasProjeto(this.projeto);
        if (listaPessoasProjeto.contains(participanteProjeto)) {
            FacesContext.getCurrentInstance().addMessage("participanteError", new FacesMessage("Pessoa: " + participanteProjeto.getFkPessoa().getNome() + " já participa desse projeto!"));
        } else {
            
            Service.getInstance().cadastraPessoaProjeto(participanteProjeto);
            AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Info Projeto", "Adicionou participante ao projeto de id: " + projeto.getId() + " - Título: " + projeto.getTitulo()
                    + " - Nome do Participante: " + participanteProjeto.getFkPessoa().getNome() + " - Username: " + participanteProjeto.getFkPessoa().getUsername(), ConfigConstantes.CONFIG_AUDITORIA_ADD_PARTICIPANTE_PROJETO);
        }
        RequestContext.getCurrentInstance().execute("PF('addParticipante').hide();");
    }

    public void removerParticipante() {
        logger.debug("remove Participante -------------" + participanteProjeto.getFkPessoa().getNome());
        if(participanteProjeto.getParticipantesAuxilio() != null && !participanteProjeto.getParticipantesAuxilio().isEmpty()){
            FacesContext.getCurrentInstance().addMessage("erroExcluirParticipante", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Impossível excluir","Impossível excluir participante, pois está vinculado a auxílio(s)"));
            return;
        }
        Service.getInstance().excluirPessoaProjeto(participanteProjeto);
        AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Info Projeto", "Removeu participante do projeto de id: " + projeto.getId() + " - Título: " + projeto.getTitulo()
                + " - Nome do Participante: " + participanteProjeto.getFkPessoa().getNome() + " - Username: " + participanteProjeto.getFkPessoa().getUsername(), ConfigConstantes.CONFIG_AUDITORIA_DEL_PARTICIPANTE_PROJETO);
        RequestContext.getCurrentInstance().execute("PF('deleteParticipantePopup').hide();");
    }

    public StreamedContent downloadArquivoAnexoAuxilio(ArquivoAnexoBean arquivo) {
        logger.debug("ArquivoAnexo: " + arquivo.getNome());
        ByteArrayInputStream stream = new ByteArrayInputStream(arquivo.getConteudo());
        StreamedContent file = new DefaultStreamedContent(stream, "image/jpg", arquivo.getNome());
        return file;
    }

    public void atualizaTipoAuxilio() {
        this.listaTiposAuxilio.clear();
        this.listaTiposAuxilio.addAll(financiador.getTiposAuxilio());
        this.listaModalidades.clear();
        modalidade = new ModalidadeBean();
    }

    public void atualizaModalidade() {
        this.listaModalidades.clear();
        this.listaModalidades.addAll(tipoAuxilioBean.getModalidades());
    }

    public void pesquisarAtividade() {
        evento = (EventoBean) projeto;
        this.listaAtividade = Service.getInstance().listarAtividade(evento);
    }

    public void adicionaParticipanteAtividade() {
        logger.debug("quantidade selecionada -------------" + pessoasAtividade.length);
        List<PessoaBean> listaParticipantes = Arrays.asList(pessoasAtividade);

        for (int i = 0; i < listaParticipantes.size(); i++) {
            if (this.atividade.getPessoasInscritas().contains(listaParticipantes.get(i))) {
                FacesContext.getCurrentInstance().addMessage("participanteAtividade", new FacesMessage("Pessoa: " + listaParticipantes.get(i).getNome() + " já participa dessa Atividade!"));
            } else {
                this.atividade.getPessoasInscritas().add(listaParticipantes.get(i));
            }
        }

        Service.getInstance().atualizarAtividade(atividade);
        RequestContext.getCurrentInstance().execute("PF('addInscricaoAtividade').hide();");
    }

    public void removerInscritoAtividade() {
        logger.debug("remove Participante -------------" + participanteAtividade.getNome());

        this.atividade.getPessoasInscritas().remove(participanteAtividade);
        Service.getInstance().atualizarAtividade(atividade);
        this.atividade = Service.getInstance().pesquisarAtividade(atividade.getId());

        atualizarProjetoSessao();

        this.pesquisarAtividade();
        this.pesquisar();
        participanteAtividade = new PessoaBean();
        this.atividade = new AtividadeBean();
        RequestContext.getCurrentInstance().execute("PF('deleteParticipanteAtividade').hide();");
    }

    public void prepareEditarAtividade() {
        this.setCurrentState(EDITAR_STATE);
    }

    public void gravarAtividade() {
        if (this.atividade.getHoraInicial().after(this.atividade.getHoraFinal())) {
            String message = "Data Inicial não pode ser depois da Data Final";
            FacesContext.getCurrentInstance().addMessage("erroInfoPAtividade", new FacesMessage(message));
        } else {
            if (ADICIONAR_STATE.equals(this.currentState)) {
                logger.debug("ADICIONAR REGISTRO");
                evento = (EventoBean) projeto;
                atividade.setFkEvento(evento);
                Service.getInstance().cadastraAtividade(atividade);

            } else if (EDITAR_STATE.equals(this.currentState)) {
                logger.debug("EDITAR REGISTRO");
                Service.getInstance().atualizarAtividade(atividade);
            }

            atualizarProjetoSessao();

            this.pesquisarAtividade();
            this.pesquisar();
        }
        RequestContext.getCurrentInstance().execute("PF('addEditPopupAtividade').hide();");
    }

    public void excluirAtividade() {
        evento = (EventoBean) this.projeto;
        evento.getAtividades().remove(atividade);
        Service.getInstance().excluirAtividade(atividade);

        atualizarProjetoSessao();

        this.pesquisarAtividade();
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopupAtividade').hide();");
    }

    public void atualizarProjetoSessao() {
        this.projeto = Service.getInstance().pesquisaProjeto(projeto.getId());

        userSessionMB.setProjeto(this.projeto);
    }

    public BigDecimal verbaAprovada(ProjetoBean projeto, int moeda) {
        return ContabilidadeService.getInstance().verbaAprovadaProjeto(projeto, moeda);
    }

    public BigDecimal verbaDisponivel(ProjetoBean projeto, int moeda) {
        return ContabilidadeService.getInstance().saldoProjeto(projeto, moeda);
    }

    public BigDecimal verbaGasta(ProjetoBean projeto, int moeda) {
        return ContabilidadeService.getInstance().totalDespesaProjeto(projeto, moeda);
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    @PostConstruct
    public void postConstruct() {
        this.projeto = userSessionMB.getProjeto();
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();

        this.listaFinanciador = Service.getInstance().listarFinanciadores();
        this.listaCentroDespesa = Service.getInstance().listarCentrosDespesa();
        this.listaContaCorrente = Service.getInstance().pesquisarContasCorrentes(this.projeto.getFkResponsavel());
        this.listaProjetos = Service.getInstance().listarProjetos();
        this.listaProgramasEspeciaisFinanciamento = Service.getInstance().listarProgramaEspecialFinanciamento();
        this.listaInstituicaoBancaria = Service.getInstance().listarInstituicoesBancarias();
        this.listaPessoasBolsistas = Service.getInstance().pesquisarPessoasAlunos();

        podeCriarAuxilio = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.AUXILIO_CRIAR);
        podeEditarAuxilio = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.AUXILIO_EDITAR);
        podeRemanejarAuxilio = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.AUXILIO_REMANEJAR);
        podeAlterarStatusAuxilioFinanceiro = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.AUXILIO_ALTERAR_STATUS_FINANCEIRO);
        podeExcluirAuxilio = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.AUXILIO_EXCLUIR);

        if (this.projeto.getTipo() == 1) {
            exibeAtividade = true;
            pesquisarAtividade();
        }
        verificaAcesso();
        pesquisar();
    }

    public List<String> getListaStatusCotacao() {
        this.listaStatusCotacao.clear();
        this.listaStatusCotacao.add("Em Elaboração");
        this.listaStatusCotacao.add("Em Andamento");
        this.listaStatusCotacao.add("Finalizado");
        this.listaStatusCotacao.add("Para Cotação");

        return listaStatusCotacao;
    }

    public void setListaStatusCotacao(List<String> listaStatusCotacao) {
        this.listaStatusCotacao = listaStatusCotacao;
    }

    public void prepareAdicionarCotacao() {
        cotacao = new CotacaoBean();
        this.setCurrentState(ADICIONAR_STATE);
    }

    public void prepareEditarCotacao() {
        this.setCurrentState(EDITAR_STATE);
    }

    public boolean podeEditarCotacao(CotacaoBean cotacao) {
        if (cotacao.getStatus().equals("Em Elaboração")) {
            imagemEditarCotacao = "/images/icons/editar.png";
            return true;
        } else {
            imagemEditarCotacao = "/images/icons/editarbw.png";
            return false;
        }
    }

    public void gravarCotacao() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR Cotacao");
            cotacao.setStatus("Em Elaboração");
            cotacao.setFkProjeto(projeto);
            Service.getInstance().cadastraCotacao(cotacao);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR Cotacao");
            Service.getInstance().atualizarCotacao(cotacao);
        }
        atualizarProjetoSessao();
        RequestContext.getCurrentInstance().execute("PF('addEditPopupCotacao').hide();");
    }

    public void excluirCotacao() {
        projeto.getCotacoes().remove(cotacao);
        Service.getInstance().excluirCotacao(cotacao);
        atualizarProjetoSessao();
        RequestContext.getCurrentInstance().execute("PF('deletePopupCotacao').hide();");
    }

    public String detalharCotacao() {
        userSessionMB.setInterPageParameter(cotacao);
        return "infoCotacao?faces-redirect=true";
    }

    public void gravarCC() {
        logger.debug("ADICIONAR REGISTRO");
        this.contaCorrente.setFkPessoa(this.projeto.getFkResponsavel());
        this.projeto.getFkResponsavel().getContasCorrentes().add(this.contaCorrente);
        Service.getInstance().atualizarPessoa(this.projeto.getFkResponsavel());
        this.listaContaCorrente = Service.getInstance().pesquisarContasCorrentes(this.projeto.getFkResponsavel());
        atualizarProjetoSessao();
    }

    public boolean validarDatas(FacesContext facesContext, UIComponent uiComponent, Object newValue) {
        Date minhaData = (Date) newValue;
        if (1 == 2) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "Data Inválida"));
        }
        return true;
    }

    public Boolean getIndicarBolsista() {
        return indicarBolsista;
    }

    public void setIndicarBolsista(Boolean indicarBolsista) {
        this.indicarBolsista = indicarBolsista;
    }

    public List<PessoaBean> getListaPessoasBolsistas() {
        return listaPessoasBolsistas;
    }

    public void setListaPessoasBolsistas(List<PessoaBean> listaPessoasBolsistas) {
        this.listaPessoasBolsistas = listaPessoasBolsistas;
    }

    public String getStatusAntigo() {
        return statusAntigo;
    }

    public void setStatusAntigo(String statusAntigo) {
        this.statusAntigo = statusAntigo;
    }

    public List<PessoaProjetoBean> getListapessoasProjeto() {
        listapessoasProjeto = Service.getInstance().buscarPessoasProjeto(this.projeto);
        return listapessoasProjeto;
    }

    public void setListapessoasProjeto(List<PessoaProjetoBean> listapessoasProjeto) {
        this.listapessoasProjeto = listapessoasProjeto;
    }

    public boolean isPodeExcluirAuxilio() {
        return podeExcluirAuxilio;
    }

    public void setPodeExcluirAuxilio(boolean podeExcluirAuxilio) {
        this.podeExcluirAuxilio = podeExcluirAuxilio;
    }
    
    public boolean podeExcluirRegistro(AuxilioBean auxilio) {
        if(podeExcluirAuxilio){
            imagemExcluirRegistro = "/images/icons/del.png";
        } else {
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return podeExcluirAuxilio;
    }
    
}
