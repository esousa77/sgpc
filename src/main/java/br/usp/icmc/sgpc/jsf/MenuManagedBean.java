/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import java.io.Serializable;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "menuMB")
@ViewScoped
public class MenuManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(FinanciadorManagedBean.class);
    private boolean podeAcessarAuditoria;
    private boolean podeAcessarConfiguracao;

    public MenuManagedBean() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        Map sessionMap = ctx.getExternalContext().getSessionMap();
        UserSessionManagedBean userSessionMB = (UserSessionManagedBean) sessionMap.get("userSessionMB");
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();

        podeAcessarAuditoria = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.AUDITORIA_ACESSAR);
        podeAcessarConfiguracao = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.ACESSA_CONFIGURACOES);
    }

    public String irProjetoAtivo() {
        return "infoProjeto?faces-redirect=true";
    }

    public boolean retornaInfoProjeto() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        Map sessionMap = ctx.getExternalContext().getSessionMap();
        UserSessionManagedBean userSessionMB = (UserSessionManagedBean) sessionMap.get("userSessionMB");

        if (userSessionMB.getProjeto() == null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isPodeAcessarAuditoria() {
        return podeAcessarAuditoria;
    }

    public void setPodeAcessarAuditoria(boolean podeAcessarAuditoria) {
        this.podeAcessarAuditoria = podeAcessarAuditoria;
    }

    public boolean isPodeAcessarConfiguracao() {
        return podeAcessarConfiguracao;
    }

    public void setPodeAcessarConfiguracao(boolean podeAcessarConfiguracao) {
        this.podeAcessarConfiguracao = podeAcessarConfiguracao;
    }
}
