/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.GrupoFornecimentoBean;
import br.usp.icmc.sgpc.beans.ProdutoBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author david
 */
@ManagedBean(name = "produtosMB")
@ViewScoped
public class ProdutoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(ProdutoManagedBean.class);
    private ProdutoBean produtos;
    private String pesquisa;
    private List<ProdutoBean> listaProdutos = new ArrayList<ProdutoBean>();
    private String currentState = PESQUISAR_STATE;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String EDITAR_STATE = "editar";
    public static final String ADICIONAR_STATE = "adicionar";
    private List<GrupoFornecimentoBean> listaGruposFornecimento = new ArrayList<GrupoFornecimentoBean>();
    private GrupoFornecimentoBean grupo;
    private boolean podeEditarProduto;
    private boolean podeCriarProduto;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;

    //###----------Construtor-------------------###//
    public ProdutoManagedBean() {
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        podeEditarProduto = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PRODUTO_EDITAR);
        podeCriarProduto = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PRODUTO_CRIAR);

        this.pesquisar();
        this.listaGruposFornecimento = Service.getInstance().listarGruposFornecimento();
    }

    public List<GrupoFornecimentoBean> getListaGruposFornecimento() {
        return listaGruposFornecimento;
    }

    public void setListaGruposFornecimento(List<GrupoFornecimentoBean> listaGruposFornecimento) {
        this.listaGruposFornecimento = listaGruposFornecimento;
    }

    public String getImagemCriarRegistro() {
         if (podeCriarProduto) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarProduto) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public boolean isPodeCriarProduto() {
        return podeCriarProduto;
    }

    public void setPodeCriarProduto(boolean podeCriarProduto) {
        this.podeCriarProduto = podeCriarProduto;
    }

    public boolean isPodeEditarProduto() {
        return podeEditarProduto;
    }

    public void setPodeEditarProduto(boolean podeEditarProduto) {
        this.podeEditarProduto = podeEditarProduto;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    //###----------------------------------------------###//
    //###----------Geters and Seters-------------------###//
    //###----------------------------------------------###//
    public GrupoFornecimentoBean getGrupo() {
        return grupo;
    }

    public void setGrupo(GrupoFornecimentoBean grupo) {
        this.grupo = grupo;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public List<ProdutoBean> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<ProdutoBean> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    public ProdutoBean getProdutos() {
        return produtos;
    }

    public void setProdutos(ProdutoBean produtos) {
        this.produtos = produtos;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    //###----------------------------------------------###//
    //###--------Demais Métodos------------------------###//
    //###----------------------------------------------###//
    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.produtos = new ProdutoBean();
        if (("".equals(this.pesquisa) || this.pesquisa == null) && grupo == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaProdutos = Service.getInstance().listarProdutos();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaProdutos = Service.getInstance().pesquisarProdutosCriteria(pesquisa, grupo);
        }
    }

    public void prepareEditar() {
        this.setCurrentState(EDITAR_STATE);
    }

    public void prepareAdicionar() {
        this.clear();
        this.setCurrentState(ADICIONAR_STATE);
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        Service.getInstance().excluirProdutos(produtos);
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deleteProdutosPopup').hide();");
    }

    public void clear() {
        this.produtos = new ProdutoBean();
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            Service.getInstance().cadastraProdutos(produtos);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarProdutos(produtos);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditProdutosPopup').hide();");
    }
}
