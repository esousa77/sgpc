/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.HistoricoAuxilioBean;
import br.usp.icmc.sgpc.beans.SubcentroDespesasBean;
import br.usp.icmc.sgpc.service.ContabilidadeService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "recolherVerbaMB")
@ViewScoped
public class RecolherVerbaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(RecolherVerbaManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private AlineaBean alineaBean;
    private String nomeAlinea;
    private BigDecimal valorRs;
    private BigDecimal valorUs;
    private Date dataAlteracao;
    private String descricao;
    
    @PostConstruct
    public void postConstruct() {
        valorRs = new BigDecimal(0);
        valorUs = new BigDecimal(0);
    }
    
    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public void prepareEditar() {
        this.setCurrentState(EDITAR_STATE);
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public AlineaBean getAlineaBean() {
        return alineaBean;
    }

    public void setAlineaBean(AlineaBean alineaBean) {
        this.alineaBean = alineaBean;
    }
    
    public void recolherVerba() {
        if(alineaBean==null){
            AuxilioBean aux = userSessionMB.getAuxilio();
            
            for(AlineaBean alineaRecolher : aux.getAlineas()){
                for(SubcentroDespesasBean subc : alineaRecolher.getSubcentrosDespesas()){
                    BigDecimal saldoRemanescenteSubcentro = subc.getValor().subtract(ContabilidadeService.getInstance().totalDespesaSubcentroPorAlinea(subc));
                    if(saldoRemanescenteSubcentro.compareTo(BigDecimal.ZERO) == 1){
                        DespesaBean despesa = new DespesaBean();
                        despesa.setDataRealizada(dataAlteracao);
                        despesa.setDescricao("Recolhimento de verba");
                        despesa.setGenero(DespesaBean.GENERO_RECOLHIMENTO);
                        despesa.setObservacao("Recolhimento de verba");
                        despesa.setValor(saldoRemanescenteSubcentro);
                        despesa.setTipoDespesa("RECOLHIMENTO");
                        despesa.setFkAlinea(alineaRecolher);
                        despesa.setFkSubcentro(subc);
                        despesa.setStatus("Finalizada");
                        Service.getInstance().cadastraDespesa(despesa);
                    }
                }
            }
        }


        if(valorRs.compareTo(BigDecimal.ZERO)==1){
            HistoricoAuxilioBean historicoAuxilioBean = new HistoricoAuxilioBean();
            historicoAuxilioBean.setFkAuxilio(userSessionMB.getAuxilio());
            historicoAuxilioBean.setAcao("Recolhimento de verba");
            historicoAuxilioBean.setDescricao(descricao + " - R$");
            historicoAuxilioBean.setDataAlteracao(dataAlteracao);
            historicoAuxilioBean.setFkResponsavel(userSessionMB.getLoggedUser());
            historicoAuxilioBean.setNomeAlinea(nomeAlinea);
            historicoAuxilioBean.setValor(valorRs);
            Service.getInstance().registrarHistoricoAuxilio(historicoAuxilioBean);
        }
        if(valorUs.compareTo(BigDecimal.ZERO)==1){
            HistoricoAuxilioBean historicoAuxilioBean = new HistoricoAuxilioBean();
            historicoAuxilioBean.setFkAuxilio(userSessionMB.getAuxilio());
            historicoAuxilioBean.setAcao("Recolhimento de verba");
            historicoAuxilioBean.setDescricao(descricao + " - U$");
            historicoAuxilioBean.setDataAlteracao(dataAlteracao);
            historicoAuxilioBean.setFkResponsavel(userSessionMB.getLoggedUser());
            historicoAuxilioBean.setNomeAlinea(nomeAlinea);
            historicoAuxilioBean.setValor(valorUs);
            Service.getInstance().registrarHistoricoAuxilio(historicoAuxilioBean);
        }
        RequestContext.getCurrentInstance().execute("PF('recolherVerbaPanel').hide();");
    }
    
    public void prepareRecolherVerba(AlineaBean alinea) {
        this.alineaBean = alinea;
        valorRs = new BigDecimal(0);
        valorUs = new BigDecimal(0);
        if(alineaBean.getMoeda() == AlineaBean.MOEDA_REAL){
            valorRs = ContabilidadeService.getInstance().saldoAlinea(alineaBean);
        } else if(alineaBean.getMoeda() == AlineaBean.MOEDA_DOLAR){
            valorUs = ContabilidadeService.getInstance().saldoAlinea(alineaBean);
        }
        nomeAlinea = alineaBean.getFkCategoriaAlinea().getNome();
        dataAlteracao = new Date();
        descricao = "Recolhimento de verba";
        
        RequestContext.getCurrentInstance().execute("PF('recolherVerbaPanel').show();");
    }
    
    public void prepareRecolherVerbaAuxilio(){
        AuxilioBean aux = userSessionMB.getAuxilio();
        valorRs = ContabilidadeService.getInstance().saldoAuxilio(aux,AlineaBean.MOEDA_REAL);
        valorUs = ContabilidadeService.getInstance().saldoAuxilio(aux,AlineaBean.MOEDA_DOLAR);
        nomeAlinea = "TODAS";
        dataAlteracao = new Date();
        descricao = "Recolhimento de verba";
        RequestContext.getCurrentInstance().execute("PF('recolherVerbaPanel').show();");
    }

    public String getNomeAlinea() {
        return nomeAlinea;
    }

    public void setNomeAlinea(String nomeAlinea) {
        this.nomeAlinea = nomeAlinea;
    }

    public BigDecimal getValorRs() {
        return valorRs;
    }

    public void setValorRs(BigDecimal valorRs) {
        this.valorRs = valorRs;
    }

    public BigDecimal getValorUs() {
        return valorUs;
    }

    public void setValorUs(BigDecimal valorUs) {
        this.valorUs = valorUs;
    }

    public Date getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(Date dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
}
