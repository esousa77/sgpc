/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.ModalidadeBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "tiposAuxiliosMB")
@ViewScoped
public class TiposAuxiliosManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(TiposAuxiliosManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private FinanciadorBean financiador;
    private TipoAuxilioBean tipoAuxilio;
    private ModalidadeBean modalidade;
    private boolean podeCriarTipoAuxilio;
    private boolean podeEditarTipoAuxilio;
    private boolean podeExcluirTipoAuxilio;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/del.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private List<TipoAuxilioBean> listaTiposAuxilio = new ArrayList<TipoAuxilioBean>();
    private String pesquisa;

    @PostConstruct
    public void postConstruct() {
        //UserSessionManagedBean userSessionMB = (UserSessionManagedBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSessionMB");
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();
        this.financiador = (FinanciadorBean) userSessionMB.getInterPageParameter();
        listaTiposAuxilio = financiador.getTiposAuxilio();

        podeEditarTipoAuxilio = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.TIPO_AUXILIO_EDITAR);
        podeCriarTipoAuxilio = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.TIPO_AUXILIO_CRIAR);
        podeExcluirTipoAuxilio = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.TIPO_AUXILIO_EXCLUIR);
    }

    public List<TipoAuxilioBean> getListaTiposAuxilio() {
        return listaTiposAuxilio;
    }

    public void setListaTiposAuxilio(List<TipoAuxilioBean> listaTiposAuxilio) {
        this.listaTiposAuxilio = listaTiposAuxilio;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public void prepareEditarTipoAuxilio(TipoAuxilioBean tipoAux) {
        this.setCurrentState(EDITAR_STATE);
        this.tipoAuxilio = tipoAux;
        RequestContext.getCurrentInstance().execute("PF('addEditLinhaPopup').show();");
    }
    
    public void prepareExcluirTipoAuxilio(TipoAuxilioBean tipoAux) {
        this.setCurrentState(EDITAR_STATE);
        this.tipoAuxilio = tipoAux;
        RequestContext.getCurrentInstance().execute("PF('deleteLinhaPopup').show();");
    }

    public void prepareEditarModalidade(TipoAuxilioBean tipoAux, ModalidadeBean mod) {
        this.setCurrentState(EDITAR_STATE);
        this.tipoAuxilio = tipoAux;
        this.modalidade = mod;
        RequestContext.getCurrentInstance().execute("PF('addEditModalidadePopup').show();");
    }
    public void prepareExcluirModalidade(TipoAuxilioBean tipoAux, ModalidadeBean mod) {
        this.setCurrentState(EDITAR_STATE);
        this.tipoAuxilio = tipoAux;
        this.modalidade = mod;
        RequestContext.getCurrentInstance().execute("PF('deleteModalidadePopup').show();");
    }

    public void prepareAdicionarTipoAuxilio() {
        this.tipoAuxilio = new TipoAuxilioBean();
        this.tipoAuxilio.setFkFinanciador(financiador);
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditLinhaPopup').show();");
    }

    public void prepareAdicionarModalidade(TipoAuxilioBean tipAux) {
        this.tipoAuxilio = tipAux;
        this.modalidade = new ModalidadeBean();
        this.modalidade.setFkTipoAuxilio(tipoAuxilio);
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditModalidadePopup').show();");
    }

    public void excluirTipoAuxilio() {
        logger.debug("EXCLUIR REGISTRO LINHA");
        try{
            Service.getInstance().excluirTipoAuxilio(tipoAuxilio);
            financiador.getTiposAuxilio().remove(tipoAuxilio);
            Service.getInstance().atualizarFinanciador(financiador);
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "O tipo de auxílio tem auxílios vinculados.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        tipoAuxilio = new TipoAuxilioBean();
        RequestContext.getCurrentInstance().execute("PF('deleteLinhaPopup').hide();");
    }

    public void excluirModalidade() {
        try{
            Service.getInstance().excluirModalidade(modalidade);
            tipoAuxilio.getModalidades().remove(modalidade);
            Service.getInstance().atualizarFinanciador(tipoAuxilio.getFkFinanciador());
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "A modalidade tem auxílios vinculados.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        modalidade = new ModalidadeBean();
        RequestContext.getCurrentInstance().execute("PF('deleteModalidadePopup').hide();");
    }

    public void gravarTipoAuxilio() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO LINHA");
            financiador.getTiposAuxilio().add(tipoAuxilio);
            financiador = Service.getInstance().atualizarFinanciador(financiador);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO LINHA");
            Service.getInstance().atualizarTipoAuxilio(tipoAuxilio);
        }
        RequestContext.getCurrentInstance().execute("PF('addEditLinhaPopup').hide();");
    }

    public void gravarModalidade() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO MODALIDADE");
            tipoAuxilio.getModalidades().add(modalidade);
            tipoAuxilio = Service.getInstance().atualizarTipoAuxilio(tipoAuxilio);
            financiador = Service.getInstance().buscarFinanciador(financiador.getId());
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO MODALIDADE");
            Service.getInstance().atualizarModalidade(modalidade);
        }
        RequestContext.getCurrentInstance().execute("PF('addEditModalidadePopup').hide();");
    }

    public boolean isPodeCriarTipoAuxilio() {
        return podeCriarTipoAuxilio;
    }

    public void setPodeCriarTipoAuxilio(boolean podeCriarTipoAuxilio) {
        this.podeCriarTipoAuxilio = podeCriarTipoAuxilio;
    }

    public boolean isPodeEditarTipoAuxilio() {
        return podeEditarTipoAuxilio;
    }

    public void setPodeEditarTipoAuxilio(boolean podeEditarTipoAuxilio) {
        this.podeEditarTipoAuxilio = podeEditarTipoAuxilio;
    }

    public boolean isPodeExcluirTipoAuxilio() {
        return podeExcluirTipoAuxilio;
    }

    public void setPodeExcluirTipoAuxilio(boolean podeExcluirTipoAuxilio) {
        this.podeExcluirTipoAuxilio = podeExcluirTipoAuxilio;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public FinanciadorBean getFinanciador() {
        return financiador;
    }

    public void setFinanciador(FinanciadorBean financiador) {
        this.financiador = financiador;
    }

    public ModalidadeBean getModalidade() {
        return modalidade;
    }

    public void setModalidade(ModalidadeBean modalidade) {
        this.modalidade = modalidade;
    }

    public TipoAuxilioBean getTipoAuxilio() {
        return tipoAuxilio;
    }

    public void setTipoAuxilio(TipoAuxilioBean tipoAuxilio) {
        this.tipoAuxilio = tipoAuxilio;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getImagemCriarRegistro() {
        if (podeCriarTipoAuxilio) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarTipoAuxilio) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        if(podeExcluirTipoAuxilio){
            imagemExcluirRegistro = "/images/icons/del.png";
        }else{
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaTiposAuxilio = Service.getInstance().listarTiposAuxilio(financiador);
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaTiposAuxilio = Service.getInstance().pesquisarTiposAuxilio(financiador, pesquisa);
        }
    }
}
