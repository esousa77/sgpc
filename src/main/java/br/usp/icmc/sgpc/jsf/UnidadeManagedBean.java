/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.DepartamentoBean;
import br.usp.icmc.sgpc.beans.InstituicaoBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.UnidadeBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "unidadeMB")
@ViewScoped
public class UnidadeManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(InfoAuxilioManagedBean.class);

    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;

    private boolean podeCriarUnidade;
    private boolean podeEditarUnidade;
    private boolean podeExcluirUnidade;
    
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";

    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;

    private InstituicaoBean instituicao;
    private UnidadeBean unidade;
    private DepartamentoBean departamento;
    
    private List<UnidadeBean> listaUnidades = new ArrayList<UnidadeBean>();

    private String pesquisa;

    @PostConstruct
    public void postConstruct(){
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();
        this.instituicao = (InstituicaoBean) userSessionMB.getInterPageParameter();
        podeCriarUnidade = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.UNIDADE_CRIAR);
        podeEditarUnidade = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.UNIDADE_EDITAR);
        podeExcluirUnidade = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.UNIDADE_EXCLUIR);
        this.pesquisar();
    }

    public void prepareAdicionarUnidade(){
        unidade = new UnidadeBean();
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopupUnidade').show();");
    }

    public void prepareEditarUnidade(UnidadeBean unid){
        this.setCurrentState(EDITAR_STATE);
        this.unidade = unid;
        RequestContext.getCurrentInstance().execute("PF('addEditPopupUnidade').show();");
    }
    
    public void prepareExcluirUnidade(UnidadeBean unid){
        this.unidade = unid;
        RequestContext.getCurrentInstance().execute("PF('deletePopupUnidade').show();");
    }

    public void excluirUnidade() {
        try{
            Service.getInstance().excluirUnidade(unidade);
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "A Unidade tem departamentos vinculados.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopupUnidade').hide();");
    }

    public void prepareAdicionarDepartamento(UnidadeBean unid) {
        departamento = new DepartamentoBean();
        this.setCurrentState(ADICIONAR_STATE);
        this.unidade = unid;
        RequestContext.getCurrentInstance().execute("PF('addEditPopupDep').show();");
    }

    public void prepareEditarDepartamento(DepartamentoBean dept){
        this.setCurrentState(EDITAR_STATE);
        this.departamento = dept;
        RequestContext.getCurrentInstance().execute("PF('addEditPopupDep').show();");
    }
    public void prepareExcluirDepartamento(DepartamentoBean dept){
        this.setCurrentState(EDITAR_STATE);
        this.departamento = dept;
        RequestContext.getCurrentInstance().execute("PF('deletePopupDep').show();");
    }

    public void excluirDepartamento() {
        logger.debug("EXCLUIR REGISTRO");
        try{
            Service.getInstance().excluirDepartamento(departamento);
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "O Departamento tem pessoas vinculadas.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopupDep').hide();");
    }

    public void gravarDepartamento() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR Departamento");
            departamento.setFkUnidade(unidade);
            Service.getInstance().cadastraDepartamento(departamento);
            instituicao.getUnidades().add(unidade);
            Service.getInstance().atualizarInstituicao(instituicao);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR Departamento");            
            Service.getInstance().atualizarDepartamento(departamento);
            Service.getInstance().atualizarInstituicao(instituicao);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopupDep').hide();");
    }

    public void gravarUnidade(){
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR Unidade");
            unidade.setFkInstituicao(instituicao);
            //Service.getInstance().cadastraUnidade(unidade);
            instituicao.getUnidades().add(unidade);
            Service.getInstance().atualizarInstituicao(instituicao);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR Unidade");
            Service.getInstance().atualizarUnidade(unidade);
            //Service.getInstance().atualizarInstituicao(instituicao);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopupUnidade').hide();");
    }

    public void pesquisar(){
        logger.debug("PESQUISAR");
        this.unidade = new UnidadeBean();
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaUnidades = Service.getInstance().listarUnidades(instituicao);
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaUnidades = Service.getInstance().pesquisarUnidades(instituicao, pesquisa);
        }
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public DepartamentoBean getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoBean departamento) {
        this.departamento = departamento;
    }

    public String getImagemCriarRegistro() {
        if(podeCriarUnidade){
            imagemCriarRegistro = "/images/icons/add1.png";
        }else{
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if(podeCriarUnidade){
            imagemEditarRegistro = "/images/icons/editar.png";
        }else{
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        if(podeCriarUnidade){
            imagemExcluirRegistro = "/images/icons/del.png";
        }else{
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public InstituicaoBean getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(InstituicaoBean instituicao) {
        this.instituicao = instituicao;
    }

    public UnidadeBean getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeBean unidade) {
        this.unidade = unidade;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public List<UnidadeBean> getListaUnidades() {
        return listaUnidades;
    }

    public void setListaUnidades(List<UnidadeBean> listaUnidades) {
        this.listaUnidades = listaUnidades;
    }

    public boolean isPodeCriarUnidade() {
        return podeCriarUnidade;
    }

    public void setPodeCriarUnidade(boolean podeCriarUnidade) {
        this.podeCriarUnidade = podeCriarUnidade;
    }

    public boolean isPodeEditarUnidade() {
        return podeEditarUnidade;
    }

    public void setPodeEditarUnidade(boolean podeEditarUnidade) {
        this.podeEditarUnidade = podeEditarUnidade;
    }

    public boolean isPodeExcluirUnidade() {
        return podeExcluirUnidade;
    }

    public void setPodeExcluirUnidade(boolean podeExcluirUnidade) {
        this.podeExcluirUnidade = podeExcluirUnidade;
    }

}
