/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.service;

import br.usp.icmc.sgpc.security.ConfigConstantes;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author artur
 */
public class MailService {

    private static MailService instance = new MailService();
    Logger logger = Logger.getLogger(this.getClass());
    //private String username = "sgpc.teste@gmail.com";
    //private String password = "amepdtbaqf";

    private String username = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_MAIL_USERNAME).getValor();
    private String password = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_MAIL_PASSWORD).getValor();
    /*
     * Metodo construtor privado, para evitar
     * multipla instanciacao
     */

    private MailService() {
    }

    public static MailService getInstance() {
        return instance;
    }

    /**
     * Send mails to
     * @param subject
     * @param sendTo
     * @param message
     */
    public void sendMail(final String subject, final String sendTo, final String message) {
        Thread sendMail = new Thread() {
            public void run() {
                Properties p = new Properties();
                p.put("mail.smtp.host", ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_MAIL_HOST).getValor());
                p.put("mail.smtp.starttls.enable", "true");
                p.put("mail.smtp.auth", "true");

                Authenticator auth = new SMTPAuthenticator();

                Session session = Session.getInstance(p, auth);
                MimeMessage msg = new MimeMessage(session);

                try {
                    // "de" e "para"!!
                    msg.setFrom(new InternetAddress(username));
                    msg.setRecipients(Message.RecipientType.TO, sendTo);

                    msg.setSentDate(new Date());
                    msg.setSubject(subject);
                    msg.setText(message);

                    // enviando mensagem (tentando)
                    Transport.send(msg);
                    logger.debug("Email Enviado com Sucesso!!!");
                } catch (AddressException e) {
                    // nunca deixe catches vazios!
                    logger.debug("Address Exception - " + e.getMessage());
                } catch (MessagingException e) {
                    // nunca deixe catches vazios!
                    logger.debug("Messaging Exception - " + e.getMessage());
                }
            }
        };
        sendMail.start();
    }

    private class SMTPAuthenticator extends Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
        }
    }
}
