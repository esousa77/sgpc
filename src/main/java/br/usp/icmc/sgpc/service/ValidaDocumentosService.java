/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.usp.icmc.sgpc.service;

import java.io.Serializable;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 */
public class ValidaDocumentosService implements Serializable{
    /*
     * Objeto para implementacao do singleton
     */

    private static ValidaDocumentosService instance = new ValidaDocumentosService();
    Logger logger = Logger.getLogger(this.getClass());
    
    private ValidaDocumentosService() {
    }

    public static ValidaDocumentosService getInstance() {
        return instance;
    }

    private static final int[] pesoCPF = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
    private static final int[] pesoCNPJ = {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};

    private int calcularDigito(String str, int[] peso) {
        int soma = 0;
        for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
            digito = Integer.parseInt(str.substring(indice, indice + 1));
            soma += digito * peso[peso.length - str.length() + indice];
        }
        soma = 11 - soma % 11;
        return soma > 9 ? 0 : soma;
    }

    public boolean isValidCPF(String cpf) {
        String valor = cpf.replace(".", "");
        valor = valor.replace("-", "");
        
        if ((valor == null) || (valor.length() != 11)) {
            return false;
        }

        Integer digito1 = calcularDigito(valor.substring(0, 9), pesoCPF);
        Integer digito2 = calcularDigito(valor.substring(0, 9) + digito1, pesoCPF);
        return valor.equals(valor.substring(0, 9) + digito1.toString() + digito2.toString());
    }

    public boolean isValidCNPJ(String cnpj) {
        String valor = cnpj.replace(".", "");
        valor = valor.replace("-", "");
        valor = valor.replace("/", "");

        if ((valor == null) || (valor.length() != 14)) {
            return false;
        }

        Integer digito1 = calcularDigito(valor.substring(0, 12), pesoCNPJ);
        Integer digito2 = calcularDigito(valor.substring(0, 12) + digito1, pesoCNPJ);
        return valor.equals(valor.substring(0, 12) + digito1.toString() + digito2.toString());
    }
}
